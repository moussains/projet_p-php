function bs_input_file() {
    $(".input-file").before(
        function() {
            if ( ! $(this).prev().hasClass('input-ghost') ) {
                var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                element.attr("name",$(this).attr("name"));
                element.change(function(){
                    element.next(element).find('input').val((element.val()).split('\\').pop());
                });
                $(this).find("button.btn-choose").click(function(){
                    element.click();
                });
                $(this).find('input').css("cursor","pointer");
                $(this).find('input').mousedown(function() {
                    $(this).parents('.input-file').prev().click();
                    return false;
                });
                return element;
            }
        }
    );
}

$(function() {
    bs_input_file();
});

function previewFile(){
    var preview = document.querySelector('.img_avatar'); //selects the query named img
    preview.height = "100";
    preview.width = "100";
    var file    = document.querySelector('.input-ghost').files[0]; //sames as here
    file.height = "100";
    file.width = "100";
    var reader  = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file); //reads the data as a URL
    } else {
        preview.src = "";
    }
}

// Affichage de tooltip
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});


//Script pour un choix du lieu de restitution 
$(document).ready(function() {
    $(".restitut").hide();
    $('.depart').select2({
        placeholder: "Lieu de prise en location",
        allowClear: true,
        language: 'fr'
    });
});
$('#cb').click(function() {
    if ($('input[name=restitution]').is(':checked')) {
        $(".restitut").show();
        $('.destination').attr("required","true");
        $('.destination').select2({
            placeholder: "Lieu de restitution",
            allowClear: true,
            language: 'fr'
        });
    }
    else{
        $('.destination').removeAttr("required");
        $(".restitut").hide();
    }
});
