<?php
  require_once "../functions.php";
  $especes = mysqli_query($db, "SELECT * FROM `espece`");
  $races = mysqli_query($db, "SELECT * FROM `race`");

  if(isset($_GET['id']))
  {
    $id = $_GET['id'];
    $anim = mysqli_query($db, "SELECT *, E.description AS desE, R.description AS desR, DATE_FORMAT(dateanimal,'%M %Y') AS dtearr, A.id AS idt, E.id AS idE, R.id AS idR, A.nom AS nomA, A.disponible AS dispo, E.nom_courant AS nomE, R.nom AS nomR, sexe, E.prix AS leprix FROM race R INNER JOIN espece E ON R.id_espece = E.id INNER JOIN animal A ON E.id = A.id_espece WHERE A.id = ".$id);
    $lanim = mysqli_fetch_assoc($anim);
  }



?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <title>Arche de Cloé - ADMIN</title>

  <!-- Favicons -->
  <link href="../img/logoarchec.png" rel="icon">
  <!-- Bootstrap core CSS -->
  <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="../lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="../css/style.css" rel="stylesheet">
  <link href="../css/style-responsive.css" rel="stylesheet">
  <script src="../lib/chart-master/Chart.js"></script>

</head>

<body>
  <section id="container">
    <!--header start-->
      <?php include "../header.php"; ?>
    <!--header end-->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="../img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin</h5>
          <li class="mt">
            <a href="../">
              <i class="fa fa-home"></i>
              <span>Accueil</span>
            </a>
          </li>
          <li class="sub-menu">
            <a class="active" href="./">
              <i class="fa fa-themeisle"></i>
              <span>Animaux</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="../clients/">
              <i class="fa fa-users"></i>
              <span>Clients</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="../adoptions/">
              <i class="fa fa-paw"></i>
              <span>Adoptions</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="../reservations/">
              <i class="fa fa-registered"></i>
              <span>Réservations</span>
            </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12 main-chart">
            <!--CUSTOM CHART START -->
            <div class="border-head">
              <h3><i class="fa fa-chevron-right"></i>&nbsp;Animaux</h3> 
            </div><br><br><h2 class='text-center'>Modififcation de l'animal N°<?php echo $lanim["idt"]; ?></h2>
            <p class='text-center'> 
            </p>
            <div data-form-type="formoid">
                              
                <form class="block mbr-form" action="finaliser_modif.php?id=<?php echo $lanim['idt']; ?>" method="POST">
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                          <div class="multi-horizontal form-group" data-for="name">
                            <select class="form-control input" name="sexe" placeholder="Votre Nom" required >
                              <option value="<?php echo $lanim['sexe']; ?>"  selected hidden><?php echo $lanim['sexe']; ?></option>
                              <option value="M">M</option>
                              <option value="F">F</option>
                              <option value="I">Inconnu</option>
                            </select>
                          </div>
                          <div class="multi-horizontal form-group" data-for="name">
                              <input type="text" class="form-control input" name="nom" value="<?php echo $lanim['nomA']; ?>" placeholder="Nom" required>
                          </div>
                          <div class="multi-horizontal form-group" data-for="name">
                            <select class="form-control input" name="espece" d placeholder="Votre Nom" required >
                              <option value="<?php echo $lanim['idE']; ?>" selected hidden><?php echo $lanim['nomE']; ?>
                            <?php
                              while ($es = mysqli_fetch_array($especes)) { ?>
                                <option value="<?php echo $es['id']; ?>"> <?php echo $es['nom_courant']; ?> </option>
                            <?php    
                              }
                            ?>
                            </select>
                          </div>
                          <div class="multi-horizontal form-group" data-for="name">
                            <select class="form-control input" name="race" placeholder="Votre Nom" required ">
                              <option value="<?php echo $lanim['idR']; ?>" selected hidden><?php echo $lanim['nomR']; ?>
                            <?php
                              while ($rs = mysqli_fetch_array($races)) { ?>
                                <option value="<?php echo $rs['id']; ?>"> <?php echo $rs['nom']; ?> </option>
                            <?php    
                              }
                            ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                          <div class="multi-horizontal form-group" >
                              <label>Date de naissance : jj/mm/aaaa</label>
                              <input type="date" class="form-control input" name="datenaissance" value="<?php echo $lanim['date_naissance']; ?>" placeholder="Date naissance" required>
                          </div>
                          <div class="multi-horizontal form-group">
                              <textarea type="text" class="form-control" name="description"  placeholder="Description" required>
                                <?php echo $lanim['commentaires']; ?>
                              </textarea>
                          </div>
                        </div>
                        <div class="input-group-btn col-md-12" align="center">
                          <button href="#" type="submit" name="mofidier" class="btn btn-success btn-form display-4">Modifier</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
<div style="height: 150px;"></div>
    <!--footer start-->
    <footer class="site-footer">
      <?php include "../footer.php";?>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="../lib/jquery/jquery.min.js"></script>

  <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="../lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="../lib/jquery.scrollTo.min.js"></script>
  <script src="../lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="../lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="../lib/common-scripts.js"></script>
  <script type="text/javascript" src="../lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="../lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="../lib/sparkline-chart.js"></script>
  <script src="../lib/zabuto_calendar.js"></script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        }
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
</body>

</html>
