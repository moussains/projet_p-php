<?php
  include "../functions.php";
  $especes = mysqli_query($db, "SELECT * FROM `espece`");
  $races = mysqli_query($db, "SELECT * FROM `race`");

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <title>Arche de Cloé - ADMIN</title>

  <!-- Favicons -->
  <link href="../img/logoarchec.png" rel="icon">
  <!-- Bootstrap core CSS -->
  <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="../lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="../css/style.css" rel="stylesheet">
  <link href="../css/style-responsive.css" rel="stylesheet">
  <script src="../lib/chart-master/Chart.js"></script>

</head>

<body>
  <section id="container">
    <!--header start-->
      <?php include "../header.php"; ?>
    <!--header end-->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="../img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin</h5>
          <li class="mt">
            <a href="../">
              <i class="fa fa-home"></i>
              <span>Accueil</span>
            </a>
          </li>
          <li class="sub-menu">
            <a class="active" href="./">
              <i class="fa fa-themeisle"></i>
              <span>Animaux</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="#">
              <i class="fa fa-users"></i>
              <span>Clients</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="#">
              <i class="fa fa-paw"></i>
              <span>Adoptions</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="#">
              <i class="fa fa-registered"></i>
              <span>Réservations</span>
            </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12 main-chart">
            <!--CUSTOM CHART START -->
            <div class="border-head">
              <h3><i class="fa fa-chevron-right"></i>&nbsp;Animaux</h3> 
            </div><br><br><h2 class='text-center'>Ajout d'un animal</h2>
            <p class='text-center'> 
            </p>
            <div data-form-type="formoid">
                              
                <form class="block mbr-form" action="finaliser_ajout.php" method="post">
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                          <div class="multi-horizontal form-group" data-for="name">
                            <select class="form-control input" name="sexe" placeholder="Votre Nom" required >
                              <option value="" disabled selected hidden>- Sexe  -</option>
                              <option value="M">M</option>
                              <option value="F">F</option>
                              <option value="I">Inconnu</option>
                            </select>
                          </div>
                          <div class="multi-horizontal form-group" data-for="name">
                              <input type="text" class="form-control input" name="nom" placeholder="Nom" required>
                          </div>
                          <div class="multi-horizontal form-group" data-for="name">
                            <select class="form-control input" name="espece" d placeholder="Votre Nom" required >
                              <option value="" disabled selected hidden>- Type d'espèce -</option>
                            <?php
                              while ($es = mysqli_fetch_array($especes)) { ?>
                                <option value="<?php echo $es['id']; ?>"> <?php echo $es['nom_courant']; ?> </option>
                            <?php    
                              }
                            ?>
                            </select>
                          </div>
                          <div class="multi-horizontal form-group" data-for="name">
                            <select class="form-control input" name="race" placeholder="Votre Nom" required ">
                              <option value="" disabled selected hidden>- Race -</option>
                            <?php
                              while ($rs = mysqli_fetch_array($races)) { ?>
                                <option value="<?php echo $rs['id']; ?>"> <?php echo $rs['nom']; ?> </option>
                            <?php    
                              }
                            ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                          <div class="multi-horizontal form-group" data-for="name">
                              <label>Date de naissance : jj/mm/aaaa</label>
                              <input type="date" class="form-control input" name="datenaissance"  placeholder="Nom" required>
                          </div>
                          <div class="multi-horizontal form-group" data-for="name">
                              <textarea type="text" class="form-control input" name="description" placeholder="Description" required></textarea>
                          </div>
                        </div>
                        <div class="input-group-btn col-md-12" align="center">
                          <button href="#" type="submit" name="ajouter" class="btn btn-primary btn-form display-4">Ajouter</button>
                        </div>
                    </div>
                </form>
                <br>
                <a href="./" onclick="return confirm('Êtes-vous sûr de vouloir annuler l\'ajout ? ')" type="button" role="button" class="btn btn-danger btn-form display-4">
                  Annuler
                </a>
            </div>
          </div>
          <!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
<div style="height: 150px;"></div>
    <!--footer start-->
    <footer class="site-footer">
      <?php include "../footer.php";?>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="../lib/jquery/jquery.min.js"></script>

  <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="../lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="../lib/jquery.scrollTo.min.js"></script>
  <script src="../lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="../lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="../lib/common-scripts.js"></script>
  <script type="text/javascript" src="../lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="../lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="../lib/sparkline-chart.js"></script>
  <script src="../lib/zabuto_calendar.js"></script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        }
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
</body>

</html>
