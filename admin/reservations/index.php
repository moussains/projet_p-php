<?php
require_once "../../classes.php";


?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>Réservations LVHM - ADMIN</title>

    <!-- Favicons -->
    <link href="../img/logoarchec.png" rel="icon">
    <!-- Bootstrap core CSS -->
    <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--external css-->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="../lib/gritter/css/jquery.gritter.css" />
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style-responsive.css" rel="stylesheet">
    <script src="../lib/chart-master/Chart.js"></script>


</head>

<body>
<section id="container">
    <!-- Début header -->
    <?php include "../header.php"; ?>
    <!-- Fin header -->


    <!-- Début sidebar -->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- Début menu sidebar -->
            <ul class="sidebar-menu" id="nav-accordion">
                <p class="centered"><a href="#"><img src="../img/ui-sam.jpg" class="img-circle" width="80"></a></p>
                <h5 class="centered">Admin</h5>
                <li class="mt">
                    <a href="../">
                        <i class="fa fa-home"></i>
                        <span>Accueil</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="../clients">
                        <i class="fa fa-users"></i>
                        <span>Clients</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="../vehicules/">
                        <i class="fa fa-car"></i>
                        <span>Véhicules</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="../agences/">
                        <i class="fa fa-file"></i>
                        <span>Agences</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="../locations/">
                        <i class="fa fa-location-arrow"></i>
                        <span>Locations</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./" class="active">
                        <i class="fa fa-registered"></i>
                        <span>Réservations</span>
                    </a>
                </li>
            </ul>
            <!-- Fin menu sidebar -->
        </div>
    </aside>
    <!-- Fin sidebar -->

    <!-- Début du contenu principal -->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12 main-chart">
                    <div class="border-head">
                        <h3><i class="fa fa-chevron-right"></i>&nbsp;Réservations</h3>
                    </div><br><a href="#" class="btn btn-info" role="button" disabled><i class="fa fa-plus"></i>&nbsp;Ajouter une nouvelle réservation</a><br><br>
                    <h4>TOUS LES RESERVATIONS</h4>
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <input type="text" class="form-control" id="InputToutAnim" placeholder="Rechercher..."></div>
                        <div class="panel-body table-responsive">

                            <h4>Total : <?php $countreservation = 0; foreach ($_Reserver as $cli ) { $countreservation++; } if($countreservation<=1){ echo $countreservation.' reservation'; }else{ echo $countreservation.' reservations '; }?></h4>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center">N°Réservation</th>
                                    <th class="text-center">ID_Client</th>
                                    <th class="text-center">Immatriculation</th>
                                    <th class="text-center">DateRéservation</th>
                                    <th class="text-center">Départ le </th>
                                    <th class="text-center">Retour le </th>
                                    <th class="text-center">Prix</th>
                                    <th class="text-center">Annulée</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody id="listedesreservations">
                                <?php
                                if ($countreservation> 0) {
                                    foreach ($_Reserver as $cli) { 
                                        if($cli['annulee']==0){echo "<tr class='bg-success'>";}else{echo "<tr class='bg-danger'>";} ?>
                                            <td class="text-center"><?php echo $cli['numReservation']; ?></td>
                                            <td class="text-center"><?php echo $Clients->getClient_session($cli['id_cli'])['nom'].' '.$Clients->getClient_session($cli['id_cli'])['prenom']; ?></td>
                                            <td class="text-center"><?php echo $cli['immatriculation']; ?></td>
                                            <td class="text-center"><?php echo $cli['dateReservation']; ?></td>
                                            <td class="text-center"><?php echo $cli['dateHreDepart'].' à '.$Agences->getAgenceRestitut($cli['lieuDepart'])['ville']; ?></td>
                                            <td class="text-center"><?php echo $cli['dateHreRetour'].' à '.$Agences->getAgenceRestitut($cli['lieuRetour'])['ville']; ?></td>
                                            <td class="text-center"><?php echo $cli['montant']; ?>€</td>
                                            <td class="text-center"><?php if($cli['annulee']==0){echo "<span class='text-success'>Non</span>";}else{echo "<span class='text-danger'>Oui</span>";}; ?></td>
                                            <td class="text-center">

                                                
                                                <a class="btn btn-danger btn-xs" role="button" title="supprimer?" onClick="return confirm('Êtes-vous sûr de vouloir supprimer <?php echo $cli['id_cli'].' '.$cli['immatriculation']; ?> ?')" href="supprimer.php?id=<?php echo $cli['id_cli'];?>">
                                                    <i class="fa fa-trash-o "></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                else
                                {
                                    echo "<td class='text-center' rowspan='5' >Aucune réservation est faite !</td>";
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br><br>
                </div>
                <!-- /col-lg-12 END SECTION MIDDLE -->
            </div>
            <!-- /row -->
        </section>
    </section>
    <!-- Fin du contenu principal -->

    <!-- ---------------------Début Modal------------------------- -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Début de la contenu modal -->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">ANIMAL N°<span id="modalNumAnimal"></span></h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center"><img src="../img/clients.jpg" class="img-circle" width="30">&nbsp;<span id="modalNomAnimal"></span></h4>
                    <div class="col-md-6 col-xs-6">
                        <B>Race et type d'espèce :</B>
                        <p>
                            <small>
                                <span id="modalNomRace"></span><br>
                                <span id="modalNomEspece"></span>
                            </small>
                        </p><br>
                        <B>Date de naissance :</B>
                        <p>
                            <small>
                                <span id="modalDateNaissance"></span>
                            </small>
                        </p><br>
                        <B>Sexe :</B>
                        <p>
                            <small>
                                <span id="modalSexe"></span>
                            </small>
                        </p><br>
                        <span class="label label-info">Prix : <span id="modalPrix"></span> €</span><br>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <B>Arrrivé depuis <span id="modalDateArrivee"></span></B><br><br>
                        <B>Description :</B>
                        <p>
                            <small>
                                Nom latin : <span id="modalNomLatin"></span><br>
                                <span id="modalDesEsp"></span><br>
                                <span id="modalDesRace"></span><br>
                                <span style="color:blue;" id="modalCommentaire"></span><br>
                            </small>
                        </p>
                        <br>
                        <B>Réservé(e) par :</B><br>
                        <span id="modalPReserve"></span><br>
                        <br>
                        <B>Adopté(e) par :</B><br>
                        <span id="modalPAdopte"></span>
                    </div>
                    <a id="modalBtnModif" class="btn btn-primary btn-xs"  role="button"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Modifier</a>

                    <a id="modalBtnSup" class="btn btn-danger btn-xs" role="button"> <i class="fa fa-trash-o "></i>&nbsp;&nbsp;Supprimer</a><br><br>
                    <hr>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
            <!-- Fin de la contenu modal -->
        </div>
    </div>
    <!-- ------------------------Fin Modal-------------------------- -->

    <!-- Début footer -->
    <footer class="site-footer">
        <?php include "../footer.php";?>
    </footer>
    <!-- Fin footer -->
</section>




<!-- Les scripts -->
<script src="../lib/jquery/jquery.min.js"></script>

<script src="../lib/bootstrap/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="../lib/jquery.dcjqaccordion.2.7.js"></script>
<script src="../lib/jquery.scrollTo.min.js"></script>
<script src="../lib/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../lib/jquery.sparkline.js"></script>
<!--common script for all pages-->
<script src="../lib/common-scripts.js"></script>
<script type="text/javascript" src="../lib/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="../lib/gritter-conf.js"></script>
<!--script for this page-->
<script src="../lib/sparkline-chart.js"></script>
<script src="../lib/zabuto_calendar.js"></script>
<script type="application/javascript">

    $(document).ready(function(){

        //Recherche sur la liste des reservations
        $("#InputToutAnim").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#listedesreservations tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

    });

    function afficherModale(idt, nomA, nomR, nomE, dteN, sexe, prix, dteArr, nomL, desE, desR, commentaire )
    {
        // placer les champs dans la modale
        $("#modalNumAnimal").empty().append(idt);
        $("#modalNomAnimal").empty().append(nomA);
        $("#modalNomRace").empty().append(nomR);
        $("#modalNomEspece").empty().append(nomE);
        $("#modalDateNaissance").empty().append(dteN);
        $("#modalSexe").empty().append(sexe);
        $("#modalPrix").empty().append(prix);
        $("#modalDateArrivee").empty().append(dteArr);
        $("#modalNomLatin").empty().append(nomL);
        $("#modalDesEsp").empty().append(desE);
        $("#modalDesRace").empty().append(desR);
        $("#modalCommentaire").empty().append(commentaire);

        // bouton supp
        var commandeSup = "supprimer.php?id="+idt;
        $("#modalBtnSup").attr("href",commandeSup);

        $("#modalBtnSup").on("click", function () {
            return confirm('Êtes-vous sûr de vouloir supprimer '+nomA);
        });

        // bouton modif
        var commandeModif = "modifier.php?id="+idt;
        $("#modalBtnModif").attr("href",commandeModif);


        // afficher
        $("#myModal").modal("show");
    }

</script>


</body>

</html>
