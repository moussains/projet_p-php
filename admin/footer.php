      <div class="text-center">
        <p>
          &copy;<strong>LVHM <?php echo date('Y'); ?></strong>. Tous les droits sont réservés !
        </p>
        <div class="credits">
          Site créé par <a href="https://portfoliomoussa.000webhostapp.com/">MOUSSA et HAJAR</a>
        </div>
        <a href="#" class="go-top">
          <i class="fa fa-angle-up"></i>
        </a>
      </div>