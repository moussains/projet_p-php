<?php
  require_once "../../classes.php";
  VerifSessionOffadmin();
  if (isset($_POST['ajouter'])) {
    $Agences->setAgence($_POST['ville'], $_POST['adresse'], $_POST['cp']);
    var_dump($_POST['ville'], $_POST['adresse'], $_POST['cp']); 
  }

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <title>Arche de Cloé - ADMIN</title>

  <!-- Favicons -->
  <link href="../img/logoarchec.png" rel="icon">
  <!-- Bootstrap core CSS -->
  <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="../lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="../css/style.css" rel="stylesheet">
  <link href="../css/style-responsive.css" rel="stylesheet">
  <script src="../lib/chart-master/Chart.js"></script>

</head>

<body>
  <section id="container">
    <!--header start-->
      <?php include "../header.php"; ?>
    <!--header end-->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered"><a href="#"><img src="../img/ui-sam.jpg" class="img-circle" width="80"></a></p>
            <h5 class="centered">Admin</h5>
            <li class="mt">
                <a href="../">
                    <i class="fa fa-home"></i>
                    <span>Accueil</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="../clients/" >
                    <i class="fa fa-users"></i>
                    <span>Clients</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="../vehicules/">
                    <i class="fa fa-car"></i>
                    <span>Véhicules</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="./" class="active">
                    <i class="fa fa-file"></i>
                    <span>Agences</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="../locations/">
                    <i class="fa fa-location-arrow"></i>
                    <span>Locations</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="../reservations/">
                    <i class="fa fa-registered"></i>
                    <span>Réservations</span>
                </a>
            </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12 main-chart">
            <!--CUSTOM CHART START -->
            <div class="border-head">
                <h3><i class="fa fa-chevron-right"></i>&nbsp;Agences</h3>
            </div><br><br><h2 class='text-center'>Ajout d'une agence</h2>
            <p class='text-center'> 
            </p>
            <div class="panel panel-success" data-form-type="formoid">
              <div class="panel-heading"><h4 class="text-center">Agence bien à ajouté dans la base de données!</h4></div>
            </div>

            <div class="col-lg-4 col-lg-offset-4">
              <a href="./" class="center-block btn btn-info" role="button">Ok</a>             
            </div>

          </div>
          <!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
    <div style="height: 150px;"></div>
    <!--footer start-->
    <footer class="site-footer">
      <?php include "../footer.php";?>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="../lib/jquery/jquery.min.js"></script>

  <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="../lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="../lib/jquery.scrollTo.min.js"></script>
  <script src="../lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="../lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="../lib/common-scripts.js"></script>
  <script type="text/javascript" src="../lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="../lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="../lib/sparkline-chart.js"></script>
  <script src="../lib/zabuto_calendar.js"></script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        }/*,
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]*/
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
</body>

</html>
