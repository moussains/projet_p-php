<?php
require_once "../../classes.php";
VerifSessionOffadmin();
$modifAgence = 0;
if(isset($_GET['id']))
{
    $id = $_GET['id'];
    $lagence = $Agences ->getAgenceRestitut($id);
    $modifAgence = 1;
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <title>Agences LVHM - ADMIN</title>

    <!-- Favicons -->
    <link href="../img/logoarchec.png" rel="icon">
    <!-- Bootstrap core CSS -->
    <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--external css-->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="../lib/gritter/css/jquery.gritter.css" />
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style-responsive.css" rel="stylesheet">
    <script src="../lib/chart-master/Chart.js"></script>

</head>

<body>
<section id="container">
    <!--header start-->
    <?php include "../header.php"; ?>
    <!--header end-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
                <p class="centered"><a href="#"><img src="../img/ui-sam.jpg" class="img-circle" width="80"></a></p>
                <h5 class="centered">Admin</h5>
                <li class="mt">
                    <a href="../">
                        <i class="fa fa-home"></i>
                        <span>Accueil</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="../clients/" >
                        <i class="fa fa-users"></i>
                        <span>Clients</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="../vehicules/">
                        <i class="fa fa-car"></i>
                        <span>Véhicules</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./" class="active">
                        <i class="fa fa-file"></i>
                        <span>Agences</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="../locations/">
                        <i class="fa fa-location-arrow"></i>
                        <span>Locations</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="../reservations/">
                        <i class="fa fa-registered"></i>
                        <span>Réservations</span>
                    </a>
                </li>
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12 main-chart">
                    <!--CUSTOM CHART START -->
                    <div class="border-head">
                        <h3><i class="fa fa-chevron-right"></i>&nbsp;Agences</h3>
                    </div><br><br><h2 class='text-center'>Modififcation de l'agence N°<?php if(!empty($modifAgence) ){echo $lagence['numAgence'];} ?></h2>
                    <p class='text-center'>
                    </p>
                    <div data-form-type="formoid">
                        <form action="finaliser_modif.php?id=<?php echo $lagence['numAgence']; ?>" method="POST">
                            <div class="col-sm-12 col-md-12">
                                <h4 class="text-center"><ins>Information de l'agence</ins></h4>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-12 col-md-12" for="adresse">Adresse :</label>
                                        <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifAgence) ){echo $lagence['adresse'];} ?>" type="text" id="adresse" name="adresse" placeholder="Entrez  Le adresse" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12 col-md-12" for="cp">Code postal :</label>
                                        <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifAgence) ){echo $lagence['cp'];} ?>" type="text" id="cp" name="cp" placeholder="Ex : 76600" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-12 col-md-12" for="ville">Ville :</label>
                                        <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifAgence) ){echo $lagence['ville'];} ?>" type="text" id="ville" name="ville" placeholder="Le Havre" required>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center">&nbsp;</p>
                            <button type="submit" name="modifier" class="col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 col-xs-offset-3 col-xs-6 btn btn-default etape">Modifier</button> 
                        </form>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </section>
    </section>
    <!--main content end-->
    <div style="height: 150px;"></div>
    <!--footer start-->
    <footer class="site-footer">
        <?php include "../footer.php";?>
    </footer>
    <!--footer end-->
</section>
<!-- js placed at the end of the document so the pages load faster -->
<script src="../lib/jquery/jquery.min.js"></script>

<script src="../lib/bootstrap/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="../lib/jquery.dcjqaccordion.2.7.js"></script>
<script src="../lib/jquery.scrollTo.min.js"></script>
<script src="../lib/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../lib/jquery.sparkline.js"></script>
<!--common script for all pages-->
<script src="../lib/common-scripts.js"></script>
<script type="text/javascript" src="../lib/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="../lib/gritter-conf.js"></script>
<!--script for this page-->
<script src="../lib/sparkline-chart.js"></script>
<script src="../lib/zabuto_calendar.js"></script>
<script type="application/javascript">
    $(document).ready(function() {
        $(".modifMDP").hide();
        $("#date-popover").popover({
            html: true,
            trigger: "manual"
        });
        $("#date-popover").hide();
        $("#date-popover").click(function(e) {
            $(this).hide();
        });

        $("#my-calendar").zabuto_calendar({
            action: function() {
                return myDateFunction(this.id, false);
            },
            action_nav: function() {
                return myNavFunction(this.id);
            },
            ajax: {
                url: "show_data.php?action=1",
                modal: true
            }
        });
        $('#cb').click(function() {
            if ($('input[name=restitution]').is(':checked')) {
                $(".modifMDP").show();
                $('.mdp').attr("required","true");
            }
            else{
                $('.mdp').removeAttr("required");
                $(".modifMDP").hide();
            }
        });
    });

    function myNavFunction(id) {
        $("#date-popover").hide();
        var nav = $("#" + id).data("navigation");
        var to = $("#" + id).data("to");
        console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }

</script>
</body>

</html>
