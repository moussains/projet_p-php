<?php
  require_once "../../classes.php";
  VerifSessionOffadmin();
  $modifClient = 0;
  if(isset($_GET['id']))
  {
    $id = $_GET['id'];
    $lecli = $Clients->getClient_session($id);
    $modifClient = 1;
  }
?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <title>Clients LVHM - ADMIN</title>

  <!-- Favicons -->
  <link href="../img/logoarchec.png" rel="icon">
  <!-- Bootstrap core CSS -->
  <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="../lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="../css/style.css" rel="stylesheet">
  <link href="../css/style-responsive.css" rel="stylesheet">
  <script src="../lib/chart-master/Chart.js"></script>

</head>

<body>
  <section id="container">
    <!--header start-->
      <?php include "../header.php"; ?>
    <!--header end-->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="../img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin</h5>
          <li class="mt">
            <a href="../">
              <i class="fa fa-home"></i>
              <span>Accueil</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="./" class="active">
              <i class="fa fa-users"></i>
              <span>Clients</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="../vehicules/">
              <i class="fa fa-car"></i>
              <span>Véhicules</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="../agences/">
              <i class="fa fa-file"></i>
              <span>Agences</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="../locations/">
              <i class="fa fa-location-arrow"></i>
              <span>Locations</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="../reservations/">
              <i class="fa fa-registered"></i>
              <span>Réservations</span>
            </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12 main-chart">
            <!--CUSTOM CHART START -->
            <div class="border-head">
              <h3><i class="fa fa-chevron-right"></i>&nbsp;Clients</h3> 
            </div><br><br><h2 class='text-center'>Modififcation du client N°<?php if(!empty($modifClient) ){echo $lecli['id_cli'];} ?></h2>
            <p class='text-center'> 
            </p>
            <div data-form-type="formoid">
                <form action="finaliser_modif.php?id=<?php echo $lecli['id_cli']; ?>" method="POST">
                    <div class="col-sm-12 col-md-12">
                        <h4 class="text-center"><ins>Données personnelles du client</ins></h4>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12 col-xs-12" for="sexe">Civilité :</label>
                                <select class="col-sm-12 col-md-12 col-xs-12" id="sexe" name="sexe" required>
                                    <?php if(!empty($modifClient)){ ?>
                                      <option value="<?php echo $lecli['sexe']; ?>" selected hidden> <?php if($lecli['sexe']=="M"){ echo "Monsieur"; }else{ echo "Madame"; } ?></option>
                                    <?php } ?>
                                    <option></option>
                                    <option value="M">Monsieur</option>
                                    <option value="F">Madame</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="nom">Nom :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifClient) ){echo $lecli['nom'];} ?>" type="text" id="nom" name="nom" placeholder="Entrez  Le nom" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="prenom">Prénom :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifClient) ){echo $lecli['prenom'];} ?>" type="text" id="prenom" name="prenom" placeholder="Entrez  Le prénom" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="date_n">Date de naissance :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifClient) ){echo $lecli['dateN'];} ?>" type="date" id="date_n" name="date_n" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="email">Adresse e-mail :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifClient) ){echo $lecli['mail'];} ?>" type="email" id="email" name="email" placeholder="azerty@xyz.com" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="adresse">Adresse :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifClient) ){echo $lecli['adresse'];} ?>" type="text" id="adresse" name="adresse" placeholder="Entrez  Le adresse" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="cp">Code postal :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifClient) ){echo $lecli['cp'];} ?>" type="text" id="cp" name="cp" placeholder="Ex : 76600" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="ville">Ville :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifClient) ){echo $lecli['ville'];} ?>" type="text" id="ville" name="ville" placeholder="Le Havre" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12 col-xs-12" for="pays">Pays :</label>
                                <select class="col-sm-12 col-md-12 col-xs-12" id="pays" name="pays" required>
                                    <?php foreach ($_Pays as $paysFr) { ?>
                                        <?php if ($paysFr['id_pays'] == 75) { ?>
                                            <option value="<?php echo $paysFr['id_pays'] ?>" selected><?php echo $paysFr['nom_fr_fr']; ?></option>
                                        <?php }else{?>
                                            <option value="<?php echo $paysFr['id_pays'] ?>"><?php echo $paysFr['nom_fr_fr']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="tel">Téléphone :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(!empty($modifClient) ){echo $lecli['tel'];} ?>" type="text" id="tel" name="tel" placeholder="Ex : 0768266541" required>
                            </div>
                        </div>
                    </div>
                    <p class="text-center">&nbsp;</p>
                    <div class="col-sm-12 col-md-12"> 
                        <h4 class="text-center"><ins> Le mot de passe</ins></h4>
                        <span class="modifMDP">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-12 col-md-12" for="pwd">Mot de passe :</label>
                                    <input class="col-sm-12 col-md-12 mdp" value="" type="text" id="pwd" name="pwd" placeholder="Entrez un mot de passe">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-12 col-md-12" for="pwd_confirm">Confirmation de mot de passe :</label>
                                    <input class="col-sm-12 col-md-12 mdp" value="" type="text" id="pwd_confirm" name="pwd_confirm" placeholder="Confirmez  le mot de passe">
                                </div>
                            </div>
                        </span>
                        <div class="col-md-offset-3 col-md-6">
                            <label><input id="cb" type="checkbox" name="restitution">Modifier le mot de passe</label>
                        </div>
                    </div>
                    <p class="text-center">&nbsp;</p>
                    <button type="submit" name="modifier" class="col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 col-xs-offset-3 col-xs-6 btn btn-default etape">Modifier</button>      
                </form>
            </div>
          </div>
          <!-- /col-lg-9 END SECTION MIDDLE -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
<div style="height: 150px;"></div>
    <!--footer start-->
    <footer class="site-footer">
      <?php include "../footer.php";?>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="../lib/jquery/jquery.min.js"></script>

  <script src="../lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="../lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="../lib/jquery.scrollTo.min.js"></script>
  <script src="../lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="../lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="../lib/common-scripts.js"></script>
  <script type="text/javascript" src="../lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="../lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="../lib/sparkline-chart.js"></script>
  <script src="../lib/zabuto_calendar.js"></script>
  <script type="application/javascript">
    $(document).ready(function() {
      $(".modifMDP").hide();
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        }
      });
      $('#cb').click(function() {
          if ($('input[name=restitution]').is(':checked')) {
              $(".modifMDP").show();
              $('.mdp').attr("required","true");
          }
          else{
              $('.mdp').removeAttr("required");
              $(".modifMDP").hide();
          }
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }

  </script>
</body>

</html>
