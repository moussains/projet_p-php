<?php
  require_once "../classes.php";
  VerifSessionOffAccueiladmin();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <title>LVHM - ADMIN</title>

  <!-- Favicons -->
  <link href="./img/logoarchec.png" rel="icon">
  <!-- Bootstrap core CSS -->
  <link href="./lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="./lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="./css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="./lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="./css/style.css" rel="stylesheet">
  <link href="./css/style-responsive.css" rel="stylesheet">
  <script src="./lib/chart-master/Chart.js"></script>
</head> 

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="bottom"></div>
      </div>
      <!--logo start-->
      <a href="#" class="logo"><img src="./img/logo.png" class="img_logo"></a>
      <!--logo end-->

      <div class="top-menu top-menu1">
        <ul class="nav pull-right top-menu">
          <li class="dropdown"><a data-toggle="dropdown" class=" logout" href=""><i class="fa fa-user-circle-o"></i>&nbsp;&nbsp;<i class="fa fa-chevron-down"></i></a>
            <ul class="dropdown-menu dropdown-menu-right ">
              <li><a href="#"><i class="fa fa-user"></i>&nbsp;Mon profil(LVHM)</a></li>
              <li><a href="../connexion_bienvenue/deconnecter/"><i class="fa fa-power-off"></i>&nbsp;Déconnexion</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin</h5>
          <li class="mt">
            <a class="active" href="./">
              <i class="fa fa-home"></i>
              <span>Accueil</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="./clients/">
              <i class="fa fa-users"></i>
              <span>Clients</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="./vehicules/">
              <i class="fa fa-car"></i>
              <span>Véhicules</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="./agences/">
              <i class="fa fa-file"></i>
              <span>Agences</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="./locations/">
              <i class="fa fa-location-arrow"></i>
              <span>Locations</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="./reservations/">
              <i class="fa fa-registered"></i>
              <span>Réservations</span>
            </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12 main-chart">
            <div class="border-head">
              <h3><i class="fa fa-chevron-right"></i>&nbsp;Accueil</h3>
            </div>
            <div id="bande" class="entry-content">
              <p>&nbsp;</p>
              <div class="text-center">
                <p>
                  Pour pouvoir gérer le site internet de Location Voiture Hajar & Moussa (LVHM), cette interface d'administation est l'idéale. Elle permet de gérer l'ensemble du site de Location Voiture Hajar & Moussa (LVHM), c'est-à-dire ajouter des nouveaux clients, connaître les résevations ou les adoptions faites par des clients. Ou aussi de gérer les véhicules et les agences.
                </p><br>
                <div class="row">
                  <a href="./clients/" role="button" class="btn btn-lg leb" ><i class="fa fa-users"></i> Clients</a>
                  <a href="./vehicules/" role="button" class="btn btn-lg leb" ><i class="fa fa-car"></i> Véhicules</a>
                  <a href="./agences/" role="button" class="btn btn-lg leb" ><i class="fa fa-file"></i> Agences</a>
                  <a href="./locations/" role="button" class="btn btn-lg leb" ><i class="fa fa-location-arrow"></i> Locations</a>
                  <a href="./reservations/" role="button" class="btn btn-lg leb" ><i class="fa fa-registered"></i> Réservations</a>
                </div>
              </div> 
            </div>
          </div>
        </div>
      </section>
    </section>
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <?php include "./footer.php"; ?>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>

  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <script type="text/javascript" src="lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="lib/sparkline-chart.js"></script>
  <script src="lib/zabuto_calendar.js"></script>


  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
</body>

</html>
