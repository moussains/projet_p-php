   <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="bottom"></div>
      </div>
      <!--logo start-->
      <a href="#" class="logo"><img src="../img/logo.png" class="img_logo"></a>
      <!--logo end-->

      <div class="top-menu ">
        <ul class="nav pull-right top-menu top-menu2">
          <li class="dropdown"><a data-toggle="dropdown" class=" logout" href=""><i class="fa fa-user-circle-o"></i>&nbsp;&nbsp;<i class="fa fa-chevron-down"></i></a>
            <ul class="dropdown-menu dropdown-menu-right ">
              <li><a href="#"><i class="fa fa-user"></i>&nbsp;Mon profil(ADMIN)</a></li>
              <li><a href="../../connexion_bienvenue/deconnecter/"><i class="fa fa-power-off"></i>&nbsp;Déconnexion</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </header>