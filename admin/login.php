<?php 
  require_once "../classes.php";

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <title>ADMIN LVHM - LOGIN</title>
  <?php 
    if (isset($_POST['connexionadmin']) && !empty($TrueORfalse2) ) { 
      echo '<meta http-equiv="refresh" content="2; URL=./">'; 
    }else{
      VerifSessionOnLoginadmin();
    }
  ?>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <style type="text/css">
    #login-page{
      opacity:1.0;
      
    }

    .form-login{
      border: 4px solid #866904;
    }
  </style>
</head>

<body >
  <div id="login-page">
    <div class="container">
      <form class="form-login" action="login.php" method="POST">
        <h2 class="form-login-heading">ADMIN - LVHM</h2>
        <div class="login-wrap">
          <?php echo $reponseInsertion2; ?>
          <input type="text" class="form-control" name="user" placeholder="Identifiant" >
          <br>
          <input type="password" class="form-control" name="pwd" placeholder="Mot de passe">
          <span style="margin-top: 30%;">&nbsp;</span>    
          <button class="btn btn-theme btn-block" type="submit"  name="connexionadmin"><i class="fa fa-lock"></i> Se connecter</button>
          <hr>
          <div class="centered">
            <p>Connexion réservé uniquement aux administrateurs su site !</p>
          </div>
          <div class="text-center">
            Vous n'avez pas encore de compte ?<br/>
            <a class="" href="../">
              Inscrivez-vous sur notre site !
            </a>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="lib/jquery.backstretch.min.js"></script>
  <script>
    $.backstretch("../assets/images/login-bg.png", {
      speed: 500
    });
  </script>
</body>

</html>
