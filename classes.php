<?php
	require_once('_serveur.php');

	//---------------------------------------------------------
	// CLASS Client
	//---------------------------------------------------------
	class Client{
		//LISTE CLIENTS
		public function getClients_Pays(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("SELECT * FROM client C INNER JOIN pays P ON C.id_pays = P.id_pays ORDER BY id_cli DESC");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//INSERTION CLIENT DANS LA BASE DE DONNÉES
		public function setClients_Pays( $nom, $prenom, $dateN, $sexe, $mail, $tel, $adresse, $cp, $ville, $mdp, $id_pays){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			
			$req = $connect->prepare("INSERT INTO `client`(`nom`, `prenom`, `dateN`, `sexe`, `mail`, `tel`, `adresse`, `cp`, `ville`, `mdp`, `id_pays`) VALUES (:nom,:prenom,:dateN,:sexe,:mail,:tel,:adresse,:cp,:ville,:mdp,:id_pays)");
			$req->bindParam(':nom',$nom);
			$req->bindParam(':prenom',$prenom);
			$req->bindParam(':dateN',$dateN);
			$req->bindParam(':sexe',$sexe);
			$req->bindParam(':mail',$mail);
			$req->bindParam(':tel',$tel);
			$req->bindParam(':adresse',$adresse);
			$req->bindParam(':cp',$cp);
			$req->bindParam(':ville',$ville);
			$req->bindParam(':mdp',$mdp);
			$req->bindParam(':id_pays',$id_pays);
			$req->execute();
		}

		//UPDATE CLIENT DANS LA BASE DE DONNÉES SANS MOT DE PASSE
		public function setUpdateClients_Pays($id_cli, $nom, $prenom, $dateN, $sexe, $mail, $tel, $adresse, $cp, $ville, $id_pays){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			
			$req = $connect->prepare("UPDATE `client` SET `nom`=:nom ,`prenom`=:prenom,`dateN`=:dateN,`sexe`=:sexe,`mail`=:mail,`tel`=:tel,`adresse`=:adresse,`cp`=:cp,`ville`=:ville,`id_pays`=:id_pays WHERE id_cli = :id_cli");
			$req->bindParam(':id_cli',$id_cli);
			$req->bindParam(':nom',$nom);
			$req->bindParam(':prenom',$prenom);
			$req->bindParam(':dateN',$dateN);
			$req->bindParam(':sexe',$sexe);
			$req->bindParam(':mail',$mail);
			$req->bindParam(':tel',$tel);
			$req->bindParam(':adresse',$adresse);
			$req->bindParam(':cp',$cp);
			$req->bindParam(':ville',$ville);
			$req->bindParam(':mdp',$mdp);
			$req->bindParam(':id_pays',$id_pays);
			$req->execute();
		}


		//UPDATE CLIENT DANS LA BASE DE DONNÉES AVEC MOT DE PASSE
		public function setUpdateClients_PaysMDP($id_cli, $nom, $prenom, $dateN, $sexe, $mail, $tel, $adresse, $cp, $ville, $mdp, $id_pays){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			
			$req = $connect->prepare("UPDATE `client` SET `nom`=:nom ,`prenom`=:prenom,`dateN`=:dateN,`sexe`=:sexe,`mail`=:mail,`tel`=:tel,`adresse`=:adresse,`cp`=:cp,`ville`=:ville,`mdp`=:mdp,`id_pays`=:id_pays WHERE id_cli = :id_cli");
			$req->bindParam(':id_cli',$id_cli);
			$req->bindParam(':nom',$nom);
			$req->bindParam(':prenom',$prenom);
			$req->bindParam(':dateN',$dateN);
			$req->bindParam(':sexe',$sexe);
			$req->bindParam(':mail',$mail);
			$req->bindParam(':tel',$tel);
			$req->bindParam(':adresse',$adresse);
			$req->bindParam(':cp',$cp);
			$req->bindParam(':ville',$ville);
			$req->bindParam(':mdp',$mdp);
			$req->bindParam(':id_pays',$id_pays);
			$req->execute();
		}


		//MODIFICATION PHOTO CLIENT DANS LA BASE DE DONNÉES
		public function setClients_Photo($id_CLi, $photo){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare('UPDATE `client` SET `photo`= :photo WHERE id_cli = :id_CLi');
			$req->bindParam(':id_CLi',$id_CLi);
			$req->bindParam(':photo',$photo);
			$req->execute();
		}


		//MODIFICATION NUM_PERMIS CLIENT DANS LA BASE DE DONNÉES
		public function setClients_Permis($id_CLi, $photo){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("UPDATE `client` SET `numPermis`= :numPermi WHERE id_cli = :id_CLi");
			$req->bindParam(':id_CLi',$id_CLi);
			$req->bindParam(':numPermi',$numPermi);
			$req->execute();
		}


		//MODIFICATION NOM PRENOM DATE-NAISSANCE SEXE
		public function setClients_NPDS($id_CLi, $nom, $prenom, $dateN, $sexe){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("UPDATE client SET nom = :nom, prenom = :prenom, dateN = :dateN, sexe = :sexe WHERE id_cli = :id_CLi ");
			$req->bindParam(':id_CLi',$id_CLi);
			$req->bindParam(':nom',$nom);
			$req->bindParam(':prenom',$prenom);
			$req->bindParam(':dateN',$dateN);
			$req->bindParam(':sexe',$sexe);
			$req->execute();
		}


		//MODIFCATION ADRESSE CODE-POSTALE VILLE
		public function setClients_ACPV($id_CLi, $adrs, $cp, $ville){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("UPDATE client SET adresse = :adrs, cp = :cp, ville = :ville WHERE id_cli = :id_CLi");
			$req->bindParam(':id_CLi',$id_CLi);
			$req->bindParam(':adrs',$adrs);
			$req->bindParam(':cp',$cp);
			$req->bindParam(':ville',$ville);
			$req->execute();
		}


		//MODIFICATION ADRESSE-EMAIL TELEPHONE
		public function setClients_AT($id_CLi, $adrs_mail, $tel){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("UPDATE client SET mail = :adrsmail, tel = :tel WHERE id_cli = :id_CLi");
			$req->bindParam(':id_CLi',$id_CLi);
			$req->bindParam(':adrsmail',$adrs_mail);
			$req->bindParam(':tel',$tel);
			$req->execute();
		}


		//MODIFICATION MOT DE PASSE
		public function setClients_MDP($id_CLi, $mdp){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("UPDATE client SET mdp = :mdp WHERE id_cli = :id_CLi");
			$req->bindParam(':id_CLi',$id_CLi);
			$req->bindParam(':mdp',$mdp);
			$req->execute();
		}


		//MODIFICATION PAYS
		public function setClients_P($id_CLi, $id_pays){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("UPDATE client SET id_pays = :idpays WHERE id_cli = :id_CLi");
			$req->bindParam(':id_CLi',$id_CLi);
			$req->bindParam(':idpays', $id_pays);
			$req->execute();
		}


		//VERIFICATION SI LE CLIENT EXISTE POUR SE CONNECTER
		public function getClients_Connexion($mail){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT * FROM client WHERE mail = '".$mail."'");
			$req->execute();
			$donnees = $req->fetch();
			return $donnees;
		}

		//IDENTIFIANT DU CLIENT CONNECTE
		public function getClient_session($idcli){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT * FROM client WHERE id_cli = ".$idcli);
			$req->execute();
			$donnees = $req->fetch();
			return $donnees;
		}

		//SUPPRESSION DU CLIENT
		public function setDeleteClient($idcli){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			
			$client = $connect->prepare("DELETE FROM `client` WHERE `client`.`id_cli` = :id_CLi");
			$client->bindParam(':id_CLi',$idcli);
			$client->execute();
		}
	}
	$Clients=new Client();
	$_Clients=$Clients->getClients_Pays();


	//---------------------------------------------------------
	// CLASS Utilisateur
	//---------------------------------------------------------
	class Utilisateur{


		//VERIFICATION SI LE CLIENT EXISTE POUR SE CONNECTER
		public function getUsers_Connexion($id){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT * FROM utilisateur WHERE identifiant = '".$id."'");
			$req->execute();
			$donnees = $req->fetch();
			return $donnees;
		}

		//IDENTIFIANT DU CLIENT CONNECTE
		public function getUsers_session($idcli){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT * FROM utilisateur WHERE id = ".$idcli);
			$req->execute();
			$donnees = $req->fetch();
			return $donnees;
		}
	}
	$Users=new Utilisateur();



	//---------------------------------------------------------
	// CLASS Agence
	//---------------------------------------------------------
	class Agence{
		//LISTE AGENCES PAR VILLE
		public function getAgences(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("SELECT * FROM `agence` ORDER BY ville ");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}
		//REPRESENTER LES AGENCES EN UNE VILLE
		public function getAgencesGroup(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("SELECT * FROM `agence` GROUP BY cp ORDER BY ville ");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE AGENCES PAR UNE VILLE
		public function getAgences_ville($cp){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare('SELECT * FROM `agence` WHERE cp ="'.$cp.'"');
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//VILLE DE RESTITUTION
		public function getAgenceRestitut($id){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare('SELECT * FROM `agence` WHERE numAgence ='.$id);
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}


		//VILLE 
		public function getAgence_ville($id){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare('SELECT * FROM `agence` WHERE cp ="'.$id.'"');
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}



		//INSERT AGENCE DANS LA BASE DE DONNÉES AVEC MOT DE PASSE
		public function setAgence($ville, $adresse, $cp){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			
			$req = $connect->prepare("INSERT INTO `agence`(`ville`, `adresse`, `cp`) VALUES (:leville, :ladresse, :lecp)");
			$req->bindParam(':leville',$ville);
			$req->bindParam(':ladresse',$adresse);
			$req->bindParam(':lecp',$cp);
			$req->execute();
		}

		//UPDATE AGENCE DANS LA BASE DE DONNÉES AVEC MOT DE PASSE
		public function setUpdateAgence($numAgence, $ville, $adresse, $cp){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			
			$req = $connect->prepare("UPDATE `agence` SET `ville`= :laville ,`adresse`= :ladresse,`cp`= :lecp WHERE numAgence = :numA");
			$req->bindParam(':numA',$numAgence);
			$req->bindParam(':laville',$ville);
			$req->bindParam(':ladresse',$adresse);
			$req->bindParam(':lecp',$cp);
			$req->execute();
		}

		//SUPPRESSION D'UNE AGENCE
		public function setDeleteAgence($numAgence){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			
			$client = $connect->prepare("DELETE FROM `agence` WHERE `numAgence` = :numA");
			$client->bindParam(':numA',$numAgence);
			$client->execute();
		}
	}
	$Agences=new Agence();
	$_Agences=$Agences->getAgences();
	$_AgencesGroup=$Agences->getAgencesGroup();




	//---------------------------------------------------------
	// CLASS Pays
	//---------------------------------------------------------
	class Pays{
		//LISTE PAYS
		public function getPays(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("SELECT * FROM `pays`");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		public function getPaysid($idPays){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$req = $connect->prepare("SELECT * FROM `pays` WHERE id_pays = ".$idPays);
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}
	}
	$Pays=new Pays();
	$_Pays=$Pays->getPays();


	//---------------------------------------------------------
	// CLASS Agence_Vehicule
	//---------------------------------------------------------
	class Agence_Vehicule{
		//LISTE DES VEHICULE PAR AGENCE
		public function getAgence_V($idAgence){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$sql = "SELECT nom, libelle, ville, AV.immatriculation FROM agence A INNER JOIN agence_vehicule AV ON A.numAgence = AV.numAgence INNER JOIN vehicule V ON AV.immatriculation = V.immatriculation INNER JOIN modele M ON V.id_mdl = M.id_mdl INNER JOIN categorie C ON M.numCat = C.numCat WHERE AV.numAgence = ".$idAgence." AND disponibilite = 1";
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}
		//TARIF PAR MODEL DE VOITURE
		public function getVoitureAgence($dte_1,$dte_2, $lagence){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$Qte = dateDifference($dte_1 , $dte_2);
			if ($Qte==0) {$Qte=1;}
			$sql = "SELECT  *, (tarif*".$Qte.") AS leprix, (nbKmInclus*".$Qte.") AS lenbKmInclus, V.immatriculation AS limmat FROM agence A INNER JOIN agence_vehicule AV ON A.numAgence = AV.numAgence INNER JOIN vehicule V ON AV.immatriculation = V.immatriculation INNER JOIN modele M ON V.id_mdl = M.id_mdl INNER JOIN etre_proposee EP ON M.id_mdl = EP.id_mdl INNER JOIN formulesanschauffeur FSC ON EP.id_f = FSC.id_f WHERE A.numAgence = ".$lagence." AND disponibilite = 1 ORDER BY leprix;";
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}
	}
	$AgenceVehicule=new Agence_Vehicule();



	//---------------------------------------------------------
	// CLASS Vehicule
	//---------------------------------------------------------
	class Vehicule{

		//Liste
		public function getVoitureModel(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();

			$sql = "SELECT  * FROM vehicule V INNER JOIN modele M ON V.id_mdl = M.id_mdl INNER JOIN etre_proposee EP ON M.id_mdl = EP.id_mdl INNER JOIN formulesanschauffeur FSC ON EP.id_f = FSC.id_f ORDER BY dateAchat DESC";
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}


		//VOITURE CHOISIE
		public function getVoiture($dte_1,$dte_2, $immat){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$Qte = dateDifference($dte_1 , $dte_2);
			if ($Qte==0) {$Qte=1;}
			$sql = "SELECT  *, (tarif*".$Qte.") AS leprix, (nbKmInclus*".$Qte.") AS lenbKmInclus, V.immatriculation AS limmat FROM agence A INNER JOIN agence_vehicule AV ON A.numAgence = AV.numAgence INNER JOIN vehicule V ON AV.immatriculation = V.immatriculation INNER JOIN modele M ON V.id_mdl = M.id_mdl INNER JOIN etre_proposee EP ON M.id_mdl = EP.id_mdl INNER JOIN formulesanschauffeur FSC ON EP.id_f = FSC.id_f WHERE AV.immatriculation = '".$immat."' AND disponibilite = 1";
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}

		//UPDATE DISPONIBILITE VOITURE
		public function setDispoVoiture($immatriculation){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("UPDATE `vehicule` SET `disponibilite` = '0' WHERE `vehicule`.`immatriculation` = '".$immatriculation."';");
			$req->bindParam(':immat', $immatriculation);
			$req->execute();

		}
	}
	$Vehicule=new Vehicule();
	$_Vehicules = $Vehicule->getVoitureModel();



	//---------------------------------------------------------
	// CLASS Categorie
	//---------------------------------------------------------
	class Categorie{
		//CATEGORIE D'UNE MODELE DE VEHICULE
		public function getNumCAt($idCat){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$sql = "SELECT libelle FROM `categorie` WHERE numCat = ".$idCat;
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}

		//LISTE DES CATEGORIE VEHICULE
		public function getCat(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$sql = "SELECT libelle FROM `categorie`;";
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES CATEGORIE VEHICULE AVEC PRIX
		public function getCatprix(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$sql = "SELECT tarif, libelle, photoCat  FROM categorie C INNER JOIN modele M ON C.numCat = M.numCat INNER JOIN etre_proposee EP ON M.id_mdl = EP.id_mdl GROUP BY M.numCat ORDER BY tarif;";
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}
	}
	$Categorie=new Categorie();



	//---------------------------------------------------------
	// CLASS Etre_Proposee
	//---------------------------------------------------------
	class Etre_Proposee{
		//TARIF PAR MODEL DE VOITURE
		public function getTarif($dte_1,$dte_2, $lagence){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$Qte = dateDifference($dte_1 , $dte_2);
			if ($Qte==0) {$Qte=1;}
			$sql = "SELECT  (tarif*".$Qte.") AS leprix FROM agence A INNER JOIN agence_vehicule AV ON A.numAgence = AV.numAgence INNER JOIN vehicule V ON AV.immatriculation = V.immatriculation INNER JOIN modele M ON V.id_mdl = M.id_mdl INNER JOIN etre_proposee EP ON M.id_mdl = EP.id_mdl WHERE A.numAgence = ".$lagence." ORDER BY tarif LIMIT 1;";
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}
	}
	$Tarif_model_V=new Etre_Proposee();
	


	//---------------------------------------------------------
	// CLASS Reserver
	//---------------------------------------------------------
	class Reserver{

		//LISTE DES RESERVATIONS  
		public function getReserver(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT * FROM reserver ORDER BY dateReservation");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES RESERVATIONS POUR UN CLIENT
		public function getReserverCli($client){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, DATEDIFF(`dateHreRetour`, `dateHreDepart`) AS qte, DATEDIFF(`dateHreDepart`, NOW()) AS diff , DATE_FORMAT(dateReservation, \"%d %b %Y à %H:%i\") AS dteR, DATE_FORMAT(dateHreDepart, \"%d %b %Y à %H:%i\") AS dteHD, DATE_FORMAT(dateHreRetour, \"%d %b %Y à %H:%i\") AS dteHR  FROM reserver WHERE id_cli = ".$client." ORDER BY dateReservation DESC");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES RESERVATIONS EN COURS POUR UN CLIENT
		public function getReserverCliEncours($client){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, DATEDIFF(`dateHreRetour`, `dateHreDepart`) AS qte, DATEDIFF(`dateHreDepart`, NOW()) AS diff , DATE_FORMAT(dateReservation, \"%d %b %Y à %H:%i\") AS dteR, DATE_FORMAT(dateHreDepart, \"%d %b %Y à %H:%i\") AS dteHD, DATE_FORMAT(dateHreRetour, \"%d %b %Y à %H:%i\") AS dteHR  FROM reserver WHERE id_cli = ".$client." AND (NOW() >= dateReservation AND NOW() < dateHreRetour) AND annulee != 1 ORDER BY dateReservation DESC");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES RESERVATIONS EN PASSÉES POUR UN CLIENT
		public function getReserverCliPassees($client){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, DATEDIFF(`dateHreRetour`, `dateHreDepart`) AS qte, DATEDIFF(`dateHreDepart`, NOW()) AS diff , DATE_FORMAT(dateReservation, \"%d %b %Y à %H:%i\") AS dteR, DATE_FORMAT(dateHreDepart, \"%d %b %Y à %H:%i\") AS dteHD, DATE_FORMAT(dateHreRetour, \"%d %b %Y à %H:%i\") AS dteHR  FROM reserver WHERE id_cli = ".$client." AND dateHreRetour < NOW() ORDER BY dateReservation DESC");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}


		//LISTE DES RESERVATIONS ANNULÉES POUR UN CLIENT
		public function getReserverCliAnnulees($client){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, DATEDIFF(`dateHreRetour`, `dateHreDepart`) AS qte, DATEDIFF(`dateHreDepart`, NOW()) AS diff , DATE_FORMAT(dateReservation, \"%d %b %Y à %H:%i\") AS dteR, DATE_FORMAT(dateHreDepart, \"%d %b %Y à %H:%i\") AS dteHD, DATE_FORMAT(dateHreRetour, \"%d %b %Y à %H:%i\") AS dteHR  FROM reserver WHERE id_cli = ".$client." AND annulee = 1 ORDER BY dateReservation DESC");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		 
		//RESERVATION EFFECTUE SANS CHAUFFEUR CHOISIE
		public function getReserverFSCImmat($dte_1,$dte_2, $immat){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$sql = "SELECT  *, (nbKmInclus*DATEDIFF('".$dte_2."', '".$dte_1."')) AS lenbKmInclus, V.immatriculation AS limmat FROM reserver AV INNER JOIN vehicule V ON AV.immatriculation = V.immatriculation INNER JOIN modele M ON V.id_mdl = M.id_mdl INNER JOIN etre_proposee EP ON M.id_mdl = EP.id_mdl INNER JOIN formulesanschauffeur FSC ON EP.id_f = FSC.id_f WHERE AV.immatriculation = '".$immat."' ";
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}

		//RESERVATION EFFECTUEE AVEC CHAUFFEUR PAR IMMATRICULE 
		public function getReserverFACImmat($immat){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();			
			$sql = "SELECT  *, V.immatriculation AS limmat FROM reserver AV INNER JOIN vehicule V ON AV.immatriculation = V.immatriculation INNER JOIN modele M ON V.id_mdl = M.id_mdl INNER JOIN etre_proposee EP ON M.id_mdl = EP.id_mdl INNER JOIN formulesanschauffeur FSC ON EP.id_f = FSC.id_f WHERE AV.immatriculation = '".$immat."' ";
			$req = $connect->prepare($sql);
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}

		//INSERTITION DE LA RESERVATION 
		public function setReserver($immatriculation, $id_cli, $numReservation, $dateReservation, $dateHreDepart, $dateHreRetour, $montant, $idDepart, $idRetour, $annulation, $formule){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("INSERT INTO `reserver`(`immatriculation`, `id_cli`, `numReservation`, `dateReservation`, `dateHreDepart`, `dateHreRetour`, `montant`, `lieuDepart`, `lieuRetour`, `annulation`,`fsc`,annulee) VALUES (:immat,:lecli,:nResv,:dteR,:dteHD,:dteHR,:prix,:idD,:idR,:anul,:fsc,0);");
			$req->bindParam(':immat', $immatriculation);
			$req->bindParam(':lecli', $id_cli);
			$req->bindParam(':nResv', $numReservation);
			$req->bindParam(':dteR', $dateReservation);
			$req->bindParam(':dteHD', $dateHreDepart);
			$req->bindParam(':dteHR', $dateHreRetour);
			$req->bindParam(':prix', $montant);
			$req->bindParam(':idD', $idDepart);
			$req->bindParam(':idR', $idRetour);
			$req->bindParam(':anul', $annulation);
			$req->bindParam(':fsc', $formule);
			$req->execute();
			//MODIFICATION DE DISPONIBILITE 
			$Vehicule=new Vehicule();
			$Vehicule->setDispoVoiture($immatriculation);
		}


		//MODIFICATION SI UNE RESERVATION EST ANNULÉE 
		public function setReserverAnnulee($immatriculation, $id_cli){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("UPDATE `reserver` SET `annulee` = '1' WHERE `reserver`.`immatriculation` = :immat AND `reserver`.`id_cli` = :lecli;");
			$req->bindParam(':immat', $immatriculation);
			$req->bindParam(':lecli', $id_cli);
			$req->execute();;
		}
	}
	$Reserver=new Reserver();
	$_Reserver = $Reserver->getReserver();


	//---------------------------------------------------------
	// CLASS Location
	//---------------------------------------------------------
	class Location{

		//LISTE DES LOCATIONS  
		public function getLocation(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT * FROM location");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES LOCATIONS POUR UN CLIENT
		public function getLocationCli($client){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, DATEDIFF(`dateHreRetourPrevu`, `dateHreDepartPrevu`) AS qte, DATEDIFF(`dateHreDepartPrevu`, NOW()) AS diff , DATE_FORMAT(dateLocation, \"%d %b %Y à %H:%i\") AS dteR, DATE_FORMAT(dateHreDepartPrevu, \"%d %b %Y à %H:%i\") AS dteHD, DATE_FORMAT(dateHreRetourPrevu, \"%d %b %Y à %H:%i\") AS dteHR  FROM location WHERE id_cli = ".$client);
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES LOCATIONS POUR UN CLIENT
		public function getLocationCliPassees($client){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, DATEDIFF(`dateHreRetourPrevu`, `dateHreDepartPrevu`) AS qte, DATEDIFF(`dateHreDepartPrevu`, NOW()) AS diff , DATE_FORMAT(dateLocation, \"%d %b %Y à %H:%i\") AS dteR, DATE_FORMAT(dateHreDepartPrevu, \"%d %b %Y à %H:%i\") AS dteHD, DATE_FORMAT(dateHreRetourPrevu, \"%d %b %Y à %H:%i\") AS dteHR  FROM location WHERE id_cli = ".$client." AND dateHreRetourPrevu < NOW()");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES LOCATIONS POUR UN CLIENT
		public function getLocationCliEncours($client){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, DATEDIFF(`dateHreRetourPrevu`, `dateHreDepartPrevu`) AS qte, DATEDIFF(`dateHreDepartPrevu`, NOW()) AS diff , DATE_FORMAT(dateLocation, \"%d %b %Y à %H:%i\") AS dteR, DATE_FORMAT(dateHreDepartPrevu, \"%d %b %Y à %H:%i\") AS dteHD, DATE_FORMAT(dateHreRetourPrevu, \"%d %b %Y à %H:%i\") AS dteHR FROM location WHERE id_cli = ".$client." AND (NOW() >= dateHreDepartPrevu AND NOW() < dateHreRetourPrevu)");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//INSERTITION DE LA LOCATION 
		//(`dateLocation`, `montantRegle`, `dateHreDepartPrevu`, `dateHreRetourPrevu`, `id_cli`, `immatriculation`, `numAgence`, `numAgence_retourner`)
		public function setLocation($numR, $dateLocation, $montant, $dateHreDepart, $dateHreRetour, $id_cli, $immatriculation, $idDepart, $idRetour){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("INSERT INTO `location` (numLocation,`dateLocation`, `montantRegle`, `dateHreDepartPrevu`, `dateHreRetourPrevu`,`id_cli`,`immatriculation`, `numAgence`, `numAgence_retourner`) VALUES (:numR, :dteL, :prix, :dteHD, :dteHR, :lecli, :immat, :idD, :idR);");
			$req->bindParam(':numR', $numR);
			$req->bindParam(':dteL', $dateLocation);
			$req->bindParam(':prix', $montant);
			$req->bindParam(':dteHD', $dateHreDepart);
			$req->bindParam(':dteHR', $dateHreRetour);
			$req->bindParam(':lecli', $id_cli);
			$req->bindParam(':immat', $immatriculation);
			$req->bindParam(':idD', $idDepart);
			$req->bindParam(':idR', $idRetour);
			$req->execute();
			//MODIFICATION DE DISPONIBILITE 
			$Vehicule=new Vehicule();
			$Vehicule->setDispoVoiture($immatriculation);
		}
	}
	$Location=new Location();
	$_Locations = $Location->getLocation();


	//---------------------------------------------------------
	// CLASS LocationsansChauffeur
	//---------------------------------------------------------
	class Locationsanschauffeur{

		//LISTE DES LOCATIONS SANS CHAUFFEUR 
		public function getLocationsanschauffeur(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT * FROM locationsanschauffeur");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES LOCATIONSsanschauffeur POUR UN CLIENT
		public function getLocationsanschauffeurCli($client){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, DATEDIFF(`dateHreRetourPrevu`, `dateHreDepartPrevu`) AS qte, DATEDIFF(`dateHreDepartPrevu`, NOW()) AS diff , DATE_FORMAT(dateLocation, \"%d %b %Y à %H:%i\") AS dteR, DATE_FORMAT(dateHreDepartPrevu, \"%d %b %Y à %H:%i\") AS dteHD, DATE_FORMAT(dateHreRetourPrevu, \"%d %b %Y à %H:%i\") AS dteHR  FROM location WHERE id_cli = ".$client);
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//INSERTITION DE LA LOCATION sanschauffeur
		//(`dateLocation`, `montantRegle`, `dateHreDepartPrevu`, `dateHreRetourPrevu`, `id_cli`, `immatriculation`, `numAgence`, `numAgence_retourner`)
		public function setLocationsanschauffeur($numR, $dateLocation, $montant, $dateHreDepart, $dateHreRetour, $id_cli, $immatriculation, $idDepart, $idRetour){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("INSERT INTO `locationsanschauffeur` (numLocation,`dateLocation`, `montantRegle`, `dateHreDepartPrevu`, `dateHreRetourPrevu`,`id_cli`,`immatriculation`, `numAgence`, `numAgence_retourner`) VALUES (:numR, :dteL, :prix, :dteHD, :dteHR, :lecli, :immat, :idD, :idR);");
			$req->bindParam(':numR', $numR);
			$req->bindParam(':dteL', $dateLocation);
			$req->bindParam(':prix', $montant);
			$req->bindParam(':dteHD', $dateHreDepart);
			$req->bindParam(':dteHR', $dateHreRetour);
			$req->bindParam(':lecli', $id_cli);
			$req->bindParam(':immat', $immatriculation);
			$req->bindParam(':idD', $idDepart);
			$req->bindParam(':idR', $idRetour);
			$req->execute();
			//MODIFICATION DE DISPONIBILITE 
			$Vehicule=new Vehicule();
			$Vehicule->setDispoVoiture($immatriculation);
		}
	}
	$Locationsanschauffeur=new Locationsanschauffeur();



	//---------------------------------------------------------
	// CLASS LocationavecChauffeur
	//---------------------------------------------------------
	class Locationavecchauffeur{

		//LISTE DES LOCATIONS avec CHAUFFEUR 
		public function getLocationavecchauffeur(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT * FROM locationavecchauffeur");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES LOCATIONSavecchauffeur POUR UN CLIENT
		public function getLocationavecchauffeurCli($client){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, DATEDIFF(`dateHreRetourPrevu`, `dateHreDepartPrevu`) AS qte, DATEDIFF(`dateHreDepartPrevu`, NOW()) AS diff , DATE_FORMAT(dateLocation, \"%d %b %Y à %H:%i\") AS dteR, DATE_FORMAT(dateHreDepartPrevu, \"%d %b %Y à %H:%i\") AS dteHD, DATE_FORMAT(dateHreRetourPrevu, \"%d %b %Y à %H:%i\") AS dteHR  FROM location WHERE id_cli = ".$client);
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//INSERTITION DE LA LOCATION avecchauffeur
		//(`dateLocation`, `montantRegle`, `dateHreDepartPrevu`, `dateHreRetourPrevu`, `id_cli`, `immatriculation`, `numAgence`, `numAgence_retourner`)
		public function setLocationavecchauffeur($numR, $dateLocation, $montant, $dateHreDepart, $dateHreRetour, $id_cli, $immatriculation, $idDepart, $idRetour){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("INSERT INTO `locationavecchauffeur` (numLocation,`dateLocation`, `montantRegle`, `dateHreDepartPrevu`, `dateHreRetourPrevu`,`id_cli`,`immatriculation`, `numAgence`, `numAgence_retourner`) VALUES (:numR, :dteL, :prix, :dteHD, :dteHR, :lecli, :immat, :idD, :idR);");
			$req->bindParam(':numR', $numR);
			$req->bindParam(':dteL', $dateLocation);
			$req->bindParam(':prix', $montant);
			$req->bindParam(':dteHD', $dateHreDepart);
			$req->bindParam(':dteHR', $dateHreRetour);
			$req->bindParam(':lecli', $id_cli);
			$req->bindParam(':immat', $immatriculation);
			$req->bindParam(':idD', $idDepart);
			$req->bindParam(':idR', $idRetour);
			$req->execute();
			//MODIFICATION DE DISPONIBILITE 
			$Vehicule=new Vehicule();
			$Vehicule->setDispoVoiture($immatriculation);
		}
	}
	$Locationavecchauffeur=new Locationavecchauffeur();
	

	//---------------------------------------------------------
	// CLASS Note_locaton
	//---------------------------------------------------------
	class Note_location{

		//LISTE DES Note_location
		public function getNote_location(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT *, nom, prenom, note, description, NL.numLocation AS lalocation, DATE_FORMAT(dates, \"%d %b %Y à %H:%i\") AS dte  FROM note N INNER JOIN note_location NL ON N.id_note = NL.id_note INNER JOIN location L ON NL.numLocation = L.numLocation INNER JOIN client C ON L.id_cli = C.id_cli ORDER BY dates DESC LIMIT 3");
			$req->execute(); 
			$donnees = $req->fetchAll();
			return $donnees;
		}

		//LISTE DES Note_location
		public function getNoteMoyen_Count(){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT ROUND(AVG(note),2) as moyen, COUNT(note) as nbNote FROM note_location NL INNER JOIN note N ON NL.id_note = N.id_note");
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}


		//AFFICHAGE D'UNE NOTE
		public function getNote_locationLoc($numNL){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("SELECT * FROM note_location NL INNER JOIN note N ON NL.id_note = N.id_note WHERE numLocation = ".$numNL);
			$req->execute(); 
			$donnees = $req->fetch();
			return $donnees;
		}

		//INSERTITION DE LA NOTE LOCATION
		public function setNote_location($numL, $idnote, $description, $dates){
			//Connexion PDO
			$connect = new ConnectionPDO();
			$connect = $connect->getConnexionPDO();
			$req = $connect->prepare("INSERT INTO `note_location`(`numLocation`, `id_note`, `description`, `dates`) VALUES (:numL, :idnote, :prix, :dteHD);");
			$req->bindParam(':numL', $numL);
			$req->bindParam(':idnote', $idnote);
			$req->bindParam(':prix', $description);
			$req->bindParam(':dteHD', $dates);
			$req->execute();
		}

	}
	$Note_location=new Note_location();
	$_Note_location = $Note_location->getNote_location();
	$_MoyenCOunt = $Note_location->getNoteMoyen_Count();





























/*-------------------------------------------------------- Operations hors classes ------------------------------------------------*/

	
	//MODIFICATION DES INFORMATIONS POUR UN CLIENT
	$reponseAfterUpdate = "";
	if (isset($_POST['modifierinfos'])) {

		if ($_POST['verif'] == "a") {
			$Clients->setClients_NPDS( $_POST['lecli'], $_POST['nom'], $_POST['prenom'], $_POST['date_n'], $_POST['sexe']);
			$reponseAfterUpdate = "<div class='alert alert-success alert-dismissible text-center'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Vos informations sont mises à jour !</strong></div>";
		}

		elseif ($_POST['verif'] == "b") {
			$Clients->setClients_ACPV( $_POST['lecli'], $_POST['adresse'], $_POST['cp'], $_POST['ville']);
			$reponseAfterUpdate = "<div class='alert alert-success alert-dismissible text-center'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Vos informations sont mises à jour !</strong></div>";
		}

		elseif ($_POST['verif'] == "c") {
			$cli = $Clients->getClients_Connexion($_POST['lemail']);
			if (md5($_POST['pwd']) == $cli['mdp']) {
				$Clients->setClients_AT( $_POST['lecli'], $_POST['email'], $_POST['tel']);
				$reponseAfterUpdate = "<div class='alert alert-success alert-dismissible text-center'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Vos informations sont mises à jour !</strong></div>";
			}else{
				$reponseAfterUpdate = "<div class='alert alert-danger alert-dismissible text-center'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Votre mot de passe est incorrect. Veuillez réessayer !</strong></div>";
			}
		}

		elseif ($_POST['verif'] == "d") {
			$cli = $Clients->getClients_Connexion($_POST['lemail']);
			if (md5($_POST['pwd']) == $cli['mdp'] && $_POST['pwd1'] == $_POST['pwd2']) {
				$Clients->setClients_MDP( $_POST['lecli'], md5($_POST['pwd1']));
				$reponseAfterUpdate = "<div class='alert alert-success alert-dismissible text-center'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Votre mot de passe est mis à jour !</strong></div>";
			}else{
				$reponseAfterUpdate = "<div class='alert alert-danger alert-dismissible text-center'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Votre mot de passe est incorrect ou votre nouveau mot de passe n'est pas pareil et celui du confirmation. Veuillez réessayer !</strong></div>";
			}
		}

		else{
			$Clients->setClients_P( $_POST['lecli'], $_POST['pays']);
			$reponseAfterUpdate = "<div class='alert alert-success alert-dismissible text-center'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Votre pays est mis à jour !</strong></div>";
		}
	}
	
	//VÉRIFICATION DE MOT DE PASSE ------------ INSCRIPTION
	$reponseInsertion = '';
	$TrueORfalse = 0;
	if (isset($_POST['inscription'])) {
		$cli = $Clients->getClients_Connexion($_POST['email']);
		//VERIFICATION SI LE COMPTE N'EXISTE PAS DEJA
		if(empty($cli)){
			if($_POST['pwd']==$_POST['pwd_confirm']) {
				$Clients->setClients_Pays($_POST['nom'], $_POST['prenom'], $_POST['date_n'], $_POST['sexe'], $_POST['email'], $_POST['tel'], $_POST['adresse'], $_POST['cp'], $_POST['ville'], md5($_POST['pwd']), $_POST['pays']);
				$reponseInsertion = '<div class="alert alert-success text-center"><strong>Votre compte a été créer !</strong> Vous allez être redirigé automatiquement vers la page de <a href="../connecter" class="alert-link">connexion</a>.</div>';
				$TrueORfalse = 1;
			}else{
				$reponseInsertion = '<div class="alert alert-danger text-center"><strong>Vos mots de passes ne sont pas identiques !</strong></div>';
			}
		}else{
			$reponseInsertion = '<div class="alert alert-danger text-center"><strong>Cette adresse email existe déjà ! </strong></div>';
		}
	}


	//VÉRIFICATION DE CONNEXION ------------ CONNEXION - CLIENT
	$reponseInsertion1 = '';
	$TrueORfalse1 = 0;
	if (isset($_POST['connexion'])) {

		$cli = $Clients->getClients_Connexion($_POST['email']);
		//SI LES DONNEES EXISTENT DANS LA DB
		if (!empty($cli)){
			if ($_POST['email']==$cli['mail'] && md5($_POST['pwd']) == $cli['mdp']) {
				session_start();
				$_SESSION['id_cli'] = $cli['id_cli'];
				//MESSAGE APRES AVOIR VALIDER LA CONNEXION
				$reponseInsertion1 = '<div class="alert alert-success text-center"><strong>Vous êtes désormais connecté !</strong> Vous allez être redirigé automatiquement vers la page d\'<a href="../connexion_bienvenue" class="alert-link">accueil</a>.</div>';
				$TrueORfalse1 = 1;
			}elseif (($_POST['email']==$cli['mail'] && md5($_POST['pwd']) != $cli['mdp']) ){
				//MESSAGE APRES AVOIR VALIDER LA CONNEXION
				$reponseInsertion1 = '<div class="alert alert-warning text-center"><strong>Votre adresse email ou mot de passe est incorrecte !</strong></div>';
			}
		}
		//SINON LES DONNEES N'EXISTENT DANS LA DB 
		else{
			//MESSAGE APRES AVOIR VALIDER LA CONNEXION
			$reponseInsertion1 = '<div class="alert alert-danger text-center"><strong>Ces informations n\'existent pas dans notre base de données !</strong> <a href="../inscrire" class="alert-link">Inscrivez-vous !</a></div>';
		}	
	}

	//VÉRIFICATION DE CONNEXION ------------ CONNEXION - ADMIN
	$reponseInsertion2 = '';
	$TrueORfalse2 = 0;
	if (isset($_POST['connexionadmin'])) {

		$utilisateur = $Users->getUsers_Connexion($_POST['user']);
		//SI LES DONNEES EXISTENT DANS LA DB
		if (!empty($utilisateur)){
			if ($_POST['user']==$utilisateur['identifiant'] && md5($_POST['pwd']) == $utilisateur['mdp']) {
				session_start();
				$_SESSION['id'] = $utilisateur['id'];
				//MESSAGE APRES AVOIR VALIDER LA CONNEXION
				$reponseInsertion2 = '<div class="alert alert-success text-center"><strong>Vous êtes désormais connecté !</strong> Vous allez être redirigé automatiquement vers la page d\'admin.</div>';
				$TrueORfalse2 = 1;
			}elseif (($_POST['user']==$utilisateur['identifiant'] && md5($_POST['pwd']) != $utilisateur['mdp']) ){
				//MESSAGE APRES AVOIR VALIDER LA CONNEXION
				$reponseInsertion2 = '<div class="alert alert-warning text-center"><strong>Votre identifiant ou mot de passe est incorrecte !</strong></div>';
			}
		}
		//SINON LES DONNEES N'EXISTENT DANS LA DB 
		else{
			//MESSAGE APRES AVOIR VALIDER LA CONNEXION
			$reponseInsertion2 = '<div class="alert alert-danger text-center"><strong>Ces informations n\'existent pas dans notre base de données !</strong></div>';
		}	
	}




	// FONCTION DIFFERENCE ENTRE DEUX DATES
	function dateDifference($date_1 , $date_2)
	{
		$datetime1 = date_create($date_1);
		$datetime2 = date_create($date_2);
		$interval = date_diff($datetime1, $datetime2); 
		return $interval->format('%a');
	}



	// FONCTION POUR RANDOM
	function GeraHash($qtd){
		//Under the string $Caracteres you write all the characters you want to be used to randomly generate the code.
		$Caracteres = '123456789';
		$QuantidadeCaracteres = strlen($Caracteres);
		$QuantidadeCaracteres--;

		$Hash=NULL;
		    for($x=1;$x<=$qtd;$x++){
		        $Posicao = rand(0,$QuantidadeCaracteres);
		        $Hash .= substr($Caracteres,$Posicao,1);
		    }

		return $Hash;
	} 

	/* ********************************************* FONCTIONS DE VERIF SI SESSION POUR CLIENT ***********************************/

	// FONCTION VERIFICATION SI SESSION EST OFF
	function VerifSessionOff(){
		if(session_status() === PHP_SESSION_NONE) session_start(); 
		if (isset($_SESSION['id_cli'])){}else{ header("Location: ../../"); } 
	}

	// FONCTION VERIFICATION SI SESSION EST OFF SINON REDIRECTION A LA PAGE D'ACCUEIL
	function VerifSessionOnEtGETLieu(){
		if(session_status() === PHP_SESSION_NONE) session_start(); 
		if (isset($_SESSION['id_cli'])){}else{ header("Location: ../../"); } 
		if (!isset($_GET['lieu_depart']) || empty($_GET['lieu_depart']) ) { header('Location: ../'); }
	}

	// FONCTION VERIFICATION SI SESSION EST ON
	function VerifSessionOn(){
		if(session_status() === PHP_SESSION_NONE) session_start(); 
		if (isset($_SESSION['id_cli'])){ header("Location: ../connexion_bienvenue/"); } 
	}
	// FONCTION VERIFICATION SI SESSION EST ON
	function VerifSessionOnAccueil(){
		if(session_status() === PHP_SESSION_NONE) session_start(); 
		if (isset($_SESSION['id_cli'])){ header("Location: ./connexion_bienvenue/"); } 
	}


	// FONCTION VERIFICATION SI SESSION EST ON SINON REDIRECTION A LA PAGE D'ACCUEIL
	function VerifSessionOnSinon(){
		if(session_status() === PHP_SESSION_NONE) session_start(); 
		if (isset($_SESSION['id_cli'])) {
	        header("Location: ../connexion_bienvenue/");
	    }else{
	        if (!isset($_GET['lieu_depart']) || empty($_GET['lieu_depart']) ) { header('Location: ../'); }
	    } 
	}

	/* ********************************************* FONCTIONS DE VERIF SI SESSION POUR ADMIN ***********************************/
	// FONCTION VERIFICATION SI SESSION EST OFF
	function VerifSessionOffadmin(){
		if(session_status() === PHP_SESSION_NONE) session_start(); 
		if (isset($_SESSION['id'])){}else{ header("Location: ../login.php"); } 
	}

	// FONCTION VERIFICATION SI SESSION EST OFF
	function VerifSessionOffAccueiladmin(){
		if(session_status() === PHP_SESSION_NONE) session_start(); 
		if (!isset($_SESSION['id'])){ header("Location: login.php"); } 
	}
	// FONCTION VERIFICATION SI SESSION EST ON
	function VerifSessionOnLoginadmin(){
		if(session_status() === PHP_SESSION_NONE) session_start(); 
		if (isset($_SESSION['id'])){ header("Location: ./"); } 
	}




	

	// FONCTION POUR NOTATION
	function NotationdeLocation($lanote){
		if (0==$lanote && $lanote<=0.4) {
    		$etoiles = "<span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span>";
    	}
    	if (0.5<=$lanote && $lanote<=1.4) {
    		$etoiles = "<span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span>";
    	}
    	if (1.5<=$lanote && $lanote<=2.4) {
    		$etoiles = "<span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span>";
    	}
    	if (2.5<=$lanote && $lanote<=3.4) {
    		$etoiles = "<span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span>";
    	}
    	if (3.5<=$lanote && $lanote<=4.4) {
    		$etoiles = "<span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star-empty color_note_id \"></span>";
    	}
    	if (4.5<=$lanote && $lanote<=5) {
    		$etoiles = "<span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span><span class=\"glyphicon glyphicon-star color_note_id\"></span>";
    	}
        return $etoiles."  (".$lanote."/5)";
	}


	// Date d'aujourd'hui
	$aujourdhui = date('Y-m-d');
	$ladte = new Datetime($aujourdhui);
	$joursapres = $ladte->modify('+2 day');
    $jour2 = $joursapres->format('d M Y');


    /*GESTION DE PHOTO DE PROFIL*/
    /* Les configs */
    $dossier = '../../assets/images/';
    $taille_max = 3000000;
    $extension_auth = array('jpg', 'jpeg', 'gif', 'png','JPEG','JPG','GIF','PNG');


    if(isset($_POST['profilSend'])){
       
        $infos       =  pathinfo($_FILES['fichier']['name'][0]);
        $fichier     = $_FILES['fichier']['name'][0];
        $fichier_tmp = $_FILES['fichier']['tmp_name'][0];
        $taille      = $_FILES['fichier']['size'][0];
        $extension   = $infos['extension'];

        if(!empty($fichier)){ // Si fichier ne vaut pas null
            
            /* Vérification de sécurité */
            if(preg_match('#php#isU',$fichier)){ // Si un nom de fichier comporte le mot php, on le remplace.
                $fichier = str_replace('php','8sdf',$fichier);
            }
            if(!in_array($extension, $extension_auth)){ //Si l'extension n'est pas dans le tableau.
                $erreur = 'Vous ne pouvez pas uploader un fichier de type .'.$extension.' !!!';
            }
            if($taille>$taille_max){ // Si la taille du fichier est plus grande que la taille maximal.
                $erreur = 'Le fichier ' . $fichier . ' excède la taille maximale autorisée qui est de ' . floor($taille_max/1024/1024) . ' Mo';
            }
            /*************************/
            if(empty($erreur)){ // Si la variable erreur vaut null
                $fichier="profil".date("dmYHis").".".$extension;
                if(session_status() === PHP_SESSION_NONE) session_start();
                if (isset($_SESSION['id_cli'])) {
			        $Clients -> setClients_Photo($_SESSION['id_cli'], $fichier);
			    } 
                if(file_exists($dossier.$fichier)){ $fichier="fic".$i.".".$extension; $i++;}
                if(copy($fichier_tmp, $dossier.$fichier)){ // Si le fichier a bien été transferé.
                    echo '<p>Le fichier '.$fichier.' a bien été transferé.</p>';
                }else{
                    echo '<p>Le fichier '.$fichier.' n\'a pas été transferer sur le serveur, l\'erreur de transfere est incconu.</p>';
                }
            }else{
                echo '<p>'.$erreur.'</p>';
            }
        }
    }



    foreach ($_Reserver as $_RES) {

    	$dateAujourdhui = strtotime(date("Y-m-d H:i:s"));
		$dateDepart = strtotime($_RES['dateHreDepart']);

    	if ($dateDepart<= $dateAujourdhui && $_RES['annulee'] != 1) {
    		//INSERT DANS LA TABLE LOCATION
    		$Location-> setLocation($_RES['numReservation'] ,$_RES['dateHreDepart'], $_RES['montant'], $_RES['dateHreDepart'], $_RES['dateHreRetour'], $_RES['id_cli'], $_RES['immatriculation'], $_RES['lieuDepart'], $_RES['lieuRetour']);

    		//INSERT DANS LA TABLE LOCATION SANS CHAUFFEUR
    		if ($_RES['fsc']=="true") {
    			$Locationsanschauffeur-> setLocationsanschauffeur($_RES['numReservation'] ,$_RES['dateHreDepart'], $_RES['montant'], $_RES['dateHreDepart'], $_RES['dateHreRetour'], $_RES['id_cli'], $_RES['immatriculation'], $_RES['lieuDepart'], $_RES['lieuRetour']);
    		}

    		//INSERT DANS LA TABLE LOCATION AVEC CHAUFFEUR
    		if ($_RES['fsc']=="false") {
    			$Locationavecchauffeur-> setLocationavecchauffeur($_RES['numReservation'] ,$_RES['dateHreDepart'], $_RES['montant'], $_RES['dateHreDepart'], $_RES['dateHreRetour'], $_RES['id_cli'], $_RES['immatriculation'], $_RES['lieuDepart'], $_RES['lieuRetour']);
    		}
    	}
    }
	


