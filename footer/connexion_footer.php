<section class="mbr-section mbr-section--relative mbr-section--fixed-size section_background_footer"  data-rv-view="0">
    
    <div class="mbr-section__container container">
        <div class="mbr-contacts mbr-contacts--wysiwyg row" style="padding-top: 45px; padding-bottom: 45px;">
            <div class="col-sm-4">
                <div><a href="#"><img src="../../assets/images/logo.png" class="mbr-contacts__img mbr-contacts__img--left img img-responsive center-block"></a></div>
            </div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-8 col-xs-8">
                        <p class="mbr-contacts__text">
                            <strong>LVHM</strong><br>
                            L'agence LVHM propose à ses clients des locations sur toutes les villes de la région Normandie avec des trajets prédéfinis (d’un lieu de départ à un lieu de destination) comme par exemple :<br>
                            Le Havre ↔ Rouen, Le Havre ↔ Le Havre, Caen ↔ Rouen, etc...<br>
                            Chaque ville a au moins une agence, dont le nom de l’agence est LVHM.
                            
                        </p>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <p class="mbr-contacts__text">
                            <strong>CONTACT</strong><br>
                            <a role="button" class="btn btn-default" href="../contact/"><span class="glyphicon glyphicon-phone-alt"></span> - <span class="glyphicon glyphicon-envelope"></span></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center" style="color: #ffff;">
            <p><span class="glyphicon glyphicon-copyright-mark"></span> Tous droits réservés 2019 - LVHM</p>
        </div>
    </div>
</section>
