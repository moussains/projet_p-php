Présetation de notre sujet : 

Notre sujet est de créer un projet pour locations de voiture. 
Nous devons donc créer un site internet qui propose à ses clients des locations sur toutes les villes de la région Normandie avec des trajets prédéfinis 
(d’un lieu de départ à un lieu de destination) comme par exemple :
Le Havre ↔ Rouen,
Le Havre ↔ Le Havre,
Caen ↔ Rouen,
etc...
Chaque ville a au moins une agence, dont le nom de l’agence est LVHM.
Donc pour ce projet, nous aurons effectivement besoin d’une base de données qui héberge toutes les données nécessaires à cette application WEB.*
#
#
#
#
Fonctionnement du site : 

Il existe deux interfaces, une interface utilisateur simple (client), et une interface admin pour la gestion du site.

Donc lorsqu'on fait une réservation sans se connecter, cela ne sera pas possible, et la personne sera proposé de se connecter pour établir une réservation.
Et au moment où la personne s'est connecter, elle aura la possibilité de réserver un véhicule. Et si par exemple une réservation à pour un départ le 21/12/2019 à 10h00, 
si la date est arrivé (c'est-à-dire aujourd'hui) alors il créera une location automatiquement. 

Le client connecté peut aussi modifier ses informations à partir de la page "profil" et elle pourra consulter ses réservations(en cours, annulées et passées) et
ses locations (en cours et passées). Si une location est passée, on peut la noter sur 5.

Si on est connecté, on peut également se déconnecter. 
#
#
#
#
Modalité d'accès :

Interface client : moussa@lvhm.fr / moussa1998
Interface admin :  moussains / moussa1998
Les mots de passes sont cryptés en MD5.
#
#
#
#
Base de données : 

La base de données se trouve dans le fichier lvhm.sql
Le connexion à la base s'effectue dans le fichier _server.php
