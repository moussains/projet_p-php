<?php
    require_once('../classes.php');
    VerifSessionOn();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Inscrire - LVHM</title>
    <?php if (isset($_POST['inscription']) && !empty($TrueORfalse)) { echo '<meta http-equiv="refresh" content="4; URL=../connecter">'; }?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../assets/create/style.css">
</head>

<body >
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="../"><img src="../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="../">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../contact/"><span class="glyphicon glyphicon-earphone"></span>  CONTACT</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white active" href="./"><span class="glyphicon glyphicon-plus-sign"></span> S'INSCRIRE</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../connecter/"><span class="glyphicon glyphicon-user"></span> SE CONNECTER</a></li>
                            </ul>                            
                            
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine">
    <a href="#">css templates</a>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" data-rv-view="7" > 
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <h2 class="text-center">INSCRIPTION</h2>
        <br><br>
        <div class="mbr-section__row row">
            <div class="thumbnail shadow col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <?php if (isset($_POST['inscription'])) { echo $reponseInsertion; } ?>
                <form action="./" method="POST">
                    <div class="col-sm-12 col-md-12">
                        <h4 class="text-center"><ins>Données personnelles du conducteur principal</ins></h4>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12 col-xs-12" for="sexe">Civilité :</label>
                                <select class="col-sm-12 col-md-12 col-xs-12" id="sexe" name="sexe" required>
                                    <option></option>
                                    <option value="M">Monsieur</option>
                                    <option value="F">Madame</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="nom">Nom :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['nom'];} ?>" type="text" id="nom" name="nom" placeholder="Entrez votre nom" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="prenom">Prénom :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['prenom'];} ?>" type="text" id="prenom" name="prenom" placeholder="Entrez votre prénom" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="date_n">Date de naissance :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['date_n'];} ?>" type="date" id="date_n" name="date_n" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="email">Adresse e-mail :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['email'];} ?>" type="email" id="email" name="email" placeholder="azerty@xyz.com" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="adresse">Adresse :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['adresse'];} ?>" type="text" id="adresse" name="adresse" placeholder="Entrez votre adresse" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="cp">Code postal :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['cp'];} ?>" type="text" id="cp" name="cp" placeholder="Ex : 76600" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="ville">Ville :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['ville'];} ?>" type="text" id="ville" name="ville" placeholder="Le Havre" required>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12 col-xs-12" for="pays">Pays :</label>
                                <select class="col-sm-12 col-md-12 col-xs-12" id="pays" name="pays" required>
                                    <?php foreach ($_Pays as $paysFr) { ?>
                                        <?php if ($paysFr['id_pays'] == 75) { ?>
                                            <option value="<?php echo $paysFr['id_pays'] ?>" selected><?php echo $paysFr['nom_fr_fr']; ?></option>
                                        <?php }else{?>
                                            <option value="<?php echo $paysFr['id_pays'] ?>"><?php echo $paysFr['nom_fr_fr']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="tel">Téléphone :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['tel'];} ?>" type="text" id="tel" name="tel" placeholder="Ex : 0768266541" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12"> 
                        <h4 class="text-center"><ins>Votre mot de passe</ins></h4>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="pwd">Mot de passe :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['pwd'];} ?>" type="password" id="pwd" name="pwd" placeholder="Entrez un mot de passe" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="pwd_confirm">Confirmation de mot de passe :</label>
                                <input class="col-sm-12 col-md-12" value="<?php if(empty($TrueORfalse) && isset($_POST['inscription'])){echo $_POST['pwd_confirm'];} ?>" type="password" id="pwd_confirm" value="" name="pwd_confirm" placeholder="Confirmez votre mot de passe" required>
                            </div>
                        </div>
                    </div>
                    <p class="text-center">Déjà inscrit ? <a>Se connecter</a></p>
                    <button type="submit" name="inscription" class="col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 col-xs-offset-3 col-xs-6 btn btn-default etape">S'incrire</button>      
                </form>
            </div>                 
        </div><br><br><br><br>  
    </div>
</section>

<?php
    include("../footer/other_footer.php");
?>


<script src="../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../assets/jarallax/jarallax.js"></script>
<script src="../assets/mobirise/js/script.js"></script>

</body>
</html>