SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DROP TABLE IF EXISTS `agence`;
CREATE TABLE IF NOT EXISTS `agence` (
  `numAgence` int(11) NOT NULL AUTO_INCREMENT,
  `ville` varchar(50) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `cp` char(5) NOT NULL,
  PRIMARY KEY (`numAgence`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO `agence` (`numAgence`, `ville`, `adresse`, `cp`) VALUES
(1, 'Le Havre', '112 Rue Jules Lecesne', '76600'),
(2, 'Rouen', '4 Rue des Basnages', '76000'),
(3, 'Caen', '168 Rue Caponière', '14000'),
(4, 'Cherbourg-Octeville', '42 Rue François La Vieille', '50130'),
(5, 'Cherbourg-en-Cotentin', '34 rue émile bertin', '50100'),
(6, 'Évreux', '45 Rue Jean Jaurès', '27000'),
(7, 'Dieppe', '125 Avenue des Canadiens', '76200'),
(8, 'Saint-Étienne-du-Rouvray', '1 Bis Avenue du Val l\'Abbé', '76800'),
(9, 'Sotteville-lès-Rouen', '292 Rue Victor Hugo', '76300'),
(10, 'Alençon', '31 Rue Maréchal Jean-Marie de Lattre de Tassigny', '61000'),
(11, 'Le Grand-Quevilly', '26 Boulevard Pierre Brossolette', '76120'),
(12, 'Vernon', '13 Place de la République', '27200'),
(13, 'Le Havre', '70 Rue Charles Laffitte', '76600');

DROP TABLE IF EXISTS `agence_vehicule`;
CREATE TABLE IF NOT EXISTS `agence_vehicule` (
  `immatriculation` varchar(50) NOT NULL,
  `numAgence` int(11) NOT NULL,
  PRIMARY KEY (`immatriculation`,`numAgence`),
  KEY `agence_vehicule_agence0_FK` (`numAgence`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `agence_vehicule` (`immatriculation`, `numAgence`) VALUES
('AA-266-AE', 1),
('AE-200-TE', 1),
('LD-642-ML', 1),
('MQ-262-DG', 1),
('LI-744-KS', 2),
('OW-005-LS', 2),
('PM-393-AS', 2),
('PS-707-AS', 2),
('AZ-702-MK', 3),
('DQ-322-ML', 3),
('ML-042-DJ', 3),
('SH-175-SC', 3),
('HJ-410-DV', 4),
('MS-010-UT', 4),
('PC-200-JK', 4),
('PS-753-ML', 4),
('DJ-140-FS', 5),
('GS-541-ZC', 5),
('NV-320-SQ', 5),
('PG-754-AE', 5),
('CL-879-AE', 6),
('LD-642-ML', 6),
('LD-771-LK', 6),
('NL-355-SQ', 6),
('CD-224-AE', 7),
('DB-151-VS', 7),
('MO-500-JS', 7),
('PA-721-OI', 7),
('CL-801-ZS', 8),
('FV-954-SQ', 8),
('GD-564-HU', 8),
('SD-057-GE', 8),
('FM-650-SQ', 9),
('NV-321-NK', 9),
('SH-175-SC', 9),
('SS-419-HS', 9),
('AE-266-AA', 10),
('AZ-660-HH', 10),
('CB-114-AZ', 10),
('CB-852-ER', 10),
('BV-754-SQ', 11),
('HH-741-CD', 11),
('PM-302-SS', 11),
('ZG-752-MS', 11),
('MM-333-AE', 12),
('MS-014-AZ', 12),
('PO-766-JD', 12),
('XS-002-ML', 12),
('DJ-010-QJ', 13),
('OD-110-DJ', 13),
('RU-743-TU', 13),
('WQ-272-QQ', 13);

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `numCat` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) NOT NULL,
  `photoCat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`numCat`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `categorie` (`numCat`, `libelle`, `photoCat`) VALUES
(1, 'Catégorie citadine', 'cat_citadine.png'),
(2, 'Catégorie compacte', 'cat_compact.png'),
(3, 'Classe intermédiaire', 'cat_intermediate.png'),
(4, 'Classe supérieure', 'cat_superieure.png');

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_cli` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `dateN` date DEFAULT NULL,
  `sexe` varchar(250) DEFAULT NULL,
  `mail` varchar(50) NOT NULL,
  `numPermis` int(11) DEFAULT NULL,
  `tel` char(14) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `cp` char(5) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `id_pays` int(11) NOT NULL,
  PRIMARY KEY (`id_cli`),
  KEY `Client_Pays_FK` (`id_pays`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `client` (`id_cli`, `nom`, `prenom`, `dateN`, `sexe`, `mail`, `numPermis`, `tel`, `adresse`, `cp`, `ville`, `mdp`, `photo`, `id_pays`) VALUES
(1, 'INSSA', 'Moussa', '1998-01-02', 'M', 'moussa@lvhm.fr', 516579663, '07 68 32 36 99', '4 rue michel dubosc', '76600', 'Le Havre', '0bad5c5baec69b7e7b88b263c7e8023d', 'profil08122019180159.jpg', 75),
(2, 'INSSA', 'Insa', NULL, NULL, 'inssa.insa976@outlook.fr', 516579663, '07 68 32 36 99', '4 rue michel dubosc', '76600', 'Le Havre', '78551ed262f2268af7a3c58cc8843485', NULL, 75),
(3, 'Moussa', 'MOusaz', '2019-11-06', 'M', 'moudhzd@efike.Fr', NULL, '865254245', '6556 frfg', '76600', 'Le Havre', '639583119441bd84c373c314afd2814d', NULL, 75);

DROP TABLE IF EXISTS `etre_proposee`;
CREATE TABLE IF NOT EXISTS `etre_proposee` (
  `id_mdl` int(11) NOT NULL,
  `id_f` int(11) NOT NULL,
  `tarif` float NOT NULL,
  PRIMARY KEY (`id_mdl`,`id_f`),
  KEY `etre_proposee_FormuleSansChauffeur0_FK` (`id_f`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `etre_proposee` (`id_mdl`, `id_f`, `tarif`) VALUES
(1, 1, 21),
(2, 2, 21),
(3, 3, 22),
(4, 4, 25),
(5, 5, 24),
(6, 6, 25),
(7, 7, 26),
(8, 8, 26),
(9, 9, 27),
(10, 10, 28),
(11, 11, 28),
(12, 12, 28);

DROP TABLE IF EXISTS `formulaireavecchauffeur`;
CREATE TABLE IF NOT EXISTS `formulaireavecchauffeur` (
  `id_f` int(11) NOT NULL,
  `lieuDestination` varchar(50) DEFAULT NULL,
  `tarif` float NOT NULL,
  `descriptionFAC` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_f`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `formule`;
CREATE TABLE IF NOT EXISTS `formule` (
  `id_f` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_f`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `formule` (`id_f`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12);

DROP TABLE IF EXISTS `formulesanschauffeur`;
CREATE TABLE IF NOT EXISTS `formulesanschauffeur` (
  `id_f` int(11) NOT NULL,
  `nbKmInclus` int(11) NOT NULL,
  PRIMARY KEY (`id_f`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `formulesanschauffeur` (`id_f`, `nbKmInclus`) VALUES
(1, 250),
(2, 275),
(3, 235),
(4, 250),
(5, 240),
(6, 245),
(7, 260),
(8, 255),
(9, 250),
(10, 260),
(11, 255),
(12, 265);

DROP TABLE IF EXISTS `location`;
CREATE TABLE IF NOT EXISTS `location` (
  `numLocation` int(11) NOT NULL,
  `dateLocation` datetime NOT NULL,
  `montantRegle` float NOT NULL,
  `dateHreDepartPrevu` datetime NOT NULL,
  `dateHreDepartReel` datetime DEFAULT NULL,
  `dateHreRetourPrevu` datetime NOT NULL,
  `dateHreRetourReel` datetime DEFAULT NULL,
  `id_cli` int(11) NOT NULL,
  `immatriculation` varchar(50) NOT NULL,
  `numAgence` int(11) NOT NULL,
  `numAgence_retourner` int(11) NOT NULL,
  PRIMARY KEY (`numLocation`),
  KEY `Location_Client_FK` (`id_cli`),
  KEY `Location_Vehicule0_FK` (`immatriculation`),
  KEY `Location_agence1_FK` (`numAgence`),
  KEY `Location_agence2_FK` (`numAgence_retourner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `location` (`numLocation`, `dateLocation`, `montantRegle`, `dateHreDepartPrevu`, `dateHreDepartReel`, `dateHreRetourPrevu`, `dateHreRetourReel`, `id_cli`, `immatriculation`, `numAgence`, `numAgence_retourner`) VALUES
(41578951, '2019-12-08 16:48:00', 126, '2019-12-08 16:48:00', NULL, '2019-12-09 16:46:00', NULL, 1, 'PS-753-ML', 4, 4),
(91243877, '2019-12-08 17:42:00', 175, '2019-12-08 17:42:00', NULL, '2019-12-09 19:32:00', NULL, 1, 'DJ-010-QJ', 13, 5);

DROP TABLE IF EXISTS `locationavecchauffeur`;
CREATE TABLE IF NOT EXISTS `locationavecchauffeur` (
  `numLocation` int(11) NOT NULL,
  `dateLocation` datetime NOT NULL,
  `montantRegle` float NOT NULL,
  `dateHreDepartPrevu` datetime NOT NULL,
  `dateHreDepartReel` datetime DEFAULT NULL,
  `dateHreRetourPrevu` datetime NOT NULL,
  `dateHreRetourReel` datetime DEFAULT NULL,
  `id_f` int(11) DEFAULT NULL,
  `id_cli` int(11) NOT NULL,
  `immatriculation` varchar(50) NOT NULL,
  `numAgence` int(11) NOT NULL,
  `numAgence_retourner` int(11) NOT NULL,
  PRIMARY KEY (`numLocation`),
  KEY `LocationAvecChauffeur_FormulaireAvecChauffeur0_FK` (`id_f`),
  KEY `LocationAvecChauffeur_Client1_FK` (`id_cli`),
  KEY `LocationAvecChauffeur_Vehicule2_FK` (`immatriculation`),
  KEY `LocationAvecChauffeur_agence3_FK` (`numAgence`),
  KEY `LocationAvecChauffeur_agence4_FK` (`numAgence_retourner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `locationsanschauffeur`;
CREATE TABLE IF NOT EXISTS `locationsanschauffeur` (
  `numLocation` int(11) NOT NULL,
  `nbKmDepart` int(11) DEFAULT NULL,
  `nbKmRetour` int(11) DEFAULT NULL,
  `dateLocation` datetime NOT NULL,
  `montantRegle` float NOT NULL,
  `dateHreDepartPrevu` datetime NOT NULL,
  `dateHreDepartReel` datetime DEFAULT NULL,
  `dateHreRetourPrevu` datetime NOT NULL,
  `dateHreRetourReel` datetime DEFAULT NULL,
  `id_f` int(11) DEFAULT NULL,
  `id_cli` int(11) NOT NULL,
  `immatriculation` varchar(50) NOT NULL,
  `numAgence` int(11) NOT NULL,
  `numAgence_retourner` int(11) NOT NULL,
  PRIMARY KEY (`numLocation`),
  KEY `LocationSansChauffeur_FormuleSansChauffeur0_FK` (`id_f`),
  KEY `LocationSansChauffeur_Client1_FK` (`id_cli`),
  KEY `LocationSansChauffeur_Vehicule2_FK` (`immatriculation`),
  KEY `LocationSansChauffeur_agence3_FK` (`numAgence`),
  KEY `LocationSansChauffeur_agence4_FK` (`numAgence_retourner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `modele`;
CREATE TABLE IF NOT EXISTS `modele` (
  `id_mdl` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `km` int(11) NOT NULL,
  `tarifKmSupplementaire` float NOT NULL,
  `nbPlaces` int(11) NOT NULL,
  `vitesseMax` int(11) NOT NULL,
  `description` text NOT NULL,
  `avecPermis` tinyint(1) NOT NULL,
  `boiteManuelle` tinyint(1) NOT NULL,
  `nbPortes` int(11) NOT NULL,
  `photoModele` varchar(255) DEFAULT NULL,
  `numCat` int(11) NOT NULL,
  PRIMARY KEY (`id_mdl`),
  KEY `Modele_categorie_FK` (`numCat`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `modele` (`id_mdl`, `nom`, `km`, `tarifKmSupplementaire`, `nbPlaces`, `vitesseMax`, `description`, `avecPermis`, `boiteManuelle`, `nbPortes`, `photoModele`, `numCat`) VALUES
(1, 'FIAT 500', 110000, 0.3, 4, 200, 'Voiture toute neuf !', 1, 1, 5, 'fiat-500.jpg', 1),
(2, 'Twingo', 100000, 0.3, 4, 240, 'Voiture toute neuf !', 1, 0, 3, 'twingo.jpg', 1),
(3, 'Clio 4', 120000, 0.4, 5, 260, 'Voiture toute neuf !', 1, 1, 5, 'clio-4.jpeg', 1),
(4, 'Golf 5', 130000, 0.5, 5, 270, 'Voiture toute neuf !', 1, 1, 5, 'golf-5.png', 2),
(5, 'Passat', 120000, 0.5, 5, 270, 'Voiture toute neuf !', 1, 1, 5, 'passat.png', 2),
(6, '308', 120000, 0.6, 5, 270, 'Voiture toute neuf !', 1, 1, 5, '308.jpg', 2),
(7, 'S90', 130000, 0.7, 5, 270, 'Voiture toute neuf !', 1, 1, 5, 's90-volvo.jpeg', 3),
(8, '508', 120000, 0.7, 5, 270, 'Voiture toute neuf !', 1, 1, 5, '508.png', 3),
(9, 'scirocco', 120000, 0.7, 2, 280, 'Voiture toute neuf !', 1, 1, 3, 'scirocco.jpg', 3),
(10, 'XJ', 130000, 0.9, 5, 280, 'Voiture toute neuf !', 1, 1, 5, 'xj.png', 4),
(11, 'Classe S', 120000, 0.9, 5, 290, 'Voiture toute neuf !', 1, 1, 5, 'classe-s.png', 4),
(12, 'A8', 120000, 0.9, 2, 280, 'Voiture toute neuf !', 1, 1, 3, 'audi-8.png', 4);

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `id_note` int(11) NOT NULL AUTO_INCREMENT,
  `note` int(11) NOT NULL,
  PRIMARY KEY (`id_note`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO `note` (`id_note`, `note`) VALUES
(1, 0),
(2, 1),
(3, 2),
(4, 3),
(5, 4),
(6, 5);

DROP TABLE IF EXISTS `note_location`;
CREATE TABLE IF NOT EXISTS `note_location` (
  `numLocation` int(11) NOT NULL,
  `id_note` int(11) NOT NULL,
  `description` text NOT NULL,
  `dates` datetime NOT NULL,
  PRIMARY KEY (`numLocation`,`id_note`),
  KEY `note_location_note0_FK` (`id_note`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `note_location` (`numLocation`, `id_note`, `description`, `dates`) VALUES
(41578951, 5, 'Une journée de location. Bien qu\'il ait neigé et que n\'ayant pas eu de pneus neige, le véhicule était de bonne tenue et propre.', '2019-12-09 18:54:30'),
(91243877, 4, 'Personnel très professionnel !! Bon accueil. Bonne qualité de service sans oublier le café gentiment offert.\r\n\r\nJe recommande fortement l\'agence LVHM.', '2019-12-09 22:47:00');

DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `id_pays` int(11) NOT NULL AUTO_INCREMENT,
  `nom_en_gb` varchar(50) NOT NULL,
  `nom_fr_fr` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pays`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=latin1;

INSERT INTO `pays` (`id_pays`, `nom_en_gb`, `nom_fr_fr`) VALUES
(1, 'Afghanistan', 'Afghanistan'),
(2, 'Albania', 'Albanie'),
(3, 'Antarctica', 'Antarctique'),
(4, 'Algeria', 'Algérie'),
(5, 'American Samoa', 'Samoa Américaines'),
(6, 'Andorra', 'Andorre'),
(7, 'Angola', 'Angola'),
(8, 'Antigua and Barbuda', 'Antigua-et-Barbuda'),
(9, 'Azerbaijan', 'Azerbaïdjan'),
(10, 'Argentina', 'Argentine'),
(11, 'Australia', 'Australie'),
(12, 'Austria', 'Autriche'),
(13, 'Bahamas', 'Bahamas'),
(14, 'Bahrain', 'Bahreïn'),
(15, 'Bangladesh', 'Bangladesh'),
(16, 'Armenia', 'Arménie'),
(17, 'Barbados', 'Barbade'),
(18, 'Belgium', 'Belgique'),
(19, 'Bermuda', 'Bermudes'),
(20, 'Bhutan', 'Bhoutan'),
(21, 'Bolivia', 'Bolivie'),
(22, 'Bosnia and Herzegovina', 'Bosnie-Herzégovine'),
(23, 'Botswana', 'Botswana'),
(24, 'Bouvet Island', 'Île Bouvet'),
(25, 'Brazil', 'Brésil'),
(26, 'Belize', 'Belize'),
(27, 'British Indian Ocean Territory', 'Territoire Britannique de l\'Océan Indien'),
(28, 'Solomon Islands', 'Îles Salomon'),
(29, 'British Virgin Islands', 'Îles Vierges Britanniques'),
(30, 'Brunei Darussalam', 'Brunéi Darussalam'),
(31, 'Bulgaria', 'Bulgarie'),
(32, 'Myanmar', 'Myanmar'),
(33, 'Burundi', 'Burundi'),
(34, 'Belarus', 'Bélarus'),
(35, 'Cambodia', 'Cambodge'),
(36, 'Cameroon', 'Cameroun'),
(37, 'Canada', 'Canada'),
(38, 'Cape Verde', 'Cap-vert'),
(39, 'Cayman Islands', 'Îles Caïmanes'),
(40, 'Central African', 'République Centrafricaine'),
(41, 'Sri Lanka', 'Sri Lanka'),
(42, 'Chad', 'Tchad'),
(43, 'Chile', 'Chili'),
(44, 'China', 'Chine'),
(45, 'Taiwan', 'Taïwan'),
(46, 'Christmas Island', 'Île Christmas'),
(47, 'Cocos (Keeling) Islands', 'Îles Cocos (Keeling)'),
(48, 'Colombia', 'Colombie'),
(49, 'Comoros', 'Comores'),
(50, 'Mayotte', 'Mayotte'),
(51, 'Republic of the Congo', 'République du Congo'),
(52, 'The Democratic Republic Of The Congo', 'République Démocratique du Congo'),
(53, 'Cook Islands', 'Îles Cook'),
(54, 'Costa Rica', 'Costa Rica'),
(55, 'Croatia', 'Croatie'),
(56, 'Cuba', 'Cuba'),
(57, 'Cyprus', 'Chypre'),
(58, 'Czech Republic', 'République Tchèque'),
(59, 'Benin', 'Bénin'),
(60, 'Denmark', 'Danemark'),
(61, 'Dominica', 'Dominique'),
(62, 'Dominican Republic', 'République Dominicaine'),
(63, 'Ecuador', 'Équateur'),
(64, 'El Salvador', 'El Salvador'),
(65, 'Equatorial Guinea', 'Guinée Équatoriale'),
(66, 'Ethiopia', 'Éthiopie'),
(67, 'Eritrea', 'Érythrée'),
(68, 'Estonia', 'Estonie'),
(69, 'Faroe Islands', 'Îles Féroé'),
(70, 'Falkland Islands', 'Îles (malvinas) Falkland'),
(71, 'South Georgia and the South Sandwich Islands', 'Géorgie du Sud et les Îles Sandwich du Sud'),
(72, 'Fiji', 'Fidji'),
(73, 'Finland', 'Finlande'),
(74, 'Åland Islands', 'Îles Åland'),
(75, 'France', 'France'),
(76, 'French Guiana', 'Guyane Française'),
(77, 'French Polynesia', 'Polynésie Française'),
(78, 'French Southern Territories', 'Terres Australes Françaises'),
(79, 'Djibouti', 'Djibouti'),
(80, 'Gabon', 'Gabon'),
(81, 'Georgia', 'Géorgie'),
(82, 'Gambia', 'Gambie'),
(83, 'Occupied Palestinian Territory', 'Territoire Palestinien Occupé'),
(84, 'Germany', 'Allemagne'),
(85, 'Ghana', 'Ghana'),
(86, 'Gibraltar', 'Gibraltar'),
(87, 'Kiribati', 'Kiribati'),
(88, 'Greece', 'Grèce'),
(89, 'Greenland', 'Groenland'),
(90, 'Grenada', 'Grenade'),
(91, 'Guadeloupe', 'Guadeloupe'),
(92, 'Guam', 'Guam'),
(93, 'Guatemala', 'Guatemala'),
(94, 'Guinea', 'Guinée'),
(95, 'Guyana', 'Guyana'),
(96, 'Haiti', 'Haïti'),
(97, 'Heard Island and McDonald Islands', 'Îles Heard et Mcdonald'),
(98, 'Vatican City State', 'Saint-Siège (état de la Cité du Vatican)'),
(99, 'Honduras', 'Honduras'),
(100, 'Hong Kong', 'Hong-Kong'),
(101, 'Hungary', 'Hongrie'),
(102, 'Iceland', 'Islande'),
(103, 'India', 'Inde'),
(104, 'Indonesia', 'Indonésie'),
(105, 'Islamic Republic of Iran', 'République Islamique d\'Iran'),
(106, 'Iraq', 'Iraq'),
(107, 'Ireland', 'Irlande'),
(108, 'Israel', 'Israël'),
(109, 'Italy', 'Italie'),
(110, 'Côte d\'Ivoire', 'Côte d\'Ivoire'),
(111, 'Jamaica', 'Jamaïque'),
(112, 'Japan', 'Japon'),
(113, 'Kazakhstan', 'Kazakhstan'),
(114, 'Jordan', 'Jordanie'),
(115, 'Kenya', 'Kenya'),
(116, 'Democratic People\'s Republic of Korea', 'République Populaire Démocratique de Corée'),
(117, 'Republic of Korea', 'République de Corée'),
(118, 'Kuwait', 'Koweït'),
(119, 'Kyrgyzstan', 'Kirghizistan'),
(120, 'Lao People\'s Democratic Republic', 'République Démocratique Populaire Lao'),
(121, 'Lebanon', 'Liban'),
(122, 'Lesotho', 'Lesotho'),
(123, 'Latvia', 'Lettonie'),
(124, 'Liberia', 'Libéria'),
(125, 'Libyan Arab Jamahiriya', 'Jamahiriya Arabe Libyenne'),
(126, 'Liechtenstein', 'Liechtenstein'),
(127, 'Lithuania', 'Lituanie'),
(128, 'Luxembourg', 'Luxembourg'),
(129, 'Macao', 'Macao'),
(130, 'Madagascar', 'Madagascar'),
(131, 'Malawi', 'Malawi'),
(132, 'Malaysia', 'Malaisie'),
(133, 'Maldives', 'Maldives'),
(134, 'Mali', 'Mali'),
(135, 'Malta', 'Malte'),
(136, 'Martinique', 'Martinique'),
(137, 'Mauritania', 'Mauritanie'),
(138, 'Mauritius', 'Maurice'),
(139, 'Mexico', 'Mexique'),
(140, 'Monaco', 'Monaco'),
(141, 'Mongolia', 'Mongolie'),
(142, 'Republic of Moldova', 'République de Moldova'),
(143, 'Montserrat', 'Montserrat'),
(144, 'Morocco', 'Maroc'),
(145, 'Mozambique', 'Mozambique'),
(146, 'Oman', 'Oman'),
(147, 'Namibia', 'Namibie'),
(148, 'Nauru', 'Nauru'),
(149, 'Nepal', 'Népal'),
(150, 'Netherlands', 'Pays-Bas'),
(151, 'Netherlands Antilles', 'Antilles Néerlandaises'),
(152, 'Aruba', 'Aruba'),
(153, 'New Caledonia', 'Nouvelle-Calédonie'),
(154, 'Vanuatu', 'Vanuatu'),
(155, 'New Zealand', 'Nouvelle-Zélande'),
(156, 'Nicaragua', 'Nicaragua'),
(157, 'Niger', 'Niger'),
(158, 'Nigeria', 'Nigéria'),
(159, 'Niue', 'Niué'),
(160, 'Norfolk Island', 'Île Norfolk'),
(161, 'Norway', 'Norvège'),
(162, 'Northern Mariana Islands', 'Îles Mariannes du Nord'),
(163, 'United States Minor Outlying Islands', 'Îles Mineures Éloignées des États-Unis'),
(164, 'Federated States of Micronesia', 'États Fédérés de Micronésie'),
(165, 'Marshall Islands', 'Îles Marshall'),
(166, 'Palau', 'Palaos'),
(167, 'Pakistan', 'Pakistan'),
(168, 'Panama', 'Panama'),
(169, 'Papua New Guinea', 'Papouasie-Nouvelle-Guinée'),
(170, 'Paraguay', 'Paraguay'),
(171, 'Peru', 'Pérou'),
(172, 'Philippines', 'Philippines'),
(173, 'Pitcairn', 'Pitcairn'),
(174, 'Poland', 'Pologne'),
(175, 'Portugal', 'Portugal'),
(176, 'Guinea-Bissau', 'Guinée-Bissau'),
(177, 'Timor-Leste', 'Timor-Leste'),
(178, 'Puerto Rico', 'Porto Rico'),
(179, 'Qatar', 'Qatar'),
(180, 'Réunion', 'Réunion'),
(181, 'Romania', 'Roumanie'),
(182, 'Russian Federation', 'Fédération de Russie'),
(183, 'Rwanda', 'Rwanda'),
(184, 'Saint Helena', 'Sainte-Hélène'),
(185, 'Saint Kitts and Nevis', 'Saint-Kitts-et-Nevis'),
(186, 'Anguilla', 'Anguilla'),
(187, 'Saint Lucia', 'Sainte-Lucie'),
(188, 'Saint-Pierre and Miquelon', 'Saint-Pierre-et-Miquelon'),
(189, 'Saint Vincent and the Grenadines', 'Saint-Vincent-et-les Grenadines'),
(190, 'San Marino', 'Saint-Marin'),
(191, 'Sao Tome and Principe', 'Sao Tomé-et-Principe'),
(192, 'Saudi Arabia', 'Arabie Saoudite'),
(193, 'Senegal', 'Sénégal'),
(194, 'Seychelles', 'Seychelles'),
(195, 'Sierra Leone', 'Sierra Leone'),
(196, 'Singapore', 'Singapour'),
(197, 'Slovakia', 'Slovaquie'),
(198, 'Vietnam', 'Viet Nam'),
(199, 'Slovenia', 'Slovénie'),
(200, 'Somalia', 'Somalie'),
(201, 'South Africa', 'Afrique du Sud'),
(202, 'Zimbabwe', 'Zimbabwe'),
(203, 'Spain', 'Espagne'),
(204, 'Western Sahara', 'Sahara Occidental'),
(205, 'Sudan', 'Soudan'),
(206, 'Suriname', 'Suriname'),
(207, 'Svalbard and Jan Mayen', 'Svalbard etÎle Jan Mayen'),
(208, 'Swaziland', 'Swaziland'),
(209, 'Sweden', 'Suède'),
(210, 'Switzerland', 'Suisse'),
(211, 'Syrian Arab Republic', 'République Arabe Syrienne'),
(212, 'Tajikistan', 'Tadjikistan'),
(213, 'Thailand', 'Thaïlande'),
(214, 'Togo', 'Togo'),
(215, 'Tokelau', 'Tokelau'),
(216, 'Tonga', 'Tonga'),
(217, 'Trinidad and Tobago', 'Trinité-et-Tobago'),
(218, 'United Arab Emirates', 'Émirats Arabes Unis'),
(219, 'Tunisia', 'Tunisie'),
(220, 'Turkey', 'Turquie'),
(221, 'Turkmenistan', 'Turkménistan'),
(222, 'Turks and Caicos Islands', 'Îles Turks et Caïques'),
(223, 'Tuvalu', 'Tuvalu'),
(224, 'Uganda', 'Ouganda'),
(225, 'Ukraine', 'Ukraine'),
(226, 'The Former Yugoslav Republic of Macedonia', 'L\'ex-République Yougoslave de Macédoine'),
(227, 'Egypt', 'Égypte'),
(228, 'United Kingdom', 'Royaume-Uni'),
(229, 'Isle of Man', 'Île de Man'),
(230, 'United Republic Of Tanzania', 'République-Unie de Tanzanie'),
(231, 'United States', 'États-Unis'),
(232, 'U.S. Virgin Islands', 'Îles Vierges des États-Unis'),
(233, 'Burkina Faso', 'Burkina Faso'),
(234, 'Uruguay', 'Uruguay'),
(235, 'Uzbekistan', 'Ouzbékistan'),
(236, 'Venezuela', 'Venezuela'),
(237, 'Wallis and Futuna', 'Wallis et Futuna'),
(238, 'Samoa', 'Samoa'),
(239, 'Yemen', 'Yémen'),
(240, 'Serbia and Montenegro', 'Serbie-et-Monténégro'),
(241, 'Zambia', 'Zambie');

DROP TABLE IF EXISTS `reserver`;
CREATE TABLE IF NOT EXISTS `reserver` (
  `immatriculation` varchar(50) NOT NULL,
  `id_cli` int(11) NOT NULL,
  `numReservation` int(11) NOT NULL,
  `dateReservation` datetime NOT NULL,
  `dateHreDepart` datetime NOT NULL,
  `dateHreRetour` datetime NOT NULL,
  `montant` float DEFAULT NULL,
  `lieuDepart` int(11) NOT NULL,
  `lieuRetour` int(11) NOT NULL,
  `annulation` tinyint(1) NOT NULL,
  `annulee` tinyint(1) DEFAULT NULL,
  `fsc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`immatriculation`,`id_cli`),
  KEY `reserver_Client0_FK` (`id_cli`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `identifiant` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `photo` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `utilisateur` (`id`, `nom`, `mail`, `identifiant`, `mdp`, `photo`) VALUES
(1, 'Moussa INSSA', 'moussainssa@outlook.fr', 'moussains', '0bad5c5baec69b7e7b88b263c7e8023d', 'photo_moussa.png'),
(2, 'Hajar M’DAGHRI', 'mdaghri.hajar1@gmail.com', 'hajar1', 'hajarAAAA', 'photo_hajar.png');

DROP TABLE IF EXISTS `vehicule`;
CREATE TABLE IF NOT EXISTS `vehicule` (
  `immatriculation` varchar(50) NOT NULL,
  `dateAchat` date NOT NULL,
  `disponibilite` tinyint(1) NOT NULL,
  `id_mdl` int(11) NOT NULL,
  PRIMARY KEY (`immatriculation`),
  KEY `Vehicule_Modele_FK` (`id_mdl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `vehicule` (`immatriculation`, `dateAchat`, `disponibilite`, `id_mdl`) VALUES
('AA-266-AE', '2016-05-03', 1, 1),
('AE-200-TE', '2016-05-03', 1, 7),
('AE-266-AA', '2016-05-03', 1, 3),
('AZ-660-HH', '2007-02-06', 1, 5),
('AZ-702-MK', '2017-02-25', 1, 5),
('BV-754-SQ', '2017-02-25', 1, 1),
('CB-114-AZ', '2016-05-03', 1, 6),
('CB-852-ER', '2014-02-03', 1, 10),
('CD-224-AE', '2007-02-06', 1, 2),
('CL-801-ZS', '2007-02-06', 1, 8),
('CL-879-AE', '2007-02-06', 1, 4),
('DB-151-VS', '2017-02-25', 1, 12),
('DH-808-BS', '2007-02-06', 1, 11),
('DJ-010-QJ', '2014-02-03', 0, 6),
('DJ-140-FS', '2017-02-25', 1, 6),
('DQ-322-ML', '2014-02-03', 1, 1),
('FM-650-SQ', '2017-02-25', 1, 7),
('FV-954-SQ', '2017-02-25', 1, 3),
('GD-564-HU', '2016-05-03', 1, 9),
('GS-541-ZC', '2007-02-06', 1, 9),
('HH-741-CD', '2016-05-03', 1, 5),
('HJ-410-DV', '2007-02-06', 1, 12),
('LD-642-ML', '2014-02-03', 1, 3),
('LD-771-LK', '2014-02-03', 1, 5),
('LI-744-KS', '2014-02-03', 1, 12),
('ML-042-DJ', '2017-02-25', 1, 11),
('MM-333-AE', '2016-05-03', 1, 2),
('MO-500-JS', '2014-02-03', 1, 11),
('MQ-262-DG', '2016-05-03', 1, 10),
('MS-010-UT', '2014-02-03', 1, 8),
('MS-014-AZ', '2014-02-03', 1, 4),
('NL-355-SQ', '2017-02-25', 1, 2),
('NV-320-SQ', '2017-02-25', 0, 8),
('NV-321-NK', '2017-02-25', 1, 4),
('OD-110-DJ', '2016-05-03', 1, 7),
('OW-005-LS', '2017-02-25', 1, 10),
('PA-721-OI', '2007-02-06', 1, 10),
('PC-200-JK', '2016-05-03', 1, 11),
('PG-754-AE', '2007-02-06', 1, 1),
('PG-757-GE', '2007-02-06', 1, 3),
('PM-302-SS', '2016-05-03', 1, 8),
('PM-393-AS', '2016-05-03', 1, 4),
('PO-766-JD', '2007-02-06', 1, 6),
('PS-707-AS', '2014-02-03', 1, 6),
('PS-753-ML', '2014-02-03', 0, 2),
('RU-743-TU', '2016-05-03', 1, 8),
('SD-057-GE', '2007-02-06', 1, 7),
('SH-175-SC', '2014-02-03', 1, 9),
('SS-419-HS', '2017-02-25', 1, 9),
('WQ-272-QQ', '2007-02-06', 1, 2),
('XS-002-ML', '2014-02-03', 1, 7),
('ZG-752-MS', '2016-05-03', 1, 12);


ALTER TABLE `agence_vehicule`
  ADD CONSTRAINT `agence_vehicule_Vehicule_FK` FOREIGN KEY (`immatriculation`) REFERENCES `vehicule` (`immatriculation`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `agence_vehicule_agence0_FK` FOREIGN KEY (`numAgence`) REFERENCES `agence` (`numAgence`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `client`
  ADD CONSTRAINT `Client_Pays_FK` FOREIGN KEY (`id_pays`) REFERENCES `pays` (`id_pays`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `etre_proposee`
  ADD CONSTRAINT `etre_proposee_FormuleSansChauffeur0_FK` FOREIGN KEY (`id_f`) REFERENCES `formulesanschauffeur` (`id_f`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `etre_proposee_Modele_FK` FOREIGN KEY (`id_mdl`) REFERENCES `modele` (`id_mdl`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `formulaireavecchauffeur`
  ADD CONSTRAINT `FormulaireAvecChauffeur_Formule_FK` FOREIGN KEY (`id_f`) REFERENCES `formule` (`id_f`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `formulesanschauffeur`
  ADD CONSTRAINT `FormuleSansChauffeur_Formule_FK` FOREIGN KEY (`id_f`) REFERENCES `formule` (`id_f`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `location`
  ADD CONSTRAINT `Location_Client_FK` FOREIGN KEY (`id_cli`) REFERENCES `client` (`id_cli`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `Location_Vehicule0_FK` FOREIGN KEY (`immatriculation`) REFERENCES `vehicule` (`immatriculation`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `Location_agence1_FK` FOREIGN KEY (`numAgence`) REFERENCES `agence` (`numAgence`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `Location_agence2_FK` FOREIGN KEY (`numAgence_retourner`) REFERENCES `agence` (`numAgence`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `locationavecchauffeur`
  ADD CONSTRAINT `LocationAvecChauffeur_Client1_FK` FOREIGN KEY (`id_cli`) REFERENCES `client` (`id_cli`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationAvecChauffeur_FormulaireAvecChauffeur0_FK` FOREIGN KEY (`id_f`) REFERENCES `formulaireavecchauffeur` (`id_f`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationAvecChauffeur_Location_FK` FOREIGN KEY (`numLocation`) REFERENCES `location` (`numLocation`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationAvecChauffeur_Vehicule2_FK` FOREIGN KEY (`immatriculation`) REFERENCES `vehicule` (`immatriculation`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationAvecChauffeur_agence3_FK` FOREIGN KEY (`numAgence`) REFERENCES `agence` (`numAgence`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationAvecChauffeur_agence4_FK` FOREIGN KEY (`numAgence_retourner`) REFERENCES `agence` (`numAgence`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `locationsanschauffeur`
  ADD CONSTRAINT `LocationSansChauffeur_Client1_FK` FOREIGN KEY (`id_cli`) REFERENCES `client` (`id_cli`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationSansChauffeur_FormuleSansChauffeur0_FK` FOREIGN KEY (`id_f`) REFERENCES `formulesanschauffeur` (`id_f`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationSansChauffeur_Location_FK` FOREIGN KEY (`numLocation`) REFERENCES `location` (`numLocation`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationSansChauffeur_Vehicule2_FK` FOREIGN KEY (`immatriculation`) REFERENCES `vehicule` (`immatriculation`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationSansChauffeur_agence3_FK` FOREIGN KEY (`numAgence`) REFERENCES `agence` (`numAgence`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `LocationSansChauffeur_agence4_FK` FOREIGN KEY (`numAgence_retourner`) REFERENCES `agence` (`numAgence`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `modele`
  ADD CONSTRAINT `Modele_categorie_FK` FOREIGN KEY (`numCat`) REFERENCES `categorie` (`numCat`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `note_location`
  ADD CONSTRAINT `note_location_Location_FK` FOREIGN KEY (`numLocation`) REFERENCES `location` (`numLocation`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `note_location_note0_FK` FOREIGN KEY (`id_note`) REFERENCES `note` (`id_note`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `reserver`
  ADD CONSTRAINT `reserver_Client0_FK` FOREIGN KEY (`id_cli`) REFERENCES `client` (`id_cli`) ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT `reserver_Vehicule_FK` FOREIGN KEY (`immatriculation`) REFERENCES `vehicule` (`immatriculation`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `vehicule`
  ADD CONSTRAINT `Vehicule_Modele_FK` FOREIGN KEY (`id_mdl`) REFERENCES `modele` (`id_mdl`) ON UPDATE CASCADE ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
