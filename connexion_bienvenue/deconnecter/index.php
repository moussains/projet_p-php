<?php
	// Démarrage ou restauration de la session
	session_start();
	if (isset($_SESSION['id_cli']) || isset($_SESSION['id'])) {
		// Réinitialisation du tableau de session
		// On le vide intégralement
		$_SESSION = array();
		// Destruction de la session
		session_destroy();
		// Destruction du tableau de session
		unset($_SESSION);
		echo "";

		header("Refresh: 2; url=../../");
	}else{
	    header("Location: ../../");
	}

?>
<!DOCTYPE html>
<html>
<head>
<style> 
	._40{
		font-size: 40px;
	}
	.pad{
		padding:20% 30%;
	}
	.encours{
		position: relative;
		-webkit-animation: mymove 1s infinite; /* Safari 4.0 - 8.0 */
		-webkit-animation-timing-function: linear; /* Safari 4.0 - 8.0 */
		animation: mymove 1s infinite;
		animation-timing-function: linear;
	}

	/* Safari 4.0 - 8.0 */
	@-webkit-keyframes mymove {
		from {left: 0px;}
		to {left: 100px;}
	}

	@keyframes mymove {
		from {left: 0px;}
		to {left: 100px;}
	}
</style>
</head>
<body>

<p class="pad"><span class="_40">Déconnexion en cours ...</span><span class="encours"><img src="../../assets/images/icon_deconnexion.png"></span></p>


</body>
</html>
