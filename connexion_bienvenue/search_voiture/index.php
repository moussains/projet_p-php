<?php 
    require_once('../../classes.php');
    VerifSessionOff();
    $lecli = $Clients->getClient_session($_SESSION['id_cli']);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>
        <?php 
            if (!empty($_GET['lieu_depart']) ) {
                echo $Agences->getAgenceRestitut($_GET['lieu_depart'])['ville'];
            }else{
                echo "Location";
            } 
        ?> - LVHM
    </title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/style.css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/search_voiture.css">
    <style type="text/css">
        .unborder{
            border:5px solid #15b6b8;
        }
    </style>

</head>

<body >
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="../../"><img src="../../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="../../">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../contact"><span class="glyphicon glyphicon-earphone"></span> CONTACT</a></li>
                                <li class="mbr-navbar__item "><a class="mbr-buttons__link btn text-white" href="../profil"><span class="glyphicon glyphicon-user"></span> MON PROFIL</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../deconnecter"><span class="glyphicon glyphicon-log-out"></span> DÉCONNEXION</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine">
    <a href="#">css templates</a>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" data-rv-view="7" > 
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <?php if (!isset($_GET['lieu_depart']) || empty($_GET['lieu_depart']) ) { ?>
            <div style="margin-left: 5%; margin-top: 20%;"><h2>Aucune recherche effectuée ! :(</h2><a href="../search_agence/" class="btn btn-info" role="button">Rechercher une location</a></div>
        <?php }else{ ?>
        <div>
            <a role="button" class="btn btn-info rad" onclick="window.history.back()">
                <span class="glyphicon glyphicon-menu-left"></span> Retour au choix de l'agence
            </a>
        </div>
        <?php //echo md5(1); ?>
        <br><br>
        <div class="mbr-section__row row">
            <div class="well col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center">
                <h2>Liste des véhicules disponibles à votre recherche</h2>
            </div>
        </div>
        <div class="mbr-section__row row">
            <div class="unborder fond_couleur col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">

                <?php foreach ($AgenceVehicule->getVoitureAgence($_GET['date_depart'],$_GET['date_restitution'], $_GET['lieu_depart']) as $v_a) { ?>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-home" style="margin-right: 15px;"></span> <?php echo $v_a['adresse']; ?>, <strong><?php echo $v_a["cp"]; ?> <?php echo $v_a["ville"]; ?></strong>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="col-sm-4 text-center">
                                <img class="img-responsive" src="../../assets/images/<?php echo $v_a['photoModele']; ?>" alt="Catégorie citadine">
                                <p><?php echo $v_a['nom']; ?></p>
                                <strong>(<?php echo $Categorie->getNumCat($v_a['numCat'])['libelle'];?>)</strong>
                            </div>
                            <div class="col-sm-4">
                                <p><?php echo $v_a['lenbKmInclus'];  ?> Kilomètres inclus, après : <strong style="color: blue;"> <?php  echo $v_a['tarifKmSupplementaire']; ?> € par km</strong>.</p>
                                <p><strong style="color: green;"><span class="glyphicon glyphicon-info-sign"></span> Choix d'une formule avec ou sans chauffeur</strong></p>
                            </div>
                            <div class="col-sm-4">
                                <blockquote class="blockquote lataille">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-credit-card"></span><?php if($v_a['avecPermis']==1){echo " Avec permis";}else{echo " Sans permis";} ?> 
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-user"></span> <?php echo $v_a['nbPlaces']; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-bed"></span> <?php echo $v_a['nbPortes']; ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-screenshot"></span><?php if($v_a['boiteManuelle']==1){echo " Boîte manuelle";}else{echo " Boîte automatique";} ?> 
                                        </div>
                                    </div>
                                </blockquote>
                                <p><?php $annul = 0; if (dateDifference($aujourdhui, $_GET['date_depart'])>=2) { $annul = 1;?>
                                    <strong style="color: green;"><span class="glyphicon glyphicon-info-sign"></span> Annulation gratuite</strong>
                                <?php }else{echo '<strong style="color: red;"><span class="glyphicon glyphicon-info-sign"></span>Annulation non gratuite</strong><br><small>(Annulation gratuite pour une location commençant le '.$jour2.')</small>';} ?></p>
                            </div>                            
                        </div>
                    </div>
                    <div class="panel-footer ">
                        <div class="">
                            
                            <span class="_16">
                                <?php echo $v_a['leprix']; ?> € <span style="font-size: 10px;">(<?php echo $v_a['tarif']; ?> € / jour)</span>
                            </span>
                        </div>
                        <div class="">
                            <form action="../reservation/" method="GET">
                                <input type="hidden" name="lieu_depart" value="<?php echo $_GET['lieu_depart']; ?>">
                                <?php if (isset($_GET['lieu_restitution']) || !empty($_GET['lieu_restitution'])) { ?>
                                <input type="hidden" name="lieu_restitution" value="<?php echo $_GET['lieu_restitution']; ?>">
                                <?php  } ?>
                                <input type="hidden" name="date_depart" value="<?php echo $_GET['date_depart']; ?>">
                                <input type="hidden" name="date_restitution" value="<?php echo $_GET['date_restitution']; ?>">
                                <input type="hidden" name="time_depart" value="<?php echo $_GET['time_depart']; ?>">
                                <input type="hidden" name="time_restitution" value="<?php echo $_GET['time_restitution']; ?>">
                                <input type="hidden" name="immatricule" value="<?php echo $v_a['limmat']; ?>">
                                <input type="hidden" name="annulation" value="<?php echo $annul; ?>">
                                <button type="submit" class="btn-info btn_right">
                                    Vérifier l'offre
                                </button>
                            </form>     
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>                 
        </div>
        <?php }?>
        <br><br><br><br> 
    </div>
</section>

<?php
    include("../../footer/connexion_footer.php");
?>


<script src="../../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../../assets/jarallax/jarallax.js"></script>
<script src="../../assets/mobirise/js/script.js"></script>

</body>
</html>