<?php 
    require_once('../../classes.php');
    VerifSessionOff();
    $lecli = $Clients->getClient_session($_SESSION['id_cli']);
    $Date = date('Y-m-d');
    $DateR = date('Y-m-d',strtotime( "7 days" ));
    $DateRmin = date('Y-m-d',strtotime( "1 days" ));
    $Heure = date('H:i');
    $HeureR = date('H:i',strtotime( "30 minutes" ));  
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Recherche - LVHM</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/style.css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/search_agence.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <style type="text/css">
        .cardre_a{
            padding: 6px; border:2px solid #27aae0;
        }
        #disp{
            display: none;
        }
    </style>
</head>

<body >
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="../"><img src="../../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="../">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../contact"><span class="glyphicon glyphicon-earphone"></span> CONTACT</a></li>
                                <li class="mbr-navbar__item "><a class="mbr-buttons__link btn text-white" href="../profil"><span class="glyphicon glyphicon-user"></span> MON PROFIL</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../deconnecter"><span class="glyphicon glyphicon-log-out"></span> DÉCONNEXION</a></li>
                            </ul>                            
                            
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine">
    <a href="#">css templates</a>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" data-rv-view="7" > 
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <div class="row well decal_bottom" id="la_recherche">
            <button type="button" class="btn btn-info pad_bottom" onclick="AfficheModif();">
                <?php if(isset($_GET['rechercher'])){echo "<span class='glyphicon glyphicon-edit'></span> Modifier la recherche ";}else{echo "<span class='glyphicon glyphicon-search'></span> Rechercher une location ";}?><span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <div id="afficheModif" >
                <form action="./" method="GET">
                    <div class="col-md-12 couleur_back">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Lieu de prise en charge</label>
                                <div>
                                    <select class="depart" name="lieu_depart" required>
                                        <option></option>
                                        <?php
                                            foreach ($_AgencesGroup as $agence) {

                                                if(isset($_GET['lieu_depart'])){
                                                    $l_d = $_GET['lieu_depart'];
                                                    if ($agence['cp'] == $l_d) {
                                                        echo '<option value="'.$agence['cp'].'" selected>'.$agence['ville'].' '.$agence['cp'].'</option >';
                                                    }else{
                                                        echo '<option value="'.$agence['cp'].'">'.$agence['ville'].' '.$agence['cp'].'</option >';
                                                    }
                                                }else{
                                                    echo '<option value="'.$agence['cp'].'">'.$agence['ville'].' '.$agence['cp'].'</option >';
                                                }
                                            }
                                         ?>
                                    </select>
                                </div>      
                            </div>
                            <div class="form-group restitut">
                                <label class="control-label">Lieu de restitution</label>
                                <div>
                                    <select class="destination" name="lieu_restitution">
                                        <option></option>
                                        <?php
                                            foreach ($_Agences as $agence) {
                                                if(isset($_GET['lieu_restitution'])){
                                                    $l_r = $_GET['lieu_restitution'];
                                                    if ($agence['numAgence'] == $l_r) {
                                                        echo '<option value="'.$agence['numAgence'].'" selected>'.$agence['ville'].' '.$agence['cp'].', <i>'.$agence['adresse'].'</i></option >';
                                                    }else{
                                                        echo '<option value="'.$agence['numAgence'].'">'.$agence['ville'].' '.$agence['cp'].', <i>'.$agence['adresse'].'</i></option >';
                                                    }
                                                }else{
                                                    echo '<option value="'.$agence['numAgence'].'">'.$agence['ville'].' '.$agence['cp'].', <i>'.$agence['adresse'].'</i></option >';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <label><input id="cb" type="checkbox" name="restitution"> Restitution dans un autre lieu</label>
                        </div>
                        <div class="col-md-4">
                            <span id="disp">
                                <div class="form-group ">
                                    <label class="control-label">Âge du conducteur <span class="glyphicon glyphicon-info-sign"></span></label>
                                    <div>
                                        <select class="age" name="age" >
                                            <option value="">Votre âge</option>
                                            <?php 
                                                for ($i=21; $i <81 ; $i++) { 
                                                    if(isset($_GET['age'])){
                                                        $age = $_GET['age'];
                                                        if($i== $age){
                                                            echo '<option value="'.$i.'" selected>'.$i.'</option >';
                                                        }else{
                                                            echo '<option value="'.$i.'">'.$i.'</option >';
                                                        }
                                                    }else{
                                                        echo '<option value="'.$i.'">'.$i.'</option >';
                                                    }
                                                }
                                             ?>
                                        </select>
                                    </div>
                                </div>    
                            </span>                         
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Date de prise en charge</label>
                                <div>
                                    <input  class="dted" min="<?php echo $Date; ?>" type="date" name="date_depart" value="<?php if(isset($_GET['date_depart'])){echo $_GET['date_depart'];}else{echo $Date;} ?>" onchange="ChangeDate()" required><input class="hred" type="time" name="time_depart" min="<?php echo $HeureR; ?>" max="20:00" value="<?php if(isset($_GET['time_depart'])){echo $_GET['time_depart'];}else{echo $HeureR;} ?>" required>
                                </div>
                                <small> Les heures de bureau sont de 9h à 20h </small>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Date de restitution</label>
                                <div>
                                    <input class="dter" type="date" name="date_restitution" value="<?php if(isset($_GET['date_restitution'])){echo $_GET['date_restitution'];}else{echo $DateR;} ?>" min="<?php echo $DateRmin; ?>" required><input type="time" name="time_restitution" min="09:00" max="20:00" value="<?php if(isset($_GET['time_restitution'])){echo $_GET['time_restitution'];}else{echo $Heure;} ?>" required>
                                </div>
                                <small> Les heures de bureau sont de 9h à 20h </small>
                            </div>                         
                        </div>
                    </div>
                    <div class="col-md-12 couleur_back">
                        <div class="form-group"> 
                            <button type="submit" name="rechercher" class=" col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 col-xs-offset-3 col-xs-6 btn btn-default"><span class="glyphicon glyphicon-search"></span> Rechercher</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div>
            <h2 class="text-center">Liste des agences <?php if (isset($_GET['lieu_depart']) && isset($_GET['rechercher'])) { $v=$Agences->getAgence_ville($_GET['lieu_depart']); echo "pour ".$v['ville'];}else{echo "par ville";} ?></h2>
        </div>
        

        <br><br>
        <div class="mbr-section__row row">
            <div class="thumbnail col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <?php 
                    if(isset($_GET['lieu_depart'])){
                        $l_d = $_GET['lieu_depart'];
                        $_Agences_V=$Agences->getAgences_ville($l_d);
                        foreach ($_Agences_V as $agence_v) { 
                        ?>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <span class="glyphicon glyphicon-map-marker"></span> <?php echo $agence_v["ville"]; ?>, <?php echo $agence_v["cp"]; ?>
                                </div>
                                <div class="panel-body">
                                    <span class="glyphicon glyphicon-home" style="margin-right: 15px;"></span><?php echo $agence_v["adresse"]; ?>
                                </div>
                                <div class="panel-footer">
                                    <a role="button" class="btn-info btn_right cardre_a" href="../search_voiture/?lieu_depart=<?php echo $agence_v['numAgence']; ?>&lieu_restitution=<?php echo $_GET['lieu_restitution']; ?>&age=<?php echo $_GET['age']; ?>&date_depart=<?php echo $_GET['date_depart']; ?>&time_depart=<?php echo $_GET['time_depart']; ?>&date_restitution=<?php echo $_GET['date_restitution']; ?>&time_restitution=<?php echo $_GET['time_restitution']; ?>">
                                        À partir de <?php $prix  = $Tarif_model_V->getTarif($_GET['date_depart'],$_GET['date_restitution'], $agence_v['numAgence']); echo $prix['leprix']; ?>
                                         € <span class="glyphicon glyphicon-menu-right"></span>
                                    </a>
                                </div>
                            </div>
                        <?php
                        }
                    }else{
                        $_Agences_V=$Agences->getAgences();
                        foreach ($_Agences_V as $agence_v) { 
                        ?>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <span class="glyphicon glyphicon-map-marker"></span> <?php echo $agence_v["ville"]; ?>, <?php echo $agence_v["cp"]; ?>
                                </div>
                                <div class="panel-body">
                                    <span class="glyphicon glyphicon-home" style="margin-right: 15px;"></span> <?php echo $agence_v["adresse"]; ?>
                                </div>
                                <div class="panel-footer">
                                    <a href="#la_recherche" onclick="AfficheModif1();" class="btn btn-info btn_right" role="button">
                                        À partir de <?php $prix  = $Tarif_model_V->getTarif($Date,$Date, $agence_v['numAgence']); echo $prix['leprix']; ?>
                                         € / jour
                                    </a>
                                </div>
                            </div>
                        <?php
                        }
                    }
                ?> 
            </div>                 
        </div><br><br><br><br>  
    </div>
</section>

<?php
    include("../../footer/connexion_footer.php");
?>


<script src="../../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../../assets/jarallax/jarallax.js"></script>
<script src="../../assets/mobirise/js/script.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="../../assets/create/create.js"></script>
<script type="text/javascript">

    function ChangeDate(){
        var d = new Date();
        var dtej = d.getFullYear() +'-'+ (d.getMonth()+1) +'-'+ d.getDate();
        var dted = $('.dted').val();
        var dtedvariable = "'"+ $('.dted').val() + "'";
        if ( dtej != dted) {
            $('.hred').attr('min','09:00');
            $('dter').attr('min',dtedvariable).attr('value',dtedvariable)
            //$('.dter').datepicker({ minDate: new Date(dted) +2});
        }else{
            $('.hred').attr('min','<?php echo $Heure;?>');
        }
    }


    $('#afficheModif').hide();

    function AfficheModif(){
        $('#afficheModif').toggle();
        if ($(".destination :selected").val()!= '') {
            $('input[name=restitution]').attr("checked","checked");
            $(".restitut").show();
            $('.destination').attr("required","true");
            $('.destination').select2({
                placeholder: "Lieu de restitution",
                allowClear: true,
                language: 'fr'
            });
        }
    }

    function AfficheModif1(){
        $('#afficheModif').show();
    }

    
</script>

</body>
</html>