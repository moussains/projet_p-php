<?php 
    require_once('../../classes.php');
    VerifSessionOff();
    $lecli = $Clients->getClient_session($_SESSION['id_cli']);

    // Si annulation, immatriculation et client correspondent à une véhicule alors ça annule la réservation sinon redirection dans la page des réservations 
    if(isset($_GET['annulation']) && isset($_GET['immat']) && isset($_GET['leclient'])){
    	$Reserver-> setReserverAnnulee($_GET['immat'], $_GET['leclient']);
    	header("Location: ./#reservation");
    }else{
    	header("Location: ./");
    }