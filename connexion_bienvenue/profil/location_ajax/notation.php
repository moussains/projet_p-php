<?php
	require_once('../../../classes.php');
    VerifSessionOff();
    $lecli = $Clients->getClient_session($_SESSION['id_cli']);

    //Toutes les locations passées pour le client connecté
    $Leslocationspassees = $Location->getLocationCliPassees($lecli['id_cli']);

    //Toutes les locations en cours par le client connecté
    $Leslocationsencours = $Location->getLocationCliEncours($lecli['id_cli']);

    if (isset($_POST['not_']) && isset($_POST['des_']) && isset($_POST['loc_'])) {
    	$dteAujourdhui = date('Y-m-d H:i:s');
    	$Note_location->setNote_location($_POST['loc_'], $_POST['not_'], $_POST['des_'], $dteAujourdhui);

    	$noteL = $Note_location->getNote_locationLoc($_POST['loc_']);
        echo "<div class='text-center' style='font-size:26px; color:blue;'>";
    	echo NotationdeLocation($noteL['note']);
        echo "</div>";
	}
?>