<?php
	require_once('../../../classes.php');
    VerifSessionOff();
    $lecli = $Clients->getClient_session($_SESSION['id_cli']);

    //Toutes les locations passées pour le client connecté
    $Leslocationspassees = $Location->getLocationCliPassees($lecli['id_cli']);

    //Toutes les locations en cours par le client connecté
    $Leslocationsencours = $Location->getLocationCliEncours($lecli['id_cli']);

    if (isset($_POST['loc_']) && $_POST['loc_'] =="loc_passees") {
?>
<table class="table">
    <caption><?php $i=0; foreach ($Leslocationspassees as $loc) {$i++;} echo $i; if ($i<=1) {echo " Location passée";}else{echo " Locations passées";} ?></caption>
    <thead>
        <tr>
            <th>N° Location</th>
            <th>Date de location</th>
            <th><span class="glyphicon glyphicon-map-marker"></span>Lieu et date départ</th>
            <th><span class="glyphicon glyphicon-flag"></span>Lieu et date fin</th>
            <th>Prix</th>
            <th>Plus de détails</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($Leslocationspassees as $loc) { if(!empty($loc)){ 
            $AD = $Agences->getAgenceRestitut($loc['numAgence']);
            $AR = $Agences->getAgenceRestitut($loc['numAgence_retourner']);
             ?>
            <tr class="warning">
                <td><?php echo $loc['numLocation']; ?></td>
                <td><?php echo $loc['dteR']; ?></td>
                <td><?php echo $AD['ville']."<br>".$loc['dteHD']; ?></td>
                <td><?php echo $AR['ville']."<br>".$loc['dteHR']; ?></td>
                <td><?php echo $loc['montantRegle']; ?>€</td>
                <td>
                    <button class="glyphicon glyphicon-eye-open btn-voir" title="Voir plus de détails" data-toggle="modal" data-target=".loc<?php echo $loc['numLocation'];?>"></button>
                    <?php  
                        $dateAujourdhui = strtotime(date("Y-m-d H:i:s"));
                        $dateDepart = strtotime($loc['dateHreRetourPrevu']);

                        if ($dateDepart<= $dateAujourdhui) { 
                        	$noteL = $Note_location->getNote_locationLoc($loc['numLocation']);
                        	if(!empty($noteL)){
                        		echo '<span class="glyphicon btn-notee" style="width:200px;">'.NotationdeLocation($noteL['note']).'<p>'.$noteL['description'].'</p></span>';
                        	}else{ ?>
                        		<button class="glyphicon glyphicon-star-empty btn-notee" data-toggle="modal" data-target=".noteloc<?php echo $loc['numLocation'];?>" title="Noter ?">Noter</button>
                    <?php } } ?>
                </td>
            </tr>
        <?php }else{ ?>
            <tr>
                <td colspan="7">Aucune réservation effectuée ! </td>
            </tr>
        <?php } } ?>
    </tbody>
</table>
<?php
	}
	if (isset($_POST['loc_']) && $_POST['loc_'] == "loc_encours" ) {
?>
<table class="table">
    <caption><?php $i=0; foreach ($Leslocationsencours as $loc) {$i++;} echo $i; if ($i<=1) {echo " Location";}else{echo " Locations";} ?> en cours</caption>
    <thead>
        <tr>
            <th>N° Location</th>
            <th>Date de location</th>
            <th><span class="glyphicon glyphicon-map-marker"></span>Lieu et date départ</th>
            <th><span class="glyphicon glyphicon-flag"></span>Lieu et date fin</th>
            <th>Prix</th>
            <th>Plus de détails</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($Leslocationsencours as $loc) { if(!empty($loc)){ 
            $AD = $Agences->getAgenceRestitut($loc['numAgence']);
            $AR = $Agences->getAgenceRestitut($loc['numAgence_retourner']);
             ?>
            <tr class="warning">
                <td><?php echo $loc['numLocation']; ?></td>
                <td><?php echo $loc['dteR']; ?></td>
                <td><?php echo $AD['ville']."<br>".$loc['dteHD']; ?></td>
                <td><?php echo $AR['ville']."<br>".$loc['dteHR']; ?></td>
                <td><?php echo $loc['montantRegle']; ?>€</td>
                <td>
                    <button class="glyphicon glyphicon-eye-open btn-voir" title="Voir plus de détails" data-toggle="modal" data-target=".loc<?php echo $loc['numLocation'];?>"></button>
                </td>
            </tr>
        <?php }else{ ?>
            <tr>
                <td colspan="7">Aucune réservation effectuée ! </td>
            </tr>
        <?php } } ?>
    </tbody>
</table>
<?php
	}

?>