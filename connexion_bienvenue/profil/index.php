<?php 
    require_once('../../classes.php');
    VerifSessionOff();
    $lecli = $Clients->getClient_session($_SESSION['id_cli']);
    //Toutes les reservations et locations effectuées par le client connecté
    $Toutesreservation = $Reserver->getReserverCli($lecli['id_cli']);
    $Touteslocation = $Location->getLocationCli($lecli['id_cli']);
    //Toutes les réservations et locations en cours par le client connecté
    $Lareservation = $Reserver->getReserverCliEncours($lecli['id_cli']);
    $Lalocation = $Location->getLocationCliEncours($lecli['id_cli']);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Profil - LVHM</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">

    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/create/profil.css">

</head>

<body >
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="../"><img src="../../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="../">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../contact"><span class="glyphicon glyphicon-earphone"></span> CONTACT</a></li>
                                <li class="mbr-navbar__item "><a class="mbr-buttons__link btn text-white active" href="./"><span class="glyphicon glyphicon-user"></span> MON PROFIL</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../deconnecter"><span class="glyphicon glyphicon-log-out"></span> DÉCONNEXION</a></li>
                            </ul>                        
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine">
    <a href="#">css templates</a>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" data-rv-view="7" >
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <div class="well well-sm">
            <h2 class="text-center">MON PROFIL</h2>
        </div>
        <?php echo $reponseAfterUpdate; ?>
        <br><br>
        <div class="mbr-section__row row">
            <div class="col-sm-12 col-md-12">
                <div class="col-md-8 col-sm-8 _60">
                    <p>Heureux de vous revoir, <br><?php echo $lecli['prenom']; ?> !</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <?php if(empty($lecli['photo'])){ 
                        if($lecli['sexe']=="M"){ ?>
                            <img src="../../assets/images/avatar_m.png" class="img img-responsive img_avatar">
                        <?php }else{ ?>
                            <img src="../../assets/images/avatar_f.png" class="img img-responsive img_avatar">
                    <?php }}else{ ?>
                        <img src="../../assets/images/<?php echo $lecli['photo'];?>" class="img img-responsive img_avatar">
                    <?php } ?>
                    <form method="POST" action="./" onchange="previewFile()" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="input-group input-file align_center">
                                <input class="profilP" name="fichier[]" type="file" accept="image/*" title="Modifier la photo ?">
                                <span class="input-group-btn ">
                                    
                                    <button class="btn btn-default LeBtn" type="submit" title="Enregistrer la photo" name="profilSend"><span class="glyphicon glyphicon-floppy-disk"></span>Enregistrer</button> 
                                </span>
                                <!-- <input type="text" class="form-control" placeholder='Choisissez une photo' /> -->
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <button type="submit" class="btn btn-primary pull-right" title="Enregistrer ?"><span class="glyphicon glyphicon-floppy-disk"></span></button>
                            <button type="reset" class="btn btn-danger" title="Annuler ?"><span class="glyphicon glyphicon-remove"></span></button>
                        </div>
                    </form>
                </div> 
            </div>
            <div class="well col-sm-12 col-md-12">
                <ul class="nav nav-tabs _24">
                    <li class="active"><a data-toggle="tab" href="#tab_b">Tableau de bord</a></li>
                    <li><a data-toggle="tab" href="#compte">Compte</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab_b" class="tab-pane fade in active">
                        <div class="well well-info">
                            <h3>Mes locations</h3>
                            <div>
                                <label>Afficher les locations : </label>
                                <select id="laloc" onchange="Filtrer_Liste_Location()">
                                    <option value="loc_encours">En cours</option>
                                    <option value="loc_passees">Passées</option>
                                </select>
                            </div>
                            <div>
                                <div class="table-responsive" id="location_">          
                                    <table class="table">
                                        <caption><?php $i=0; foreach ($Lalocation as $loc) {$i++;} echo $i; if ($i<=1) {echo " Location";}else{echo " Locations";} ?> en cours</caption>
                                        <thead>
                                            <tr>
                                                <th>N° Location</th>
                                                <th>Date de location</th>
                                                <th><span class="glyphicon glyphicon-map-marker"></span>Lieu et date départ</th>
                                                <th><span class="glyphicon glyphicon-flag"></span>Lieu et date fin</th>
                                                <th>Prix</th>
                                                <th>Plus de détails</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($Lalocation as $loc) { if(!empty($loc)){ 
                                                $AD = $Agences->getAgenceRestitut($loc['numAgence']);
                                                $AR = $Agences->getAgenceRestitut($loc['numAgence_retourner']);
                                                 ?>
                                                <tr class="warning">
                                                    <td><?php echo $loc['numLocation']; ?></td>
                                                    <td><?php echo $loc['dteR']; ?></td>
                                                    <td><?php echo $AD['ville']."<br>".$loc['dteHD']; ?></td>
                                                    <td><?php echo $AR['ville']."<br>".$loc['dteHR']; ?></td>
                                                    <td><?php echo $loc['montantRegle']; ?>€</td>
                                                    <td>
                                                        <button class="glyphicon glyphicon-eye-open btn-voir" title="Voir plus de détails" data-toggle="modal" data-target=".loc<?php echo $loc['numLocation'];?>"></button>
                                                    </td>
                                                </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                    <td colspan="7">Aucune réservation effectuée ! </td>
                                                </tr>
                                            <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="reservation" class="well well-info">
                            <h3>Mes réservations</h3>
                            <div>
                                <label>Afficher les réservations : </label>
                                <select id="lares" onchange="Filtrer_Liste_Reservation()">
                                    <option value="res_encours">En cours</option>
                                    <option value="res_passees">Passées</option>
                                    <option value="res_annulees">Annulées</option>
                                </select>
                            </div>
                            <div >
                                <div class="table-responsive" id="reservation_">          
                                    <table class="table">
                                        <caption><?php $i=0; foreach ($Lareservation as $res) {$i++;} echo $i; if ($i<=1) {echo " Réservation";}else{echo " Réservations";} ?> en cours</caption>
                                        <thead>
                                            <tr>
                                                <th>N° réservation</th>
                                                <th>Date de réservation</th>
                                                <th><span class="glyphicon glyphicon-map-marker"></span>Lieu et date départ</th>
                                                <th><span class="glyphicon glyphicon-flag"></span>Lieu et date fin</th>
                                                <th>Chauffeur</th>
                                                <th>Prix</th>
                                                <th>Plus de détails</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($Lareservation as $res) { if(!empty($res)){ 
                                                $AD = $Agences->getAgenceRestitut($res['lieuDepart']);
                                                $AR = $Agences->getAgenceRestitut($res['lieuRetour']);
                                                 ?>
                                                <tr class="danger">
                                                    <td><?php echo $res['numReservation']; ?></td>
                                                    <td><?php echo $res['dteR']; ?></td>
                                                    <td><?php echo $AD['ville']."<br>".$res['dteHD']; ?></td>
                                                    <td><?php echo $AR['ville']."<br>".$res['dteHR']; ?></td>
                                                    <td><?php if($res['fsc']=="true"){echo "Sans chauffeur";}else{echo "Avec chauffeur";} ?></td>
                                                    <td><?php echo $res['montant']; ?>€</td>
                                                    <td>
                                                        <button class="glyphicon glyphicon-eye-open btn-voir" title="Voir plus de détails" data-toggle="modal" data-target=".res<?php echo $res['numReservation'];?>"></button>
                                                        <?php  if (empty($res['annulation']) && $res['diff']<=1 ) { ?>
                                                            <button class="glyphicon glyphicon-ban-circle btn-annule-disabled" title="Vous n'êtes pas autorisé" disabled>Annuler</button>
                                                        <?php }else{ ?>
                                                            <button role="button"  onclick=" verifSuppr(<?php echo "'".$res['immatriculation']."'"; ?>, <?php echo $lecli['id_cli']; ?>, <?php echo $res['numReservation']; ?>); " class="glyphicon glyphicon-remove btn-annule" title="Annuler la réservation ?">Annuler</button>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                    <td colspan="7">Aucune réservation effectuée ! </td>
                                                </tr>
                                            <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="compte" class="tab-pane fade col-md-12 col-sm-12">
                        <h3><span class="glyphicon glyphicon-cog"></span> Paramètres</h3>
                        <div class="col-md-12 col-sm-12">
                            <div class="input-group col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                <label>Nom :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="<?php echo $lecli['nom'].' '.$lecli['prenom'];?>" disabled>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#infosperso">modifier</button>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                <label>Adresse :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="<?php echo $lecli['adresse']; ?>, <?php echo $lecli['cp'].' '.$lecli['ville']; ?>" disabled>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#infosadres">modifier</button>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                <label>Adresse e-mail et téléphone :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="<?php echo $lecli['mail'].'  |  '.$lecli['tel']; ?>" disabled>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#infoscontact">modifier</button>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                <label>Mot de passe :</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" value="<?php echo $lecli['mdp']; ?>" disabled>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#infosmdp">modifier</button>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                                <label>Pays :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="<?php echo $Pays->getPaysid($lecli['id_pays'])['nom_fr_fr']; ?>" disabled>
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target="#infospays">modifier</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
            <br><br><br>
        </div>
    </div>
</section>

<?php
include("../../footer/connexion_footer.php");
?>

<!-- ******************** PARCOURIR LE MODAL DE LA PARTIE LOCATION **************************** -->
<?php foreach ($Touteslocation as $loc) { if(!empty($loc)){ 
    $AD = $Agences->getAgenceRestitut($loc['numAgence']);
    $AR = $Agences->getAgenceRestitut($loc['numAgence_retourner']);
    $res = $Reserver->getReserverFSCImmat($loc['dateHreDepartPrevu'], $loc['dateHreRetourPrevu'], $loc['immatriculation']);
    if($res['fsc']=="true"){
        $_V = $Reserver->getReserverFSCImmat($loc['dateHreDepartPrevu'], $loc['dateHreRetourPrevu'], $loc['immatriculation']);
    }else{
        $_V = $Reserver->getReserverFACImmat($loc['immatriculation']);
    }
    $Cat = $Categorie->getNumCAt($_V['numCat']); 
     ?>
        <!-- Modal -->
        <div class="modal fade loc<?php  echo $loc['numLocation'];?>" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- MODAL HEAD -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Location n°<?php echo $loc['numLocation'];?> </h4>
                    </div>
                    <!-- MODAL BODY -->
                    <div class="modal-body">

                        <div class="fond_couleur col-md-12 col-sm-12 col-xs-12">
                            <h4>&nbsp;</h4>
                            <div class="row">
                                <div class="col-sm-4 col-xs-5 lieu">
                                    <p><strong>Lieu de prise en location</strong></p>
                                    <p> 
                                        <strong>LVHM</strong><br>
                                        <?php echo  $AD['adresse']; ?><br>
                                        <?php echo $AD['cp'].' '.$AD['ville'];?><br>
                                        <?php echo $loc['dteHD']; ?>
                                    </p>
                                </div>
                                <div class="text-center col-sm-4 col-xs-2 flech">
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                </div>
                                <div class="col-sm-4 col-xs-5 lieu">
                                    <p><strong>Lieu de restitution</strong></p>
                                    <p>
                                        <strong>LVHM</strong><br>
                                        <?php echo  $AR['adresse']; ?><br>
                                        <?php echo $AR['cp'].' '.$AR['ville'];?><br>
                                        <?php echo $loc['dteHR']; ?>
                                    </p>
                                </div>   
                            </div>

                            <div class="row">

                                <!-- Début FSC -->
                                <div class=" col-sm-12 col-md-12" >
                                    <div class="row" >
                                        <div class="col-sm-4 text-center">
                                            <img class="img img-responsive" src="../../assets/images/<?php echo $_V['photoModele'];?>" alt="<?php echo $_V['photoModele'];?>">
                                            <p><?php echo  $_V['nom']; ?></p>
                                            <strong>(<?php echo $Cat['libelle']; ?>)</strong>
                                        </div>
                                        <div class="col-sm-4">
                                            <p>
                                                <?php 
                                                    if($res['fsc']=="true"){ echo  $_V['lenbKmInclus']; ?> 
                                                        Kilomètres inclus, après : <strong style="color: blue;"> <?php echo  $_V['tarifKmSupplementaire']; ?> € par km</strong>.
                                                    <?php }else{ ?>
                                                        <strong style="color: blue;"> Nombre de kilomètre illimité !</strong><br><img class="img img-responsive" src="../../assets/images/chauffeur.png" title="Formule avec chauffeur">
                                                    <?php } ?>
                                            </p>
                                            <p></p>
                                        </div>
                                        <div class="col-sm-4">
                                            <blockquote class="blockquote lataille">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <?php 
                                                            if($res['fsc']=="true"){
                                                                echo "<span class='glyphicon glyphicon-credit-card'></span>";
                                                                if($_V['avecPermis']==1){echo " Avec permis";}else{echo " Sans permis";}
                                                            }else{

                                                            } 
                                                        ?>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <span class="glyphicon glyphicon-user"></span> <?php echo $_V['nbPlaces']; ?> personnes  <?php if($res['fsc']=="true"){}else{echo "(Chauffeur inclus)";} ?>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <span class="glyphicon glyphicon-bed"></span> <?php echo $_V['nbPortes']; ?> portes
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <span class="glyphicon glyphicon-screenshot"></span> <?php if($_V['boiteManuelle']==1){echo " Boîte manuelle";}else{echo " Boîte automatique";} ?>
                                                    </div>
                                                </div>
                                            </blockquote>
                                        </div>
                                    </div>                            
                                </div>
                                <!-- Fin FSC-->

                                <!-- Détail du paiement -->
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="well well-sm text-center">
                                            <table class="table">
                                                <caption class="text-center">Détail du paiement</caption>
                                                <tfoot class="_20">
                                                    <tr>
                                                        <td colspan="2">Prix total</td>
                                                        <td><?php echo  $_V['montant']; ?> €</td>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <tr>
                                                        <td>Voiture </td>
                                                        <td><?php echo $_V['nom']; ?></td>
                                                        <td><small>(<?php echo $_V['tarif'].'€ x '.$loc['qte'].'j'; ?>)</small> 
                                                            <?php if($res['fsc']=="false"){ echo $_V['montant']/2;}else{echo $_V['montant'];}?> €</td>
                                                    </tr>
                                                    <?php if($res['fsc']=="false"){ ?>
                                                        <tr>
                                                            <td>Chauffeur</td>
                                                            <td><span class="glyphicon glyphicon-user"></span></td>
                                                            <td><small>(<?php echo $_V['montant']/2 .'€ x 1'; ?>)</small> <?php echo  $_V['montant']/2; ?> €</td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
<?php }else{} } ?>

<!-- ******************** PARCOURIR LE MODAL DE LA PARTIE LOCATION NOTATION **************************** -->
<?php foreach ($Touteslocation as $loc) { if(!empty($loc)){ 
    $AD = $Agences->getAgenceRestitut($loc['numAgence']);
    $AR = $Agences->getAgenceRestitut($loc['numAgence_retourner']);
    $res = $Reserver->getReserverFSCImmat($loc['dateHreDepartPrevu'], $loc['dateHreRetourPrevu'], $loc['immatriculation']);
    if($res['fsc']=="true"){
        $_V = $Reserver->getReserverFSCImmat($loc['dateHreDepartPrevu'], $loc['dateHreRetourPrevu'], $loc['immatriculation']);
    }else{
        $_V = $Reserver->getReserverFACImmat($loc['immatriculation']);
    }
    $Cat = $Categorie->getNumCAt($_V['numCat']); 
     ?>
        <!-- Modal -->
        <div class="modal fade noteloc<?php  echo $loc['numLocation'];?>" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- MODAL HEAD -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Notation du location n°<?php echo $loc['numLocation'];?> </h4>
                    </div>
                    <!-- MODAL BODY -->
                    <div class="modal-body">
                        <div id="messageVerif">
                            
                        </div>
                        <div id="notation_">
                            <div class="form-group">
                                <label >Notation sur 5 *</label>
                                <select class="form-control" id="lanote" name="note">
                                    <option value=""></option>
                                    <option value="1">0 sur 5</option>
                                    <option value="2">1 sur 5</option>
                                    <option value="3">2 sur 5</option>
                                    <option value="4">3 sur 5</option>
                                    <option value="5">4 sur 5</option>
                                    <option value="6">5 sur 5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Description *</label>
                                <textarea class="form-control" id="ladescription" name="description" rows="4"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btnnote btn btn-info" onclick=" NotationVerif(<?php  echo $loc['numLocation'];?>); ">Noter</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
<?php }else{} } ?>

<!-- ******************** PARCOURIR LE MODAL DE LA PARTIE RESERVATION **************************** -->
<?php foreach ($Toutesreservation as $res) { if(!empty($res)){ 
    $AD = $Agences->getAgenceRestitut($res['lieuDepart']);
    $AR = $Agences->getAgenceRestitut($res['lieuRetour']);
     /*Datetime annulation avant le départ*/
    $date = new DateTime($res['dateHreDepart']);
    
    $dateDepart = $date->format('d/m/Y à H:i');
    $delaireservation = $date->modify('-1 day');
    $string = $delaireservation->format('d F Y à H:i');


    if($res['fsc']=="true"){
        $_V = $Reserver->getReserverFSCImmat($res['dateHreDepart'], $res['dateHreRetour'], $res['immatriculation']);
    }else{
        $_V = $Reserver->getReserverFACImmat($res['immatriculation']);
    }
    $Cat = $Categorie->getNumCAt($_V['numCat']); 
     ?>
        <!-- Modal -->
        <div class="modal fade res<?php  echo $res['numReservation'];?>" role="dialog" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- MODAL HEAD -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Réservation n°<?php echo $res['numReservation'];?> </h4>
                    </div>
                    <!-- MODAL BODY -->
                    <div class="modal-body">

                        <div class="fond_couleur col-md-12 col-sm-12 col-xs-12">
                            <h4>&nbsp;</h4>
                            <div class="row">
                                <div class="col-sm-4 col-xs-5 lieu">
                                    <p><strong>Lieu de prise en location</strong></p>
                                    <p> 
                                        <strong>LVHM</strong><br>
                                        <?php echo  $AD['adresse']; ?><br>
                                        <?php echo $AD['cp'].' '.$AD['ville'];?><br>
                                        <?php echo $res['dteHD']; ?>
                                    </p>
                                </div>
                                <div class="text-center col-sm-4 col-xs-2 flech">
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                </div>
                                <div class="col-sm-4 col-xs-5 lieu">
                                    <p><strong>Lieu de restitution</strong></p>
                                    <p>
                                        <strong>LVHM</strong><br>
                                        <?php echo  $AR['adresse']; ?><br>
                                        <?php echo $AR['cp'].' '.$AR['ville'];?><br>
                                        <?php echo $res['dteHR']; ?>
                                    </p>
                                </div>   
                            </div>

                            <div class="row">

                                <!-- Début FSC -->
                                <div class=" col-sm-12 col-md-12" >
                                    <div class="row" >
                                        <div class="col-sm-4 text-center">
                                            <img class="img img-responsive" src="../../assets/images/<?php echo $_V['photoModele'];?>" alt="<?php echo $_V['photoModele'];?>">
                                            <p><?php echo  $_V['nom']; ?></p>
                                            <strong>(<?php echo $Cat['libelle']; ?>)</strong>
                                        </div>
                                        <div class="col-sm-4">
                                            <p>
                                                <?php 
                                                    if($res['fsc']=="true"){ echo  $_V['lenbKmInclus']; ?> 
                                                        Kilomètres inclus, après : <strong style="color: blue;"> <?php echo  $_V['tarifKmSupplementaire']; ?> € par km</strong>.
                                                    <?php }else{ ?>
                                                        <strong style="color: blue;"> Nombre de kilomètre illimité !</strong><br><img class="img img-responsive" src="../../assets/images/chauffeur.png" title="Formule avec chauffeur">
                                                    <?php } ?>
                                            </p>
                                            <p></p>
                                        </div>
                                        <div class="col-sm-4">
                                            <blockquote class="blockquote lataille">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <?php 
                                                            if($res['fsc']=="true"){
                                                                echo "<span class='glyphicon glyphicon-credit-card'></span>";
                                                                if($_V['avecPermis']==1){echo " Avec permis";}else{echo " Sans permis";}
                                                            }else{

                                                            } 
                                                        ?>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <span class="glyphicon glyphicon-user"></span> <?php echo $_V['nbPlaces']; ?> personnes  <?php if($res['fsc']=="true"){}else{echo "(Chauffeur inclus)";} ?>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <span class="glyphicon glyphicon-bed"></span> <?php echo $_V['nbPortes']; ?> portes
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <span class="glyphicon glyphicon-screenshot"></span> <?php if($_V['boiteManuelle']==1){echo " Boîte manuelle";}else{echo " Boîte automatique";} ?>
                                                    </div>
                                                </div>
                                            </blockquote>
                                        </div>
                                    </div>                            
                                </div>
                                <!-- Fin FSC-->

                                <!-- Détail du paiement -->
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="well well-sm text-center">
                                                <table class="table">
                                                    <caption class="text-center">Détail du paiement</caption>
                                                    <tfoot class="_20">
                                                        <tr>
                                                            <td colspan="2">Prix total</td>
                                                            <td><?php echo  $_V['montant']; ?> €</td>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody>
                                                        <tr>
                                                            <td>Voiture </td>
                                                            <td><?php echo $_V['nom']; ?></td>
                                                            <td><small>(<?php echo $_V['tarif'].'€ x '.$res['qte'].'j'; ?>)</small> 
                                                                <?php if($res['fsc']=="false"){ echo $_V['montant']/2;}else{echo $_V['montant'];}?> €</td>
                                                        </tr>
                                                        <?php if($res['fsc']=="false"){ ?>
                                                            <tr>
                                                                <td>Chauffeur</td>
                                                                <td><span class="glyphicon glyphicon-user"></span></td>
                                                                <td><small>(<?php echo $_V['montant']/2 .'€ x 1'; ?>)</small> <?php echo  $_V['montant']/2; ?> €</td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <?php if ($res['annulation']==1) { ?>
                                            <div class="col-sm-2 col-md-2 col-xs-2">
                                                <span class="glyphicon glyphicon-info-sign _40"></span>
                                            </div>
                                            <div  class="col-sm-10 col-md-10 col-xs-10">
                                                
                                                    <p><strong>Annulation gratuite</strong></p>
                                                    <p>Vous pouvez annuler votre réservation gratuitement jusqu'au <?php echo $string; ?></p>
                                                
                                            </div>
                                            <?php }else { } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
<?php }else{} } ?>


<!-- ************************ LES POPUPS POUR MODIFIER SES INFORMATIONS***************************** -->
<!-- NOM - PRENOM - DATE DE NAISSANCE -->
<div class="modal fade" id="infosperso" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Modification de votre identité</h4>
            </div>
            <div class="modal-body">
                <form action="./" method="POST">
                    <input name="lecli" type="hidden" value="<?php echo $lecli['id_cli']; ?>">
                    <input name="verif" type="hidden" value="a">
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12 col-xs-12" for="sexe">Civilité :</label>
                        <select class="col-sm-12 col-md-12 col-xs-12" id="sexe" name="sexe" required>
                            <option value="<?php echo $lecli['sexe']; ?>" selected hidden> <?php if($lecli['sexe']=="M"){ echo "Monsieur"; }else{ echo "Madame"; } ?></option>
                            <option></option>
                            <option value="M">Monsieur</option>
                            <option value="F">Madame</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="nom">Nom :</label>
                        <input class="col-sm-12 col-md-12" value="<?php echo $lecli['nom']; ?>" type="text" id="nom" name="nom" placeholder="Entrez votre nom" required>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="prenom">Prénom :</label>
                        <input class="col-sm-12 col-md-12" value="<?php echo $lecli['prenom']; ?>" type="text" id="prenom" name="prenom" placeholder="Entrez votre prénom" required>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="date_n">Date de naissance :</label>
                        <input class="col-sm-12 col-md-12" value="<?php echo $lecli['dateN']; ?>" type="date" id="date_n" name="date_n" required>
                    </div>
                    <button type="submit" class="btn btn-success center-block" name="modifierinfos">Modifier</button>
                </form>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>
<!-- ADRESSE - CP - VILLE -->
<div class="modal fade" id="infosadres" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Modification de votre adresse</h4>
            </div>
            <div class="modal-body">
                <form action="./" method="POST">
                    <input name="lecli" type="hidden" value="<?php echo $lecli['id_cli']; ?>">
                    <input name="verif" type="hidden" value="b">
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="adresse">Adresse :</label>
                        <input class="col-sm-12 col-md-12" value="<?php echo $lecli['adresse']; ?>" type="text" id="adresse" name="adresse" placeholder="Entrez votre adresse" required>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="cp">Code postale :</label>
                        <input class="col-sm-12 col-md-12" value="<?php echo $lecli['cp']; ?>" type="text" id="cp" name="cp" placeholder="Entrez votre code postale" required>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="ville">Ville :</label>
                        <input class="col-sm-12 col-md-12" value="<?php echo $lecli['ville']; ?>" type="text" id="ville" name="ville" required>
                    </div>
                    <button type="submit" class="btn btn-success center-block" name="modifierinfos">Modifier</button>
                </form>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<!-- EMAIL ET TELEPHONE -->
<div class="modal fade" id="infoscontact" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Modification de vos informations de contact</h4>
            </div>
            <div class="modal-body">
                <form action="./" method="POST">
                    <input name="lemail" type="hidden" value="<?php echo $lecli['mail']; ?>">
                    <input name="lecli" type="hidden" value="<?php echo $lecli['id_cli']; ?>">
                    <input name="verif" type="hidden" value="c">
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="email">Adresse email :</label>
                        <input class="col-sm-12 col-md-12" value="<?php echo $lecli['mail']; ?>" type="email" id="email" name="email" placeholder="Entrez votre adresse e-mail" required>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="tel">Téléphone :</label>
                        <input class="col-sm-12 col-md-12" value="<?php echo $lecli['tel']; ?>" type="text" id="tel" name="tel" placeholder="Entrez votre n° de téléphone" required>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="pwd">Mot de passe :</label>
                        <input class="col-sm-12 col-md-12" value="" type="password" id="pwd" name="pwd" required>
                    </div>
                    <button type="submit" class="btn btn-success center-block" name="modifierinfos">Modifier</button>
                </form>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<!-- MOT DE PASSE -->
<div class="modal fade" id="infosmdp" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Modification de vos informations de contact</h4>
            </div>
            <div class="modal-body">
                <form action="./" method="POST">
                    <input name="lemail" type="hidden" value="<?php echo $lecli['mail']; ?>">
                    <input name="lecli" type="hidden" value="<?php echo $lecli['id_cli']; ?>">
                    <input name="verif" type="hidden" value="d">
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="pwd">Mot de passe actuel :</label>
                        <input class="col-sm-12 col-md-12" value="" type="password" id="pwd" name="pwd" required>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="pwd1">Nouveau mot de passe :</label>
                        <input class="col-sm-12 col-md-12" value="" type="password" id="pwd1" name="pwd1" required>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="pwd2">Confirmation du nouveau mot de passe :</label>
                        <input class="col-sm-12 col-md-12" value="" type="password" id="pwd2" name="pwd2" required>
                    </div>
                    <button type="submit" class="btn btn-success center-block" name="modifierinfos">Modifier</button>
                </form>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<!-- PAYS -->
<div class="modal fade" id="infospays" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Modification de votre pays</h4>
            </div>
            <div class="modal-body">
                <form action="./" method="POST">
                    <input name="lecli" type="hidden" value="<?php echo $lecli['id_cli']; ?>">
                    <input name="verif" type="hidden" value="e">
                    <div class="form-group">
                        <label class="col-sm-12 col-md-12" for="pays">Pays :</label>
                        <select class="col-sm-12 col-md-12" id="pays" name="pays" required>
                            <?php foreach ($_Pays as $paysFr) { ?>
                                <?php if ($paysFr['id_pays'] == $lecli['id_pays']) { ?>
                                    <option value="<?php echo $paysFr['id_pays'] ?>" selected><?php echo $paysFr['nom_fr_fr']; ?></option>
                                <?php }else{?>
                                    <option value="<?php echo $paysFr['id_pays'] ?>"><?php echo $paysFr['nom_fr_fr']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success center-block" name="modifierinfos">Modifier</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<script src="../../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../../assets/jarallax/jarallax.js"></script>
<script src="../../assets/mobirise/js/script.js"></script>
<!-- <script src="../../assets/create/create.js"></script> -->
<!-- SCRIPT POUR CACHER LE BUTTON MODIF AVATAR -->
<script type="text/javascript">
    $(document).ready(function() { 
        $(".LeBtn").hide();
        $(".profilP").change(function() { 
            $(".profilP").hide();
            $(".LeBtn").show();
        });
    });
</script>


<!-- SCRIPT POUR CHANGER LES DONNEES (EN COURS, PASSÉES, ANNULÉES) -->
<script type="text/javascript">
    //Filtre pour location
    function Filtrer_Liste_Location()
    {
        var loc = $("#laloc option:selected" ).val();
        $.post(
            "location_ajax/location.php", // Le fichier cible côté serveur.
            {
                loc_ : loc
            },
            nom_fonction_retour
        );

        function nom_fonction_retour(texte_recu){
            // Du code pour gérer le retour de l'appel AJAX.
            $("#location_").empty().append(texte_recu);
        }
    }

    //Filtre pour reservation
    function Filtrer_Liste_Reservation()
    {
        var res = $("#lares option:selected" ).val();
        $.post(
            "reservation_ajax/reservation.php", // Le fichier cible côté serveur.
            {
                res_ : res
            },
            nom_fonction_retour
        );

        function nom_fonction_retour(texte_recu){
            // Du code pour gérer le retour de l'appel AJAX.
            $("#reservation_").empty().append(texte_recu);
        }
    }


    function verifSuppr(immat, idcli, numR){
        if ( window.confirm( "Voulez-vous supprimer cette réservation n°"+numR+" ?") ) {
            // Code à éxécuter si le l'utilisateur clique sur "OK"
            window.location = "suppr.php?annulation=ON&immat="+immat+"&leclient="+idcli;
        } else {
            // Code à éxécuter si l'utilisateur clique sur "Annuler" 
        }
    }


    //Verification de notation
    function NotationVerif(numLocation)
    {
        var not = $("#lanote option:selected").val();
        var des = $("#ladescription" ).val();
        if (not != "" && des != "") {
            $.post(
                "location_ajax/notation.php", // Le fichier cible côté serveur.
                {
                    not_ : not,
                    des_ : des,
                    loc_ : numLocation
                },
                nom_fonction_retour
            );

            function nom_fonction_retour(texte_recu){
                // Du code pour gérer le retour de l'appel AJAX.
                $("#notation_").empty().append(texte_recu);
                $(".btnnote").hide();
                $("#messageVerif").empty().append("<div class='alert alert-success text-center'><strong>Note prise en compte !</strong></div>");

            }    
        }else{
            $("#messageVerif").empty().append("<div class='alert alert-danger text-center'><strong>Veuillez remplir les champs !</strong></div>");
        }

    }
</script>

</body>
</html>