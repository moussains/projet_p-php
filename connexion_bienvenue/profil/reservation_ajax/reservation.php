<?php
	require_once('../../../classes.php');
	VerifSessionOff();
    $lecli = $Clients->getClient_session($_SESSION['id_cli']);

    //Toutes les réservations passées pour le client connecté
    $Lesreservationspassees = $Reserver->getReserverCliPassees($lecli['id_cli']);

    //Toutes les réservations annulées pour le client connecté
    $Lesreservationsannulees = $Reserver->getReserverCliAnnulees($lecli['id_cli']);

    //Toutes les réservations en cours par le client connecté
    $Lesreservationsencours = $Reserver->getReserverCliEncours($lecli['id_cli']);

    if (isset($_POST['res_']) && $_POST['res_'] =="res_passees") {
?>

<table class="table">
    <caption><?php $i=0; foreach ($Lesreservationspassees as $res) {$i++;} echo $i; if ($i<=1) {echo " Réservation passée";}else{echo " Réservations passées";} ?></caption>
    <thead>
        <tr>
            <th>N° réservation</th>
            <th>Date de réservation</th>
            <th><span class="glyphicon glyphicon-map-marker"></span>Lieu et date départ</th>
            <th><span class="glyphicon glyphicon-flag"></span>Lieu et date fin</th>
            <th>Chauffeur</th>
            <th>Prix</th>
            <th>Plus de détails</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($Lesreservationspassees as $res) { if(!empty($res)){ 
            $AD = $Agences->getAgenceRestitut($res['lieuDepart']);
            $AR = $Agences->getAgenceRestitut($res['lieuRetour']);
             ?>
            <tr class="danger">
                <td><?php echo $res['numReservation']; ?></td>
                <td><?php echo $res['dteR']; ?></td>
                <td><?php echo $AD['ville']."<br>".$res['dteHD']; ?></td>
                <td><?php echo $AR['ville']."<br>".$res['dteHR']; ?></td>
                <td><?php if($res['fsc']=="true"){echo "Sans chauffeur";}else{echo "Avec chauffeur";} ?></td>
                <td><?php echo $res['montant']; ?>€</td>
                <td>
                    <button class="glyphicon glyphicon-eye-open btn-voir" title="Voir plus de détails" data-toggle="modal" data-target=".res<?php echo $res['numReservation'];?>"></button>
                </td>
            </tr>
        <?php }else{ ?>
            <tr>
                <td colspan="7">Aucune réservation effectuée ! </td>
            </tr>
        <?php } } ?>
    </tbody>
</table>
<?php
	}
	if (isset($_POST['res_']) && $_POST['res_'] == "res_encours" ) {
?>
<table class="table">
    <caption><?php $i=0; foreach ($Lesreservationsencours as $res) {$i++;} echo $i; if ($i<=1) {echo " Réservation";}else{echo " Réservations";} ?> en cours</caption>
    <thead>
        <tr>
            <th>N° réservation</th>
            <th>Date de réservation</th>
            <th><span class="glyphicon glyphicon-map-marker"></span>Lieu et date départ</th>
            <th><span class="glyphicon glyphicon-flag"></span>Lieu et date fin</th>
            <th>Chauffeur</th>
            <th>Prix</th>
            <th>Plus de détails</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($Lesreservationsencours as $res) { if(!empty($res)){ 
            $AD = $Agences->getAgenceRestitut($res['lieuDepart']);
            $AR = $Agences->getAgenceRestitut($res['lieuRetour']);
             ?>
            <tr class="danger">
                <td><?php echo $res['numReservation']; ?></td>
                <td><?php echo $res['dteR']; ?></td>
                <td><?php echo $AD['ville']."<br>".$res['dteHD']; ?></td>
                <td><?php echo $AR['ville']."<br>".$res['dteHR']; ?></td>
                <td><?php if($res['fsc']=="true"){echo "Sans chauffeur";}else{echo "Avec chauffeur";} ?></td>
                <td><?php echo $res['montant']; ?>€</td>
                <td>
                    <button class="glyphicon glyphicon-eye-open btn-voir" title="Voir plus de détails" data-toggle="modal" data-target=".res<?php echo $res['numReservation'];?>"></button>
                    <?php  if (empty($res['annulation']) && $res['diff']<=1 ) { ?>
                        <button class="glyphicon glyphicon-ban-circle btn-annule-disabled" title="Vous n'êtes pas autorisé" disabled>Annuler</button>
                    <?php }else{ ?>
                        <button role="button"  onclick=" verifSuppr(<?php echo "'".$res['immatriculation']."'"; ?>, <?php echo $lecli['id_cli']; ?>, <?php echo $res['numReservation']; ?>); " class="glyphicon glyphicon-remove btn-annule" title="Annuler la réservation ?">Annuler</button>
                    <?php } ?>
                </td>
            </tr>
        <?php }else{ ?>
            <tr>
                <td colspan="7">Aucune réservation effectuée ! </td>
            </tr>
        <?php } } ?>
    </tbody>
</table>
<?php
	}
	if (isset($_POST['res_']) && $_POST['res_'] == "res_annulees" ) {
?>
<table class="table">
    <caption><?php $i=0; foreach ($Lesreservationsannulees as $res) {$i++;} echo $i; if ($i<=1) {echo " Réservation annulée";}else{echo " Réservations annulées";} ?></caption>
    <thead>
        <tr>
            <th>N° réservation</th>
            <th>Date de réservation</th>
            <th><span class="glyphicon glyphicon-map-marker"></span>Lieu et date départ</th>
            <th><span class="glyphicon glyphicon-flag"></span>Lieu et date fin</th>
            <th>Chauffeur</th>
            <th>Prix</th>
            <th>Plus de détails</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($Lesreservationsannulees as $res) { if(!empty($res)){ 
            $AD = $Agences->getAgenceRestitut($res['lieuDepart']);
            $AR = $Agences->getAgenceRestitut($res['lieuRetour']);
             ?>
            <tr class="danger">
                <td><?php echo $res['numReservation']; ?></td>
                <td><?php echo $res['dteR']; ?></td>
                <td><?php echo $AD['ville']."<br>".$res['dteHD']; ?></td>
                <td><?php echo $AR['ville']."<br>".$res['dteHR']; ?></td>
                <td><?php if($res['fsc']=="true"){echo "Sans chauffeur";}else{echo "Avec chauffeur";} ?></td>
                <td><?php echo $res['montant']; ?>€</td>
                <td>
                    <button class="glyphicon glyphicon-eye-open btn-voir" title="Voir plus de détails" data-toggle="modal" data-target=".res<?php echo $res['numReservation'];?>">
                    	
                    </button>
                </td>
            </tr>
        <?php }else{ ?>
            <tr>
                <td colspan="7">Aucune réservation effectuée ! </td>
            </tr>
        <?php } } ?>
    </tbody>
</table>
<?php
	}

?>