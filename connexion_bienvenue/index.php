<?php 
    require_once('../classes.php');
    if(session_status() === PHP_SESSION_NONE) session_start();
    if (isset($_SESSION['id_cli'])) {
        $lecli = $Clients->getClient_session($_SESSION['id_cli']);
    }else{
        header("Location: ../");
    }
    $Date = date('Y-m-d');
    $DateR = date('Y-m-d',strtotime( "7 days" ));
    $DateRmin = date('Y-m-d',strtotime( "1 days" ));
    $Heure = date('H:i');
    $HeureR = date('H:i',strtotime( "30 minutes" ));
    $les_categories_prix = $Categorie->getCatprix();

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Accueil - Location Voiture LVHM</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../assets/create/style.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
    <style type="text/css">
        .btn-circle {
            width: 80px;
            height: 40px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 40px;
        }
        .t_25{
            font-size: 25px;
        }
        .t_10{
            font-size: 10px;
        }
        #disp{
            display: none;
        }
    </style>
</head>

<body >
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse mbr-navbar--transparent" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="#"><img src="../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="./">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <!-- <div class="mbr-navbar__hamburger mbr-hamburger1"><span><img align="middle" src="../assets/images/bg6.jpg" class="img-responsive" alt="Cinque Terre" width="34" height="34"></span></div> -->
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="./contact"><span class="glyphicon glyphicon-earphone"></span> CONTACT</a></li>
                                <li class="mbr-navbar__item "><a class="mbr-buttons__link btn text-white" href="./profil"><span class="glyphicon glyphicon-user"></span> MON PROFIL</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="./deconnecter"><span class="glyphicon glyphicon-log-out"></span> DÉCONNEXION</a></li>
                            </ul>                            
                            
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine">
    <a href="#">css templates</a>
</section>
<section class="mbr-box mbr-section mbr-section--relative mbr-section--fixed-size mbr-section--full-height mbr-section--bg-adapted mbr-parallax-background" id="header1-2" data-rv-view="4" style="background-image: url(../assets/images/bg6.jpg);">
    <div class="mbr-box__magnet mbr-box__magnet--sm-padding mbr-box__magnet--center-left mbr-after-navbar">
        
        <div class="mbr-box__container mbr-section__container container">
            <div class="mbr-box mbr-box--stretched">
                <div class="mbr-box__magnet mbr-box__magnet--center-left">
                    <div class="row">                        
                        <div class=" col-sm-6 col-md-6">
                            <div class="mbr-hero animated fadeInUp">
                                <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
                                <h1 class="mbr-hero__text text-center">Trouvez la meilleure location de voiture</h1>
                            </div>
                        </div>
                        <div id="redirection">
                        </div>
                        <div class="transbox col-sm-6 col-md-6">
                            <h3 class="text-center">Louer une voiture</h3>
                            <form class="form-horizontal" action="./search_agence/" method="GET">
                                <div class="form-group col-md-12">
                                    <label class="control-label col-sm-6">Lieu de prise en location</label>
                                    <div class="col-sm-6">
                                        <select class="depart" name="lieu_depart" style="width: 250px;" required>
                                            <option></option>
                                            <?php
                                                foreach ($_AgencesGroup as $agence) {
                                                    echo '<option value="'.$agence['cp'].'">'.$agence['ville'].' '.$agence['cp'].'</option >';
                                                }
                                             ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12  restitut" >
                                    <label class="control-label col-sm-6">Lieu de restitution</label>
                                    <div class="col-sm-6">
                                        <select class="destination" name="lieu_restitution" style="width: 250px;">
                                            <option></option>
                                            <?php
                                                foreach ($_Agences as $agence) {
                                                    echo '<option value="'.$agence['numAgence'].'">'.$agence['ville'].' '.$agence['cp'].', <span style="font-style: oblique;">'.$agence['adresse'].'</span></option >';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-offset-3 col-md-6">
                                    <label><input id="cb" type="checkbox" name="restitution"> Restitution dans un autre lieu</label>
                                </div>
                                 <div class="form-group col-md-12" id="disp">
                                    <label class="control-label col-sm-6">Âge du conducteur <span class="glyphicon glyphicon-info-sign" data-toggle="tooltip"data-placement="bottom"  title="Pour les conducteurs de moins de 26 ans et de plus de 69 ans, une taxe supplémentaire peut être demandée. Veuillez renseigner votre âge, pour que nous puissions vous proposez les meilleures offres, frais supplémentaires inclus."></span></label>
                                    <div class="col-sm-6">
                                        <select class="age" name="age" >
                                            <option value="">Votre âge</option>
                                            <?php 
                                                for ($i=21; $i <81 ; $i++) { 
                                                    echo '<option value="'.$i.'">'.$i.'</option >';
                                                }
                                             ?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group col-md-12">
                                    <label class="control-label col-sm-6">Date de prise en charge</label>
                                    <div class="col-sm-6">
                                        <input class="dted"  min="<?php echo $Date; ?>" value="<?php echo $Date; ?>" type="date" name="date_depart" required><input class="hred" min="<?php echo $HeureR; ?>" value="<?php echo $HeureR; ?>" type="time" name="time_depart" max="20:00" required>
                                    </div>
                                    <small> Les heures de bureau sont de 9h à 20h </small>
                                </div>

                                <div class="form-group col-md-12">
                                    <label class="control-label col-sm-6">Date de restitution</label>
                                    <div class="col-sm-6">
                                        <input class="dter" min="<?php echo $DateRmin; ?>" value="<?php echo $DateR; ?>" type="date" name="date_restitution" required><input type="time" name="time_restitution"  min="09:00" value="<?php echo $Heure; ?>" max="20:00" required>
                                    </div>
                                    <small> Les heures de bureau sont de 9h à 20h </small>
                                </div>
                                <div class="form-group"> 
                                    <button type="submit" name="rechercher" class="leclic col-sm-offset-3 col-sm-6 col-md-offset-3 col-md-6 col-xs-offset-3 col-xs-6 btn btn-default"><span class="glyphicon glyphicon-search"></span> Rechercher</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-arrow mbr-arrow--floating text-center">
            <div class="mbr-section__container container">
                <a class="mbr-arrow__link" href="#content4-3"><i class="glyphicon glyphicon-menu-down"></i></a>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" id="content4-3" data-rv-view="7" > 
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <h2 class="text-center">Meilleures offres de location de voiture</h2>
        <p>&nbsp;</p><p>&nbsp;</p>
        <div class="mbr-section__row row">
            <?php foreach ($les_categories_prix as $cat) { ?>
                <div class="col-sm-3 col-xs-6">
                    <a href="#redirection" data-toggle="tooltip" data-placement="right" onclick="clic();" title="<?php echo $cat['libelle'];?>">
                        <div class="thumbnail shadow">
                            <h3 class="text-center"><?php echo $cat['libelle'];?></h3>
                            <hr>
                            <img src="../assets/images/<?php echo $cat['photoCat']; ?>" alt="<?php echo $cat['libelle'];?>" width="400" height="300">
                            <p><strong>À partir de <?php echo $cat['tarif']; ?> € par jour</strong></p>
                        </div>
                    </a>
                </div>
            <?php } ?>
            <p>&nbsp;</p><p>&nbsp;</p>        
        </div>
    </div>
</section>


<section class="mbr-section mbr-section--relative mbr-section--fixed-size section_background_notes" data-rv-view="7">

    <div class="mbr-section__container container mbr-section__container--std-top-padding ">
        <h2 class="text-center">Évaluation des voitures de location</h2>
        <p>&nbsp;</p><p>&nbsp;</p>
        <div class="mbr-section__row row">
            <div class="col-sm-6 col-md-6 text-center">
                <h3>Notation moyenne des locations de voiture selon les clients.</h3>
                <p>&nbsp;</p>
                <h2 >
                    <?php
                        echo NotationdeLocation($_MoyenCOunt['moyen']);  
                    ?>
                </h2>
                <h4>(Notation(s) <?php echo $_MoyenCOunt['nbNote']; ?>)</h4>
            </div>
            <div class=" col-sm-6 col-md-6">
                <blockquote >
                    <?php  foreach ($_Note_location as $NL) { ?>
                        <div class="col-md-12 col-xs-12 col-sm-12 thumbnail shadow">
                            <div class="col-sm-8 col-xs-8 col-md-8">
                                <p><?php echo $NL['description']; ?><br><span style="color:#A8A19A; font-size: 10px;">▬▬ <?php echo $NL['dte']; ?></span></p>
                            </div>
                            <div class="col-sm-4 col-xs-4 col-md-4 text-center">
                                <p>
                                    <span style="font-size: 40px" class="glyphicon glyphicon-user"></span>
                                    <br/><?php echo $NL['nom'].' '.$NL['prenom']; ?>
                                    <br/>
                                    <?php echo NotationdeLocation($NL['note']); ?>
                                </p>
                                
                            </div>
                        </div>
                    <?php } ?>&nbsp;
                </blockquote>
            </div>
            <div class="text-center">
                <p><span class="glyphicon glyphicon-info-sign"></span> Les commentaires sont placés par nos clients à la fin de la période de location</p>
            </div>
        </div>
    </div>
</section>

<?php
    include("../footer/other_footer.php");
?>


<script src="../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../assets/jarallax/jarallax.js"></script>
<script src="../assets/mobirise/js/script.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="../assets/create/create.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('.dted').change(function(){
            var d = new Date();
            var dtej = d.getFullYear() +'-'+ (d.getMonth()+1) +'-'+ d.getDate();
            var dted = $('.dted').val();

            if ( dtej != dted) {
                $('.hred').attr('min','09:00');
            }else{
                $('.hred').attr('min','<?php echo $Heure;?>');
            }

        });
        
    }); 
    function clic(){
        $('.leclic').click();
    } 
</script>


</body>
</html>