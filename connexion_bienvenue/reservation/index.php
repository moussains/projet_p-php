<?php 
    require_once('../../classes.php');
    VerifSessionOnEtGETLieu();
    $lecli = $Clients->getClient_session($_SESSION['id_cli']);


    // Récupération des données du véhicule
    $_V = $Vehicule->getVoiture($_GET['date_depart'], $_GET['date_restitution'], $_GET['immatricule']);

    /*Datetime de depart*/
    $date = new DateTime($_GET['date_depart'].' '.$_GET['time_depart']);
    
    $dateDepart = $date->format('d/m/Y à H:i');
    $delaireservation = $date->modify('-1 day');
    $string = $delaireservation->format('d F Y à H:i');
    

    /*Datetime de restitution*/
    $dateR = new DateTime($_GET['date_restitution'].' '.$_GET['time_restitution']);
    $dateRetour = $dateR->format('d/m/Y à H:i');


    // Si le lieu de restitution n'est pas saisie, on prend celui de départ
    if (!isset($_GET['lieu_restitution']) || empty($_GET['lieu_restitution']) ) {
        $A_R = $Vehicule->getVoiture($_GET['date_depart'], $_GET['date_restitution'], $_GET['immatricule']);
    }else{
        $A_R = $Agences->getAgenceRestitut($_GET['lieu_restitution']);
    }

    // On récupère la catégorie du véhicule
    $Cat = $Categorie->getNumCAt( $_V['numCat']);  

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Location Voiture LVHM</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/style.css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/reservation.css">
</head>

<body >
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="../"><img src="../../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="../">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../contact"><span class="glyphicon glyphicon-earphone"></span> CONTACT</a></li>
                                <li class="mbr-navbar__item "><a class="mbr-buttons__link btn text-white" href="../profil"><span class="glyphicon glyphicon-user"></span> MON PROFIL</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../deconnecter"><span class="glyphicon glyphicon-log-out"></span> DÉCONNEXION</a></li>
                            </ul>                        
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine">
    <a href="#">css templates</a>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" data-rv-view="7" > 
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <div>
            <form>
                <button type="button" class="btn btn-info rad" onclick="window.history.back()">
                    <span class="glyphicon glyphicon-menu-left"></span> Retour au choix de voiture
                </button>
            </form>
        </div>
        <br><br>
        <div class="mbr-section__row row">
            <div class="well well-lg col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center">
                <h2>Finaliser votre réservation</h2>
            </div>
        </div>
        <div class="mbr-section__row row">
            <div class="fond_couleur col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <h4>Votre recherche</h4>
                <div class="row">
                    <div class="col-sm-4 col-xs-5 lieu">
                        <p><strong>Lieu de prise en location</strong></p>
                        <p> 
                            <strong>LVHM</strong><br>
                            <?php echo  $_V['adresse']; ?><br>
                            <?php echo $_V['cp'].' '.$_V['ville'];?><br>
                            <?php echo $dateDepart; ?>
                        </p>
                    </div>
                    <div class="text-center col-sm-4 col-xs-2 flech">
                        <span class="glyphicon glyphicon-arrow-right"></span>
                    </div>
                    <div class="col-sm-4 col-xs-5 lieu">
                        <p><strong>Lieu de restitution</strong></p>
                        <p>
                            <strong>LVHM</strong><br>
                            <?php echo  $A_R['adresse']; ?><br>
                            <?php echo $A_R['cp'].' '.$A_R['ville'];?><br>
                            <?php echo $dateRetour; ?>
                        </p>
                    </div>   
                </div>

                <h4>Voiture de location</h4>
                <div class="row">
                    <!-- Début choix de la formule -->
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 text-center">
                        <ul class="nav nav-pills list-inline row">
                            <li class="active col-sm-4 col-md-4 col-xs-4">
                                <a data-toggle="pill" onclick="fsc()" href="#fsc" title="Formule sans chauffeur">Formule sans chauffeur</a>
                            </li>
                            <li class="col-sm-4 col-md-4 col-xs-4">
                                <a data-toggle="pill" onclick="fac()" href="#fac" title="Formule avec chauffeur">Formule avec chauffeur</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Fin de choix de la formule -->

                    <!-- Début FSC -->
                    <div id="fsc" class="tab-pane fade in active col-sm-12 col-md-12 col-xs-12" >
                        <div class="row" >
                            <div class="pdbottom col-md-12 col-sm-12 col-xs-12">
                                <span class="_20"><?php echo  $_V['nom']; ?></span>
                                <span class="_26">
                                    <span>Prix pour toute la durée de la location :</span> <span class="unprix"></span>
                                </span>
                            </div>
                            <div class="col-sm-4 text-center">
                                <img class="img-responsive" src="../../assets/images/<?php echo $_V['photoModele'];?>" alt="<?php echo $_V['photoModele'];?>">
                                <p><?php echo  $_V['nom']; ?></p>
                                <strong>(<?php echo $Cat['libelle']; ?>)</strong>
                            </div>
                            <div class="col-sm-4">
                                <p><?php echo  $_V['lenbKmInclus']; ?> Kilomètres inclus, après : <strong style="color: blue;"> <?php echo  $_V['tarifKmSupplementaire']; ?> € par km</strong>.</p>
                                <p></p>
                            </div>
                            <div class="col-sm-4">
                                <blockquote class="blockquote lataille">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-credit-card"></span><?php if($_V['avecPermis']==1){echo " Avec permis";}else{echo " Sans permis";} ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-user"></span> <?php echo $_V['nbPlaces']; ?> personnes
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-bed"></span> <?php echo $_V['nbPortes']; ?> portes
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-screenshot"></span> <?php if($_V['boiteManuelle']==1){echo " Boîte manuelle";}else{echo " Boîte automatique";} ?>
                                        </div>
                                    </div>
                                </blockquote>
                                <p>
                                    <?php if ($_GET['annulation']==1) { ?>
                                        <strong style="color: green;"><span class="glyphicon glyphicon-info-sign"></span> Annulation gratuite</strong>
                                    <?php }else { ?> 
                                        <strong style="color: red;"><span class="glyphicon glyphicon-info-sign"></span>Annulation non gratuite</strong><br><small>(Annulation gratuite pour une location commençant le <?php echo $jour2; ?>)</small>
                                    <?php } ?>
                                </p>
                            </div>
                        </div>                            
                    </div>
                    <!-- Fin FSC-->

                    <!-- Début FAC -->
                    <div id="fac" class="tab-pane fade col-sm-12 col-md-12 col-xs-12">
                        <div class="row" >
                            <div class="pdbottom col-md-12 col-sm-12 col-xs-12">
                                <span class="_20"><?php echo  $_V['nom']; ?></span>
                                <span class="_26">
                                    <span>Prix pour toute la durée de la location :</span> <span class="unprix"></span>
                                </span>
                            </div>
                            <div class="col-sm-4 text-center">
                                <img class="img-responsive" src="../../assets/images/<?php echo $_V['photoModele'];?>" alt="<?php echo $_V['photoModele'];?>">
                                <p><?php echo  $_V['nom']; ?></p>
                                <strong>(<?php echo $Cat['libelle']; ?>)</strong>
                            </div>
                            <div class="col-sm-4">
                                <p><strong style="color: blue;"> Nombre de kilomètre illimité !</strong><br><img class="img img-responsive" src="../../assets/images/chauffeur.png" title="Formule avec chauffeur"></p>
                                <p></p>
                            </div>
                            <div class="col-sm-4">
                                <blockquote class="blockquote lataille">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-user"></span> <?php echo $_V['nbPlaces']; ?> personnes (chauffeur inclus)
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-bed"></span> <?php echo $_V['nbPortes']; ?> portes
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="glyphicon glyphicon-screenshot"></span> <?php if($_V['boiteManuelle']==1){echo " Boîte manuelle";}else{echo " Boîte automatique";} ?>
                                        </div>
                                    </div>
                                </blockquote>
                                <p>
                                    <?php if ($_GET['annulation']==1) { ?>
                                        <strong style="color: green;"><span class="glyphicon glyphicon-info-sign"></span> Annulation gratuite</strong>
                                    <?php }else { ?> 
                                        <strong style="color: red;"><span class="glyphicon glyphicon-info-sign"></span>Annulation non gratuite</strong><br><small>(Annulation gratuite pour une location commençant le <?php echo $jour2; ?>)</small>
                                    <?php } ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Fin FAC-->
                </div>
                
                <h4>Données personnelles du conducteur principal</h4>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12 col-xs-12" for="sexe">Civilité :</label>
                                <select class="col-sm-12 col-md-12 col-xs-12" id="sexe" name="sexe" disabled>
                                    <option value="<?php echo $lecli['sexe']; ?>" selected><?php if ($lecli['sexe']=='M') {echo "Monsieur";}else{echo "Madame";} ?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="nom">Nom :</label>
                                <input class="col-sm-12 col-md-12" type="text" id="nom" name="nom" value="<?php echo $lecli['nom']; ?>" placeholder="Entrez votre nom" disabled>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="prenom">Prénom :</label>
                                <input class="col-sm-12 col-md-12" type="text" id="prenom" name="prenom" value="<?php echo $lecli['prenom']; ?>" placeholder="Entrez votre prénom" disabled>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="date_n">Date de naissance :</label>
                                <input class="col-sm-12 col-md-12" type="date" id="date_n" name="date_n" value="<?php echo $lecli['dateN']; ?>" disabled>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="email">Adresse e-mail :</label>
                                <input class="col-sm-12 col-md-12" type="email" id="email" name="email" value="<?php echo $lecli['mail']; ?>" placeholder="azerty@xyz.com" disabled>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="adresse">Adresse :</label>
                                <input class="col-sm-12 col-md-12" type="text" id="adresse" name="adresse" value="<?php echo $lecli['adresse']; ?>" placeholder="Entrez votre adresse" disabled>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="cp">Code postal :</label>
                                <input class="col-sm-12 col-md-12" type="text" id="cp" name="cp" value="<?php echo $lecli['cp']; ?>" placeholder="Ex : 76600" disabled>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12 col-xs-12" for="ville">Ville :</label>
                                <input class="col-sm-12 col-md-12" type="text" id="ville" name="ville" value="<?php echo $lecli['ville']; ?>" placeholder="Entrez votre ville" disabled>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12 col-xs-12" for="pays">Pays :</label>
                                <select class="col-sm-12 col-md-12 col-xs-12" id="pays" name="pays" disabled>
                                    <option value="<?php echo $lecli['id_pays']; ?>" selected><?php echo $Pays->getPaysid($lecli['id_pays'])['nom_fr_fr']; ?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="tel">Téléphone :</label>
                                <input class="col-sm-12 col-md-12" type="text" id="tel" name="tel" value="<?php echo $lecli['tel']; ?>" placeholder="Ex : 0768266541"  disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <h4>Confirmer votre réservation</h4>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-sm-6 col-md-6">
                            <div class="well well-sm text-center">
                                <table class="table">
                                    <caption class="text-center">À régler lors de la réservation</caption>
                                    <tfoot class="_20">
                                        <tr>
                                            <td>Prix total</td>
                                            <td><span class="unprix"></span></td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr>
                                            <td>Voiture</td>
                                            <td><span class="unprix"></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="col-sm-2 col-md-2 col-xs-2">
                                <span class="glyphicon glyphicon-info-sign _40"></span>
                            </div>
                            <div  class="col-sm-10 col-md-10 col-xs-10">
                                
                                <?php if ($_GET['annulation']==1) { ?>
                                    <p><strong>Annulation gratuite</strong></p>
                                    <p>Vous pouvez annuler votre réservation gratuitement jusqu'au <?php echo $string; ?></p>
                                <?php }else { ?> 
                                    <p><strong>Annulation non gratuite</strong><br><small>(Annulation gratuite pour une location commençant le <?php echo $jour2; ?>)</small></p>
                                <?php } ?>

                            </div>
                        </div>
                        <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal">
                            Réserver maintenant pour <span class="unprix"></span>
                        </button>


                        <p>
                            <small>
                                En réservant, vous acceptez les Termes et conditions de l'opérateur, les Termes et conditions de LVHM et les Conditions du loueur local.
                            </small>
                        </p>
                    </div>
                </div>
            </div>                 
        </div><br> 
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <h4 class="modal-title">Confirmez-vous cette réservation ?</h4>
            </div>
            <div class="modal-body">

                <!-- Détails -->
                <div class="col-sm-12" >
                    <div class="row" >
                        <div class="col-sm-4 text-center">
                            <img class="img-responsive" src="../../assets/images/<?php echo $_V['photoModele'];?>" alt="<?php echo $_V['photoModele'];?>">
                            <p><?php echo  $_V['nom']; ?></p>
                            <strong>(<?php echo $Cat['libelle']; ?>)</strong>
                        </div>
                        <div class="col-sm-6">
                            <p>
                                <?php if ($_GET['annulation']==1) { ?>
                                    <p><strong style="color: green;"><span class="glyphicon glyphicon-info-sign"></span>Vous pouvez annuler votre réservation gratuitement jusqu'au <?php echo $string; ?></strong></p>
                                <?php }else { ?> 
                                    <p><strong style="color: red;"><span class="glyphicon glyphicon-info-sign"></span>Annulation non gratuite</strong><br><small>(Annulation gratuite pour une location commençant le <?php echo $jour2; ?>)</small></p>
                                <?php } ?>

                                
                            </p>
                            <p>
                                <span>Prix pour toute la durée de la location :</span> <strong style="font-size: 20px;"><span class="unprix"></span></strong> 
                            </p>
                        </div>
                    </div>                            
                </div>

            </div>
            <div class="modal-footer">
                <form action="DetailReservationConfirm.php" method="POST">
                    <input value="<?php echo $_GET['annulation']; ?>" type="hidden" name="annulation">
                    <input class="lefsc" type="hidden" name="fsc">
                    <input type="hidden" name="lieu_depart" value="<?php echo $_V['numAgence']; ?>">
                    <input type="hidden" name="lieu_restitution" value="<?php echo $A_R['numAgence']; ?>">
                    <input type="hidden" name="immat" value="<?php echo $_GET['immatricule']; ?>">
                    <input type="hidden" name="id_cli" value="<?php echo $lecli['id_cli']; ?>">
                    <input type="hidden" name="numR" value="<?php echo GeraHash(8); ?>">
                    <input type="hidden" name="dteR" value="<?php echo date('Y-m-d H:i:s'); ?>">
                    <input type="hidden" name="dteHD" value="<?php echo $_GET['date_depart'].' '.$_GET['time_depart']; ?>">
                    <input type="hidden" name="dteHR" value="<?php echo $_GET['date_restitution'].' '.$_GET['time_restitution'] ?>">
                    <input class="prix" type="hidden" name="montant" >
                    <button type="submit" class="btn btn-success" >Confirmer et payer</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>   
                </form>
                
            </div>
        </div>
    </div>
</div>

<?php
    include("../../footer/connexion_footer.php");
?>


<script src="../../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../../assets/jarallax/jarallax.js"></script>
<script src="../../assets/mobirise/js/script.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $("#fac").hide();
       $('.prix').attr("value","<?php echo $_V['leprix']; ?>");
       $('.lefsc').attr("value","true");
       $('.unprix').empty().append("<?php echo $_V['leprix']*1; ?> €");
    });
    function fac(){
        $("#fsc").hide();
        $("#fac").show();
        $('.prix').attr("value","<?php echo $_V['leprix']*2; ?>");
        $('.lefsc').attr("value","false");
        $('.unprix').empty().append("<?php echo $_V['leprix']*2; ?> €");
    }
    function fsc(){
        $("#fsc").show();
        $("#fac").hide();
        $('.prix').attr("value","<?php echo $_V['leprix']; ?>");
        $('.lefsc').attr("value","true");
        $('.unprix').empty().append("<?php echo $_V['leprix']*1; ?> €");
    }
</script>

</body>
</html>