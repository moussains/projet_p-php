<?php 
    require_once('../../classes.php');
    VerifSessionOff();
    $lecli = $Clients->getClient_session($_SESSION['id_cli']);

    // Insertion de la reservation dans la base de données
    $Reserver->setReserver($_POST['immat'], $_POST['id_cli'], $_POST['numR'], $_POST['dteR'], $_POST['dteHD'], $_POST['dteHR'], $_POST['montant'], $_POST['lieu_depart'], $_POST['lieu_restitution'],$_POST['annulation'], $_POST['fsc']);

    //Vérification de formule sans chauffeur
    if (isset($_POST['fsc'])) {
        if($_POST['fsc']=="true"){
            $_V = $Reserver->getReserverFSCImmat($_POST['dteHD'], $_POST['dteHR'], $_POST['immat']);
        }else{
            $_V = $Reserver->getReserverFACImmat($_POST['immat']);
        }
        $A_D = $Agences->getAgenceRestitut($_V['lieuDepart']);
    }
    // Récupération des données du véhicule
    
    $Qte = dateDifference($_V['dateHreDepart'], $_V['dateHreRetour']);
    if ($Qte==0) {$Qte=1;}

    /*Datetime de depart*/
    $date = new DateTime($_V['dateHreDepart']);
    
    $dateDepart = $date->format('d/m/Y à H:i');
    $delaireservation = $date->modify('-1 day');
    $ledelai = $delaireservation->format('d F Y à H:i');
    

    /*Datetime de restitution*/
    $dateR = new DateTime($_V['dateHreRetour']);
    $dateRetour = $dateR->format('d/m/Y à H:i');


    // Si le lieu de restitution n'est pas saisie, on prend celui de départ
    if (!isset($_POST['lieu_restitution']) || empty($_POST['lieu_restitution']) ) {
        $A_R = $Agences->getAgenceRestitut($_V['lieuDepart']);
    }else{
        $A_R = $Agences->getAgenceRestitut($_V['lieuRetour']);
    }

    // On récupère la catégorie du véhicule
    $Cat = $Categorie->getNumCAt($_V['numCat']);  

     

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Site made with Mobirise Website Builder v4.8.7, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.8.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Réservation <?php echo $_V['numReservation']; ?> - Voiture LVHM</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/style.css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/reservation.css">
</head>

<body >
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="../"><img src="../../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="../">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../contact"><span class="glyphicon glyphicon-earphone"></span> CONTACT</a></li>
                                <li class="mbr-navbar__item "><a class="mbr-buttons__link btn text-white" href="../profil"><span class="glyphicon glyphicon-user"></span> MON PROFIL</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../deconnecter"><span class="glyphicon glyphicon-log-out"></span> DÉCONNEXION</a></li>
                            </ul>                        
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine">
    <a href="#">css templates</a>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" data-rv-view="7" > 
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <span class="text-center" style="color: green;">
            <h1>Réservation effectuée ! <span class="glyphicon glyphicon-ok"></span></h1>
        </span>
        <div class="well well-sm">
            <span class="text-center" >
                <h2><span class="glyphicon glyphicon-info-sign"></span> Détail de la réservation</h2>
            </span>
        </div>


        <div class="fond_couleur col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <h4>&nbsp;<span style="float: right;"><a href="#"><span class="glyphicon glyphicon-print"></span>Imprimer</span></a></h4>
            <div class="row">
                <div class="col-sm-4 col-xs-5 lieu">
                    <p><strong>Lieu de prise en location</strong></p>
                    <p> 
                        <strong>LVHM</strong><br>
                        <?php echo  $A_D['adresse']; ?><br>
                        <?php echo $A_D['cp'].' '.$A_D['ville'];?><br>
                        <?php echo $dateDepart; ?>
                    </p>
                </div>
                <div class="text-center col-sm-4 col-xs-2 flech">
                    <span class="glyphicon glyphicon-arrow-right"></span>
                </div>
                <div class="col-sm-4 col-xs-5 lieu">
                    <p><strong>Lieu de restitution</strong></p>
                    <p>
                        <strong>LVHM</strong><br>
                        <?php echo  $A_R['adresse']; ?><br>
                        <?php echo $A_R['cp'].' '.$A_R['ville'];?><br>
                        <?php echo $dateRetour; ?>
                    </p>
                </div>   
            </div>

            <div class="row">

                <!-- Début FSC -->
                <div class=" col-sm-12 col-md-12" >
                    <div class="row" >
                        <div class="col-sm-4 text-center">
                            <img class="img img-responsive" src="../../assets/images/<?php echo $_V['photoModele'];?>" alt="<?php echo $_V['photoModele'];?>">
                            <p><?php echo  $_V['nom']; ?></p>
                            <strong>(<?php echo $Cat['libelle']; ?>)</strong>
                        </div>
                        <div class="col-sm-4">
                            <p>
                                <?php 
                                    if($_POST['fsc']=="true"){ echo  $_V['lenbKmInclus']; ?> 
                                        Kilomètres inclus, après : <strong style="color: blue;"> <?php echo  $_V['tarifKmSupplementaire']; ?> € par km</strong>.
                                    <?php }else{ ?>
                                        <strong style="color: blue;"> Nombre de kilomètre illimité !</strong><br><img class="img img-responsive" src="../../assets/images/chauffeur.png" title="Formule avec chauffeur">
                                    <?php } ?>
                            </p>
                            <p></p>
                        </div>
                        <div class="col-sm-4">
                            <blockquote class="blockquote lataille">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php 
                                            if($_POST['fsc']=="true"){
                                                echo "<span class='glyphicon glyphicon-credit-card'></span>";
                                                if($_V['avecPermis']==1){echo " Avec permis";}else{echo " Sans permis";}
                                            }else{

                                            } 
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="glyphicon glyphicon-user"></span> <?php echo $_V['nbPlaces']; ?> personnes  <?php if($_POST['fsc']=="true"){}else{echo "(Chauffeur inclus)";} ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="glyphicon glyphicon-bed"></span> <?php echo $_V['nbPortes']; ?> portes
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="glyphicon glyphicon-screenshot"></span> <?php if($_V['boiteManuelle']==1){echo " Boîte manuelle";}else{echo " Boîte automatique";} ?>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>                            
                </div>
                <!-- Fin FSC-->

                <!-- Détail du paiement -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-sm-6 col-md-6">
                            <div class="well well-sm text-center">
                                <table class="table">
                                    <caption class="text-center">Détail du paiement</caption>
                                    <tfoot class="_20">
                                        <tr>
                                            <td colspan="2">Prix total</td>
                                            <td><?php echo  $_V['montant']; ?> €</td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr>
                                            <td>Voiture </td>
                                            <td><?php echo $_V['nom']; ?></td>
                                            <td><small>(<?php echo $_V['tarif'].'€ x '.($Qte+1).'j'; ?>)</small> <?php if (isset($_POST['fsc'])){if($_POST['fsc']=="false"){ echo $_V['montant']/2;}else{echo $_V['montant'];} }?> €</td>
                                        </tr>
                                        <?php if (isset($_POST['fsc'])) { if($_POST['fsc']=="false"){ ?>
                                            <tr>
                                                <td>Chauffeur</td>
                                                <td><span class="glyphicon glyphicon-user"></span></td>
                                                <td><small>(<?php echo $_V['montant']/2 .'€ x 1'; ?>)</small> <?php echo  $_V['montant']/2; ?> €</td>
                                            </tr>
                                        <?php }} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="col-sm-2 col-md-2 col-xs-2">
                                <span class="glyphicon glyphicon-info-sign _40"></span>
                            </div>
                            <div  class="col-sm-10 col-md-10 col-xs-10">
                                <?php if ($_POST['annulation']==1) { ?>
                                    <p><strong>Annulation gratuite</strong></p>
                                    <p>Vous pouvez annuler votre réservation gratuitement jusqu'au <?php echo $ledelai; ?></p>
                                <?php }else { ?> 
                                    <p><strong>Annulation non gratuite</strong><br><small>(Annulation gratuite pour une location commençant le <?php echo $jour2; ?>)</small></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="pdbottom col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                        <a role="button" href="../profil/#reservation" class="btn btn-default">Voir mes réservations</a>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>


<?php
    include("../../footer/connexion_footer.php");
?>


<script src="../../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../../assets/jarallax/jarallax.js"></script>
<script src="../../assets/mobirise/js/script.js"></script>

</body>
</html>