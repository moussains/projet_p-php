<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Site made with Mobirise Website Builder v4.8.7, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.8.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Recherche - LVHM</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/style.css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/search_agence.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

</head>

    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "lvhm";

    // connexion
    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    //récupérer les éléments
    if(isset($_POST['Bouton'])) {
    $N_client=$_POST['id_cli'];
    $N_réservation=$_POST['numReservation'];


//pour l'affichage
 $afficher="select id_cli,nom,prenom,tel,mail from client where id_cli =$N_client";
    $result = $conn->query($afficher);

?>
<body>
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="../"><img src="../../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="../">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../contact"><span class="glyphicon glyphicon-earphone"></span> CONTACT</a></li>
                                <li class="mbr-navbar__item "><a class="mbr-buttons__link btn text-white" href="../profil"><span class="glyphicon glyphicon-user"></span> MON PROFIL</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../deconnecter"><span class="glyphicon glyphicon-log-out"></span> DÉCONNEXION</a></li>
                            </ul>

                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine">
    <a href="#">css templates</a>
</section>
<section class="mbr-section mbr-section--relative mbr-section--fixed-size" data-rv-view="7" >
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <div class="row well decal_bottom" id="la_recherche">

        <table class="table">
    <thead>
    <tr>
        <th>N°Réservation</th>
        <th>ID</th>
        <th>Prénom</th>
        <th>Nom</th>
        <th>Tel</th>
        <th>Email</th>

    </tr>
    </thead>
    <tbody>
    <tr>
        <?php
        if ($result->num_rows > 0) {

        while($row = $result->fetch_assoc()) {
        echo "<td>" . $N_réservation . "</td>" . "<td>" . $row["id_cli"] . "</td>" . "<td>" . $row["prenom"] . "</td>" . " <td>" . $row["nom"] . "</td>" . " <td>" . $row["tel"] . "</td>" . " <td>" . $row["email"] . "</td>";
        ?>
    </tr>
    <?php
    }
    ?>

    </tbody>
        </table>
</section>




<script src="../../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../../assets/jarallax/jarallax.js"></script>
<script src="../../assets/mobirise/js/script.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="../../assets/create/create.js"></script>
<script type="text/javascript">
</script>
<?php
$conn->close();

?>
</body>
</html>
