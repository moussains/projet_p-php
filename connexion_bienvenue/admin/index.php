<!DOCTYPE html>

<html lang="fr">

<head>
    <!-- Site made with Mobirise Website Builder v4.8.7, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.8.7, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Profil - LVHM</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">

    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../../assets/create/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/create/profil.css">

</head>

<body >
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="../"><img src="../../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="../">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../contact"><span class="glyphicon glyphicon-earphone"></span> CONTACT</a></li>
                                <li class="mbr-navbar__item "><a class="mbr-buttons__link btn text-white active" href="./"><span class="glyphicon glyphicon-user"></span> MON PROFIL</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../deconnecter"><span class="glyphicon glyphicon-log-out"></span> DÉCONNEXION</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "lvhm";

// connexion
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//afficher tout les reservation
$query= "select id_cli , numReservation,immatriculation,montant from reserver";
$result = $conn->query($query);


?>


<section class="engine">
    <a href="#">css templates</a>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" data-rv-view="7" >
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <div class="well well-sm">
            <h2 class="text-center">MON PROFIL</h2>
        </div>
        <br><br>

            <div class="well col-sm-12 col-md-12">
                <ul class="nav nav-tabs _24">
                    <li class="active"><a data-toggle="tab" href="#tab_b">Tableau de consultation</a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab_b" class="tab-pane fade in active">
                        <div class="well well-info">
                            <h3>Les réservations</h3>

                            <div>
                                <div class="table-responsive" id="location_">
                                    <table class="table">

                                        <thead>
                                        <tr>
                                            <th>N°Client</th>
                                            <th>N° Réservation</th>
                                            <th> N°Voiture</th>
                                            <th>Prix</th>
                                            <th>Plus de détails</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            <tr class="warning">
                                                <?php
                                                if ($result->num_rows > 0) {

                                                while($row = $result->fetch_assoc()) {
                                                echo "<td id='N°Client'>". $row["id_cli"]."</td>"."<td>". $row["numReservation"]."</td>". " <td>" . $row["immatriculation"] . "</td>". " <td>" . $row["montant"]. "€"."</td>";
                                                 ?>


                                                <td>
                                                    <button  title="Voir plus de détails" class="btn btn-voir" ><a href="afficher.php" >Afficher</a></button>
                                                    <button  title="Voir plus de détails" class="btn btn-warning" ><a href="modifier.php" >Modifier </a></button>
                                                    <button  title="Voir plus de détails" class="btn btn-danger" onclick=" return confirm('Etes vous sur de vouloir supprimer');"   ><a href="supprimer.php" >Supprimer</a></button>
                                                </td>
                                            </tr>
                                           <?php  }
                                            } else {
                                            echo "0 results";
                                            }
                                           ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

</section>








<script src="../../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../../assets/jarallax/jarallax.js"></script>
<script src="../../assets/mobirise/js/script.js"></script>



</body>
<?php
include("../../footer/connexion_footer.php");
$conn->close();
?>
</html>