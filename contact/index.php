<?php
    require_once('../classes.php'); 
    VerifSessionOn();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="../assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Contact - LVHM</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&subset=cyrillic,latin,greek,vietnamese">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/animatecss/animate.min.css">
    <link rel="stylesheet" href="../assets/mobirise/css/style.css">
    <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="../assets/create/style.css">
    <link rel="stylesheet" type="text/css" href="../assets/create/contact.css">
</head>

<body >
<section class="mbr-navbar mbr-navbar--freeze mbr-navbar--absolute mbr-navbar--sticky mbr-navbar--auto-collapse" id="menu-1" data-rv-view="2">
    <div class="mbr-navbar__section mbr-section">
        <div class="mbr-section__container container">
            <div class="mbr-navbar__container">
                <div class="mbr-navbar__column mbr-navbar__column--s mbr-navbar__brand">
                    <span class="mbr-navbar__brand-link mbr-brand mbr-brand--inline">
                        <span class="mbr-brand__logo"><a href="../"><img src="../assets/images/logo.png" class="mbr-navbar__brand-img mbr-brand__img" alt="Mobirise"></a></span>
                        <span class="mbr-brand__name"><a class="mbr-brand__name text-white" href="../">LVHM</a></span>
                    </span>
                </div>
                <div class="mbr-navbar__hamburger mbr-hamburger"><span class="mbr-hamburger__line"></span></div>
                <div class="mbr-navbar__column mbr-navbar__menu">
                    <nav class="mbr-navbar__menu-box mbr-navbar__menu-box--inline-right">
                        <div class="mbr-navbar__column">
                            <ul class="mbr-navbar__items mbr-navbar__items--right float-left mbr-buttons mbr-buttons--freeze mbr-buttons--right btn-decorator mbr-buttons--active mbr-buttons--only-links">
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white active" href="../contact"><span class="glyphicon glyphicon-earphone"></span>  CONTACT</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white " href="../inscrire"><span class="glyphicon glyphicon-plus-sign"></span> S'INSCRIRE</a></li>
                                <li class="mbr-navbar__item"><a class="mbr-buttons__link btn text-white" href="../connecter/"><span class="glyphicon glyphicon-user"></span> SE CONNECTER</a></li>
                            </ul>

                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine">
    <a href="#">css templates</a>
</section>

<section class="mbr-section mbr-section--relative mbr-section--fixed-size" data-rv-view="7" >
    <div class="mbr-section__container container mbr-section__container--std-top-padding">
        <h2 class="text-center">CONTACT</h2>
        <br><br>
        <div class="mbr-section__row row">
            <div class="well col-sm-12 col-md-12">
                <div class="col-md-6 col-sm-6">
                    <h3 class="text-center">Déposez un message</h3>
                    <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                        <form>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="nom">Nom :</label>
                                    <input class="col-sm-12 col-md-12" type="text" id="nom" name="nom" placeholder="Entrer votre nom">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="prenom">Prénom :</label>
                                   <input class="col-sm-12 col-md-12" type="text" id="prenom" name="prenom" placeholder="Entrer votre prénom">
                            </div>
                            <div class="form-group">
                                 <label class="col-sm-12 col-md-12" for="email">Adresse e-mail :</label>
                                   <input class="col-sm-12 col-md-12" type="email" id="email" name="email" placeholder="Votre adresse e-mail">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="sujet">Sujet:</label>
                                    <input class="col-sm-12 col-md-12" type="text"  id="sujet" name="sujet" placeholder="Entrer votre sujet">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-md-12" for="message">Message:</label>
                                <textarea class="col-sm-12 col-md-12" type="text"  rows="5" id="message" name="message" placeholder="Votre message ..."></textarea>
                            </div>
                            <button type="submit" class="lebouton col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 col-xs-offset-3 col-xs-6 btn btn-default etape">
                                <span class="glyphicon glyphicon-send"></span> Envoyer
                            </button>
                        </form>
                    </div>
                </div> 
                <div class="col-md-6 col-sm-6 pad_Titre">
                    <h3 class="text-center">Nous conctacter par téléphone</h3>
                    <p class="_100">
                        <span class="glyphicon glyphicon-phone-alt"></span>
                        <span class="_20">+33(0)6 54 35 48 75</span>
                    </p>
                </div>
            </div>
            <br><br><br>
        </div>
    </div>
</section>

<?php
include("../footer/other_footer.php");
?>


<script src="../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/smooth-scroll/smooth-scroll.js"></script>
<script src="../assets/jarallax/jarallax.js"></script>
<script src="../assets/mobirise/js/script.js"></script>

</body>
</html>